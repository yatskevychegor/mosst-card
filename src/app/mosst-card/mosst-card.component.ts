import { Component, Input, OnChanges, EventEmitter, Output } from '@angular/core';

import { ICard, IUser } from './interfaces';
import { ApiService } from './services/api.service';
import { LocaleService } from './services/locale.service';

export type TMosstCardScreen = 'select' | 'add' | 'delete';

@Component({
  selector: 'mosst-card',
  templateUrl: './mosst-card.component.html',
  styleUrls: ['./mosst-card.component.scss']
})
export class MosstCardComponent implements OnChanges {
  @Input()
  public user: IUser;

  @Input()
  public lang: string;

  @Output()
  public onCardSelect: EventEmitter<ICard>;

  @Output()
  public onAddCard: EventEmitter<ICard>;

  @Output()
  public onCardDelete: EventEmitter<ICard>;

  public screen: TMosstCardScreen = 'select';

  public card: ICard;

  public constructor(
    private localeService: LocaleService,
    private apiService: ApiService
  ) {
    this.onCardSelect = new EventEmitter<ICard>();
    this.onAddCard = new EventEmitter<ICard>();
    this.onCardDelete = new EventEmitter<ICard>();
  }

  public ngOnChanges(changes): void {
    if (changes.user) {
      this.apiService.setUser(this.user);
    }

    if (changes.lang) {
      this.localeService.setLocale(this.lang);
    }
  }

  public addCard(card: ICard): void {
    this.onAddCard.emit(card);
    this.setScreen('select');
  }

  public selectCard(card: ICard): void {
    this.onCardSelect.emit(card);
    this.setScreen('select');
  }

  public deleteCard(card: ICard): void {
    this.onCardDelete.emit(card);
    this.setScreen('select');
  }

  public setScreen(screen: TMosstCardScreen, card = this.card) {
    this.screen = screen;
    this.card = card;
  }
}
