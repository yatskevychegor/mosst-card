export default {
  DELETE_CARD: {
    TITLE: 'Удалить карту',
    DESCRIPTION: 'После удаления карты вы не сможете использовать возможность автозаполнения номера карты',
    SUBMIT_BUTTON: 'Удалить карту',
    CANCEL_BUTTON: 'Отмена'
  },
  ADD_CARD: {
    TITLE: 'Добавить карту',
    DESCRIPTION: 'В названии карты должно быть минимум 3 неспециальных символа',
    SUBMIT_BUTTON: 'Добавить',
    CANCEL_BUTTON: 'Отмена'
  },
  CARD: {
    ALIAS: 'Название карты',
    PAN: 'Номер карты',
    EXP: 'Срок действия'
  }
};
