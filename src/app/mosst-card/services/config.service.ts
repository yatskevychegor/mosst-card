import { Injectable } from '@angular/core';

import { MosstCardModuleConfig } from '../config';

import { IMosstCardModuleConfig } from '../interfaces';

@Injectable()
export class ConfigService {
  public constructor(
    private config: MosstCardModuleConfig
  ) {}

  public getApiHost(): string {
    return this.config.api.host;
  }

  public getSign(): any {
    return Object.assign({}, this.config.api.sign);
  }
}
