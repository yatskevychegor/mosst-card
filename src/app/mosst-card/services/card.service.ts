import { Injectable } from '@angular/core';

import * as _ from 'lodash';

export type CardType = 'mc' | 'visa' | 'disc';

const CardTypePatterns: {[key: string]: RegExp} = {
  disc: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
  visa: /^4/,
  mc: /^5[1-5]/
};

@Injectable()
export class CardService {
  public getCardType(pan: string): CardType {
    const keys = _.keys(CardTypePatterns);
    return <CardType>keys.find((key) => CardTypePatterns[key].test(pan));
  }
}
