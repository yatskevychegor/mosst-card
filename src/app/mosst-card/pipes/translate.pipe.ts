import { Pipe, PipeTransform } from '@angular/core';

import { LocaleService } from '../services/locale.service';

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {
  public constructor(private localeService: LocaleService) {}

  public transform(value: any, ...args): any {
    return this.localeService.translate(value, args);
  }
}
