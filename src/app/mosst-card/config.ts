import { IMosstCardModuleConfig } from './interfaces';

export class MosstCardModuleConfig implements IMosstCardModuleConfig {
  public api: any;

  public constructor(config: IMosstCardModuleConfig) {
    this.api = config.api;
  }
}
