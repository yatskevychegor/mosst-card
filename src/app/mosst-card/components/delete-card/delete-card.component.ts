import { Component, OnChanges, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { ApiService } from '../../services/api.service';

import { ICard } from '../../interfaces';

@Component({
  selector: 'app-delete-card',
  templateUrl: './delete-card.component.html'
})
export class DeleteCardComponent implements OnChanges {
  @Input()
  public data: ICard;

  @Output()
  public onDelete: EventEmitter<ICard>;

  @Output()
  public onCancel: EventEmitter<ICard>;

  public formGroup: FormGroup = new FormGroup({
    alias: new FormControl(),
    pan: new FormControl(),
    exp: new FormControl()
  });

  public pan: string;

  public constructor(
    private apiService: ApiService
  ) {
    this.formGroup = new FormGroup({
      alias: new FormControl(),
      pan: new FormControl(),
      exp: new FormControl()
    });

    this.onDelete = new EventEmitter<ICard>();
    this.onCancel = new EventEmitter<ICard>();
  }

  public ngOnChanges(): void {

    const {alias, card, expiration} = this.data;

    this.formGroup.setValue({
      alias,
      pan: card,
      exp: expiration
    });
  }

  public onSubmit(): void {
    this.apiService.deleteCard({guid: this.data.guid})
      .subscribe((res) => {
        this.onDelete.emit(this.data);
      });
  }

  public cancel(): void {
    this.onCancel.emit(this.data);
  }
}
