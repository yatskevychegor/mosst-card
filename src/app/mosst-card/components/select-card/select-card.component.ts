import { Component, EventEmitter, Output, OnChanges } from '@angular/core';

import { ICard } from '../../interfaces';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-select-card',
  templateUrl: './select-card.component.html'
})
export class SelectCardComponent implements OnChanges {
  public cards: ICard[] = [];

  public cardOptions = {
    actions: true,
    name: true
  };

  @Output()
  public onAdd: EventEmitter<void>;

  @Output()
  public onSelect: EventEmitter<ICard>;

  @Output()
  public onDelete: EventEmitter<ICard>;

  public constructor(
    private apiService: ApiService
  ) {
    this.onAdd = new EventEmitter<void>();
    this.onSelect = new EventEmitter<ICard>();
    this.onDelete = new EventEmitter<ICard>();

    this.fetch();
  }

  public ngOnChanges(): void {
    setTimeout(() => this.fetch());
  }

  public add() {
    this.onAdd.emit();
  }

  public select(card: ICard) {
    this.onSelect.emit(card);
  }

  public delete(card: ICard) {
    this.onDelete.emit(card);
  }

  private fetch() {
    this.apiService.getCardList()
      .subscribe(cards => this.cards = cards);
  }
}
