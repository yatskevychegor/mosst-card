import { Component, OnChanges, EventEmitter, Input, Output } from '@angular/core';

import { ICard } from '../../interfaces';

import { format } from '../../modules/input-mask';
import { inputMasks } from '../../masks';
import { CCValidator } from '../../validators/CCValidator';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html'
})
export class CardComponent implements OnChanges {
  @Input()
  public data: ICard;

  @Input()
  public options: {
    actions?: boolean,
    name?: boolean
  } = {};

  @Output()
  public onSelect: EventEmitter<ICard>;

  @Output()
  public onDelete: EventEmitter<ICard>;

  public cardType: string;

  public pan: string;

  public constructor() {
    this.onSelect = new EventEmitter<ICard>();
    this.onDelete = new EventEmitter<ICard>();
  }

  public ngOnChanges(): void {
    setTimeout(() => {
      this.cardType = CCValidator.getCardType(this.data.card);
      this.pan = format(inputMasks.pan)(this.data.card);
    });
  }

  public select(): void {
    this.onSelect.emit(this.data);
  }

  public delete(): void {
    this.onDelete.emit(this.data);
  }
}
