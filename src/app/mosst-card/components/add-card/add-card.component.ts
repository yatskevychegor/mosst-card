import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { ApiService } from '../../services/api.service';

import { ICard } from '../../interfaces';

import { inputMasks } from '../../masks';

import { creditCardValidator } from '../../validators/cardValidator';
import { expDateValidator } from '../../validators/expDateValidator';

import { IMask } from '../../modules/input-mask/index';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html'
})
export class AddCardComponent {
  @Input()
  public data: ICard;

  @Output()
  public onAdd: EventEmitter<ICard>;

  @Output()
  public onCancel: EventEmitter<void>;

  public formGroup: FormGroup = new FormGroup({
    alias: new FormControl(),
    pan: new FormControl(),
    exp: new FormControl()
  });

  public pan: string;

  public panMask: IMask = inputMasks.pan;
  public expMask: IMask = inputMasks.exp;

  public constructor(
    private apiService: ApiService,
  ) {
    this.formGroup = new FormGroup({
      alias: new FormControl(),
      pan: new FormControl('', [
        creditCardValidator
      ]),
      exp: new FormControl('', [
        expDateValidator
      ])
    });

    this.onAdd = new EventEmitter<ICard>();
    this.onCancel = new EventEmitter<void>();
  }

  public onSubmit(): void {
    const {alias, exp} = this.formGroup.value;
    const {pan} = this;

    this.apiService.addCard({alias, exp, pan})
      .subscribe((res) => {
        this.onAdd.emit(this.data);
      });
  }

  public cancel(): void {
    this.onCancel.emit();
  }
}
