// core

import { NgModule, ModuleWithProviders, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// module

import { MosstCardComponent } from './mosst-card.component';

// components

import { CardComponent } from './components/card/card.component';
import { SelectCardComponent } from './components/select-card/select-card.component';
import { DeleteCardComponent } from './components/delete-card/delete-card.component';
import { AddCardComponent } from './components/add-card/add-card.component';

// pipe

import { TranslatePipe } from './pipes/translate.pipe';

// services

import { LocaleService } from './services/locale.service';
import { ApiService } from './services/api.service';
import { ConfigService } from './services/config.service';

// modules

import { InputMaskModule } from './modules/input-mask/input-mask.module';

// interfaces

import { IMosstCardModuleConfig } from './interfaces';

// config

import { MosstCardModuleConfig } from './config';

export const MOSST_CARD_MODULE_CONFIG = new InjectionToken('MOSST_CARD_MODULE_CONFIG');

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    InputMaskModule
  ],
  declarations: [
    MosstCardComponent,
    CardComponent,
    SelectCardComponent,
    DeleteCardComponent,
    AddCardComponent,
    TranslatePipe
  ],
  providers: [
    ApiService,
    LocaleService,
    ConfigService
  ],
  exports: [
    MosstCardComponent
  ]
})
export class MosstCardModule {
  public static forRoot(config?: IMosstCardModuleConfig): ModuleWithProviders {
    return {
      ngModule: MosstCardModule,
      providers: [
        {
          provide: MOSST_CARD_MODULE_CONFIG,
          useValue: config
        },
        {
         provide: MosstCardModuleConfig,
         useFactory: provideConfig,
         deps: [MOSST_CARD_MODULE_CONFIG]
        },
        ConfigService
      ]
    };
  }

  public static forChild(): ModuleWithProviders {
    return {
      ngModule: MosstCardModule
    };
  }
}

export function provideConfig(config: IMosstCardModuleConfig) {
  return new MosstCardModuleConfig(config);
}
