import { AbstractControl } from '@angular/forms';

const expDateRegExp = /^((0[1-9])|(1[0-2]))\/[0-9][0-9]$/;

export function expDateValidator(control: AbstractControl): {[s: string]: boolean} {
  const exp: string = control.value.toString();
  if (exp.length > 0) {
    if (!expDateRegExp.test(exp)) {
      return { 'invalid': true };
    }
  }
}
