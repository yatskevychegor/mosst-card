export interface IMask {
  mask?: ((val: string) => string) | string;
  output?: ((val: string) => string) | string;
  allowedInputs?: string;
  advancedSetter?: ((val: string) => string) | RegExp | string;
  replacement?: string;
  maskedValue?: boolean;
  showConstants?: boolean;
}
