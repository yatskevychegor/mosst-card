import { NgModule } from '@angular/core';

import { InputMaskDirective } from './directives/mask.directive';

@NgModule({
  declarations: [
    InputMaskDirective
  ],
  exports: [
    InputMaskDirective
  ]
})
export class InputMaskModule {}
