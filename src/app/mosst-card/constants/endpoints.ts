export const API_CARD_LIST = '/card/list';
export const API_CARD_ADD = '/card/store';
export const API_CARD_DELETE = '/card/delete';
