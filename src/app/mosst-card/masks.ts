import { IMask } from './modules/input-mask';

export const inputMasks: {[key: string]: IMask} = {
  pan: {
    mask: 'XXXX XXXX XXXX XXXX',
    output: 'XXXXXXXXXXXXXXXX',
    allowedInputs: '[0-9|\*]',
    advancedSetter: /^[456].*/g
  },
  exp: {
    mask: 'XX/XX',
    output: (output) => output,
    allowedInputs: '[0-9]',
    maskedValue: true,
    showConstants: true
  }
};
