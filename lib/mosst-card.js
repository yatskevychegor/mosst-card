import { Component, Directive, ElementRef, EventEmitter, Injectable, InjectionToken, Input, NgModule, Output, Pipe } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, FormsModule, NgModel, ReactiveFormsModule } from '@angular/forms';
import { Http, HttpModule, RequestOptions } from '@angular/http';
import { Observable as Observable$1 } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import { get, pick } from 'lodash';
import { MD5, enc } from 'crypto-js';

class MosstCardModuleConfig {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.api = config.api;
    }
}

class ConfigService {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
    }
    /**
     * @return {?}
     */
    getApiHost() {
        return this.config.api.host;
    }
    /**
     * @return {?}
     */
    getSign() {
        return Object.assign({}, this.config.api.sign);
    }
}
ConfigService.decorators = [
    { type: Injectable },
];
/**
 * @nocollapse
 */
ConfigService.ctorParameters = () => [
    { type: MosstCardModuleConfig, },
];

const API_CARD_LIST = '/card/list';
const API_CARD_ADD = '/card/store';
const API_CARD_DELETE = '/card/delete';

const CURRENT_YEAR_PREFIX = '20';
class ApiService {
    /**
     * @param {?} http
     * @param {?} configService
     */
    constructor(http$$1, configService) {
        this.http = http$$1;
        this.configService = configService;
        this.apiHost = configService.getApiHost();
    }
    /**
     * @param {?} user
     * @return {?}
     */
    setUser(user) {
        this.user = (pick(user, ['profile_guid', 'profile_session_guid']));
    }
    /**
     * @return {?}
     */
    getCardList() {
        return this.request({
            path: API_CARD_LIST,
            method: 'POST'
        }).map((res) => res.cards.map((card) => {
            return Object.assign({}, card, {
                expiration: card.expiration.split(`/${CURRENT_YEAR_PREFIX}`).join('/')
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    addCard(params) {
        const /** @type {?} */ card = Object.assign({}, params, {
            exp: params.exp.split('/').join(`/${CURRENT_YEAR_PREFIX}`)
        });
        return this.request({
            path: API_CARD_ADD,
            method: 'POST',
            body: { card }
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    deleteCard(params) {
        return this.request({
            path: API_CARD_DELETE,
            method: 'POST',
            body: { card_guid: params.guid }
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    request(params) {
        const { path, method, body } = params;
        const /** @type {?} */ host = this.apiHost[this.apiHost.length - 1] === '/'
            ? this.apiHost.slice(0, this.apiHost.length - 1)
            : this.apiHost;
        const /** @type {?} */ url = host + (path[0] === '/'
            ? path
            : ('/' + path));
        const /** @type {?} */ options = new RequestOptions({ url, method, body: Object.assign(body || {}, {
                auth: this.getAuthSign()
            }) });
        return this.http
            .request(url, options)
            .catch(() => {
            return Observable$1.throw({ code: -1, message: 'Server error.' });
        })
            .flatMap((res) => {
            const /** @type {?} */ json = res.json();
            const { error } = json;
            if (res.status !== 200 || error) {
                if (error) {
                    return Observable$1.throw(typeof (error) === 'object' ? error : { code: -1, message: error });
                }
                else {
                    return Observable$1.throw(error || { code: -1, message: 'Invalid request.' });
                }
            }
            return Observable$1.of(json);
        })
            .catch(error => Observable$1.throw(error));
    }
    /**
     * @return {?}
     */
    getAuthSign() {
        const { memberPointId, key } = this.configService.getSign();
        const /** @type {?} */ dateISO = (new Date()).toISOString();
        return Object.assign({
            memberPointId,
            time: dateISO,
            sign: MD5(memberPointId + dateISO + key).toString(enc.Hex)
        }, this.user);
    }
}
ApiService.decorators = [
    { type: Injectable },
];
/**
 * @nocollapse
 */
ApiService.ctorParameters = () => [
    { type: Http, },
    { type: ConfigService, },
];

var en = {};

var ru = {
    DELETE_CARD: {
        TITLE: 'Удалить карту',
        DESCRIPTION: 'После удаления карты вы не сможете использовать возможность автозаполнения номера карты',
        SUBMIT_BUTTON: 'Удалить карту',
        CANCEL_BUTTON: 'Отмена'
    },
    ADD_CARD: {
        TITLE: 'Добавить карту',
        DESCRIPTION: 'В названии карты должно быть минимум 3 неспециальных символа',
        SUBMIT_BUTTON: 'Добавить',
        CANCEL_BUTTON: 'Отмена'
    },
    CARD: {
        ALIAS: 'Название карты',
        PAN: 'Номер карты',
        EXP: 'Срок действия'
    }
};

var uk = {};

var locales = { en, ru, uk };

class LocaleService {
    constructor() {
        this.currentLang = 'ru';
        this.locales = locales;
    }
    /**
     * @param {?} value
     * @param {?} args
     * @return {?}
     */
    translate(value, args) {
        let /** @type {?} */ currentValue = get(this.locales[this.currentLang], value) || value;
        args.forEach((arg, index) => {
            currentValue = currentValue.replace(`%S${index + 1}`, arg);
        });
        return currentValue;
    }
    /**
     * @param {?=} customLocales
     * @return {?}
     */
    updateLocales(customLocales = {}) {
        for (const /** @type {?} */ locale in customLocales) {
            if (customLocales.hasOwnProperty(locale)) {
                this.locales[locale] = Object.assign({}, this.locales[locale], customLocales[locale]);
            }
        }
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    setLocale(lang) {
        if (Object.keys(this.locales).indexOf(lang) === -1) {
            return;
        }
        this.currentLang = lang;
    }
}
LocaleService.decorators = [
    { type: Injectable },
];
/**
 * @nocollapse
 */
LocaleService.ctorParameters = () => [];

class MosstCardComponent {
    /**
     * @param {?} localeService
     * @param {?} apiService
     */
    constructor(localeService, apiService) {
        this.localeService = localeService;
        this.apiService = apiService;
        this.screen = 'select';
        this.onCardSelect = new EventEmitter();
        this.onAddCard = new EventEmitter();
        this.onCardDelete = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.user) {
            this.apiService.setUser(this.user);
        }
        if (changes.lang) {
            this.localeService.setLocale(this.lang);
        }
    }
    /**
     * @param {?} card
     * @return {?}
     */
    addCard(card) {
        this.onAddCard.emit(card);
        this.setScreen('select');
    }
    /**
     * @param {?} card
     * @return {?}
     */
    selectCard(card) {
        this.onCardSelect.emit(card);
        this.setScreen('select');
    }
    /**
     * @param {?} card
     * @return {?}
     */
    deleteCard(card) {
        this.onCardDelete.emit(card);
        this.setScreen('select');
    }
    /**
     * @param {?} screen
     * @param {?=} card
     * @return {?}
     */
    setScreen(screen, card = this.card) {
        this.screen = screen;
        this.card = card;
    }
}
MosstCardComponent.decorators = [
    { type: Component, args: [{
                selector: 'mosst-card',
                template: `
    <app-select-card *ngIf="screen === 'select'"
      (onSelect)="selectCard($event)"
      (onDelete)="setScreen('delete', $event)"
      (onAdd)="setScreen('add')"
    ></app-select-card>

    <app-add-card *ngIf="screen === 'add'"
      (onAdd)="addCard($event)"
      (onCancel)="setScreen('select')"
    ></app-add-card>

    <app-delete-card *ngIf="screen === 'delete'"
      [data]="card"
      (onDelete)="deleteCard($event)"
      (onCancel)="setScreen('select')"
    ></app-delete-card>
  `,
                styles: [`
    @font-face {
      font-family: "Roboto";
      src: url("./assets/fonts/RobotoRegular/RobotoRegular.eot");
      src: url("./assets/fonts/RobotoRegular/RobotoRegular.eot?#iefix") format("embedded-opentype"), url("./assets/fonts/RobotoRegular/RobotoRegular.woff") format("woff"), url("./assets/fonts/RobotoRegular/RobotoRegular.ttf") format("truetype");
      font-style: normal;
      font-weight: 400; }

    @font-face {
      font-family: "RobotoMedium";
      src: url("./assets/fonts/RobotoMedium/RobotoMedium.eot");
      src: url("./assets/fonts/RobotoMedium/RobotoMedium.eot?#iefix") format("embedded-opentype"), url("./assets/fonts/RobotoMedium/RobotoMedium.woff") format("woff"), url("./assets/fonts/RobotoMedium/RobotoMedium.ttf") format("truetype");
      font-style: normal;
      font-weight: 500; }

    @font-face {
      font-family: "Material Design Icons";
      src: url("./assets/fonts/icons/materialdesignicons-webfont.eot?v=1.8.36");
      src: url("./assets/fonts/icons/materialdesignicons-webfont.eot?#iefix&v=1.8.36") format("embedded-opentype"), url("./assets/fonts/icons/materialdesignicons-webfont.woff2?v=1.8.36") format("woff2"), url("./assets/fonts/icons/materialdesignicons-webfont.woff?v=1.8.36") format("woff"), url("./assets/fonts/icons/materialdesignicons-webfont.ttf?v=1.8.36") format("truetype"), url("./assets/fonts/icons/materialdesignicons-webfont.svg?v=1.8.36#materialdesigniconsregular") format("svg");
      font-weight: normal;
      font-style: normal; }

    * {
      font-family: "Roboto", sans-serif; }

    :host ::ng-deep {
      /* ----------  Height ---------- */
      /* ---------- Colors ---------- */
      /* ---------- Backgrounds ---------- */
      /* ---------- Shadows ---------- */
      /* ---------- Radius ---------- */
      /* ----------  Icons ---------- */
      /* ----------  Icons ---------- */ }
      :host ::ng-deep * {
        max-height: 100000px;
        -webkit-box-sizing: border-box;
                box-sizing: border-box; }
      :host ::ng-deep *:before,
      :host ::ng-deep *:after {
        -webkit-box-sizing: border-box;
                box-sizing: border-box; }
      :host ::ng-deep img {
        vertical-align: top;
        max-width: 100%;
        border: 0;
        -ms-interpolation-mode: bicubic; }
      :host ::ng-deep form, :host ::ng-deep fieldset {
        margin: 0;
        padding: 0;
        border-style: none; }
      :host ::ng-deep input[type=text]::-ms-clear {
        display: none; }
      :host ::ng-deep input[type="search"]::-webkit-search-decoration {
        display: none; }
      :host ::ng-deep button,
      :host ::ng-deep textarea,
      :host ::ng-deep input[type=text],
      :host ::ng-deep input[type=file],
      :host ::ng-deep input[type=submit],
      :host ::ng-deep input[type=button],
      :host ::ng-deep input[type=search],
      :host ::ng-deep input[type=reset],
      :host ::ng-deep input[type=password],
      :host ::ng-deep input[type="search"]::-webkit-search-cancel-button {
        -webkit-appearance: none;
        outline: none; }
      :host ::ng-deep button,
      :host ::ng-deep input[type=button],
      :host ::ng-deep input[type=reset],
      :host ::ng-deep input[type=submit] {
        cursor: pointer; }
      :host ::ng-deep textarea,
      :host ::ng-deep input {
        border-radius: 0; }
      :host ::ng-deep input[type=image] {
        padding: 0;
        border: none; }
      :host ::ng-deep button::-moz-focus-inner,
      :host ::ng-deep input::-moz-focus-inner {
        padding: 0;
        border: 0; }
      :host ::ng-deep *:focus {
        outline: none; }
      :host ::ng-deep main, :host ::ng-deep article, :host ::ng-deep aside, :host ::ng-deep details, :host ::ng-deep figcaption, :host ::ng-deep figure, :host ::ng-deep footer,
      :host ::ng-deep header, :host ::ng-deep hgroup, :host ::ng-deep menu, :host ::ng-deep nav, :host ::ng-deep section {
        display: block; }
      :host ::ng-deep mark {
        background: none; }
      :host ::ng-deep blockquote, :host ::ng-deep q {
        quotes: none; }
        :host ::ng-deep blockquote:before, :host ::ng-deep blockquote:after, :host ::ng-deep q:before, :host ::ng-deep q:after {
          content: '';
          content: none; }
      :host ::ng-deep p, :host ::ng-deep dl, :host ::ng-deep menu, :host ::ng-deep ol, :host ::ng-deep ul {
        margin: 1em 0; }
      :host ::ng-deep dd {
        margin: 0 0 0 40px; }
      :host ::ng-deep menu, :host ::ng-deep ol, :host ::ng-deep ul {
        padding: 0; }
      :host ::ng-deep nav ul, :host ::ng-deep nav ol {
        padding: 0;
        margin: 0;
        list-style: none;
        list-style-type: none; }
      :host ::ng-deep table {
        border-collapse: collapse;
        border-spacing: 0; }
      :host ::ng-deep h1, :host ::ng-deep h2, :host ::ng-deep h3, :host ::ng-deep h4, :host ::ng-deep h5, :host ::ng-deep h6 {
        margin: 0; }
      :host ::ng-deep .mdi-access-point:before {
        content: "\\F002"; }
      :host ::ng-deep .mdi-access-point-network:before {
        content: "\\F003"; }
      :host ::ng-deep .mdi-account:before {
        content: "\\F004"; }
      :host ::ng-deep .mdi-account-alert:before {
        content: "\\F005"; }
      :host ::ng-deep .mdi-account-box:before {
        content: "\\F006"; }
      :host ::ng-deep .mdi-account-box-outline:before {
        content: "\\F007"; }
      :host ::ng-deep .mdi-account-card-details:before {
        content: "\\F5D2"; }
      :host ::ng-deep .mdi-account-check:before {
        content: "\\F008"; }
      :host ::ng-deep .mdi-account-circle:before {
        content: "\\F009"; }
      :host ::ng-deep .mdi-account-convert:before {
        content: "\\F00A"; }
      :host ::ng-deep .mdi-account-edit:before {
        content: "\\F6BB"; }
      :host ::ng-deep .mdi-account-key:before {
        content: "\\F00B"; }
      :host ::ng-deep .mdi-account-location:before {
        content: "\\F00C"; }
      :host ::ng-deep .mdi-account-minus:before {
        content: "\\F00D"; }
      :host ::ng-deep .mdi-account-multiple:before {
        content: "\\F00E"; }
      :host ::ng-deep .mdi-account-multiple-minus:before {
        content: "\\F5D3"; }
      :host ::ng-deep .mdi-account-multiple-outline:before {
        content: "\\F00F"; }
      :host ::ng-deep .mdi-account-multiple-plus:before {
        content: "\\F010"; }
      :host ::ng-deep .mdi-account-network:before {
        content: "\\F011"; }
      :host ::ng-deep .mdi-account-off:before {
        content: "\\F012"; }
      :host ::ng-deep .mdi-account-outline:before {
        content: "\\F013"; }
      :host ::ng-deep .mdi-account-plus:before {
        content: "\\F014"; }
      :host ::ng-deep .mdi-account-remove:before {
        content: "\\F015"; }
      :host ::ng-deep .mdi-account-search:before {
        content: "\\F016"; }
      :host ::ng-deep .mdi-account-settings:before {
        content: "\\F630"; }
      :host ::ng-deep .mdi-account-settings-variant:before {
        content: "\\F631"; }
      :host ::ng-deep .mdi-account-star:before {
        content: "\\F017"; }
      :host ::ng-deep .mdi-account-star-variant:before {
        content: "\\F018"; }
      :host ::ng-deep .mdi-account-switch:before {
        content: "\\F019"; }
      :host ::ng-deep .mdi-adjust:before {
        content: "\\F01A"; }
      :host ::ng-deep .mdi-air-conditioner:before {
        content: "\\F01B"; }
      :host ::ng-deep .mdi-airballoon:before {
        content: "\\F01C"; }
      :host ::ng-deep .mdi-airplane:before {
        content: "\\F01D"; }
      :host ::ng-deep .mdi-airplane-landing:before {
        content: "\\F5D4"; }
      :host ::ng-deep .mdi-airplane-off:before {
        content: "\\F01E"; }
      :host ::ng-deep .mdi-airplane-takeoff:before {
        content: "\\F5D5"; }
      :host ::ng-deep .mdi-airplay:before {
        content: "\\F01F"; }
      :host ::ng-deep .mdi-alarm:before {
        content: "\\F020"; }
      :host ::ng-deep .mdi-alarm-check:before {
        content: "\\F021"; }
      :host ::ng-deep .mdi-alarm-multiple:before {
        content: "\\F022"; }
      :host ::ng-deep .mdi-alarm-off:before {
        content: "\\F023"; }
      :host ::ng-deep .mdi-alarm-plus:before {
        content: "\\F024"; }
      :host ::ng-deep .mdi-alarm-snooze:before {
        content: "\\F68D"; }
      :host ::ng-deep .mdi-album:before {
        content: "\\F025"; }
      :host ::ng-deep .mdi-alert:before {
        content: "\\F026"; }
      :host ::ng-deep .mdi-alert-box:before {
        content: "\\F027"; }
      :host ::ng-deep .mdi-alert-circle:before {
        content: "\\F028"; }
      :host ::ng-deep .mdi-alert-circle-outline:before {
        content: "\\F5D6"; }
      :host ::ng-deep .mdi-alert-octagon:before {
        content: "\\F029"; }
      :host ::ng-deep .mdi-alert-octagram:before {
        content: "\\F6BC"; }
      :host ::ng-deep .mdi-alert-outline:before {
        content: "\\F02A"; }
      :host ::ng-deep .mdi-all-inclusive:before {
        content: "\\F6BD"; }
      :host ::ng-deep .mdi-alpha:before {
        content: "\\F02B"; }
      :host ::ng-deep .mdi-alphabetical:before {
        content: "\\F02C"; }
      :host ::ng-deep .mdi-altimeter:before {
        content: "\\F5D7"; }
      :host ::ng-deep .mdi-amazon:before {
        content: "\\F02D"; }
      :host ::ng-deep .mdi-amazon-clouddrive:before {
        content: "\\F02E"; }
      :host ::ng-deep .mdi-ambulance:before {
        content: "\\F02F"; }
      :host ::ng-deep .mdi-amplifier:before {
        content: "\\F030"; }
      :host ::ng-deep .mdi-anchor:before {
        content: "\\F031"; }
      :host ::ng-deep .mdi-android:before {
        content: "\\F032"; }
      :host ::ng-deep .mdi-android-debug-bridge:before {
        content: "\\F033"; }
      :host ::ng-deep .mdi-android-studio:before {
        content: "\\F034"; }
      :host ::ng-deep .mdi-angular:before {
        content: "\\F6B1"; }
      :host ::ng-deep .mdi-angularjs:before {
        content: "\\F6BE"; }
      :host ::ng-deep .mdi-animation:before {
        content: "\\F5D8"; }
      :host ::ng-deep .mdi-apple:before {
        content: "\\F035"; }
      :host ::ng-deep .mdi-apple-finder:before {
        content: "\\F036"; }
      :host ::ng-deep .mdi-apple-ios:before {
        content: "\\F037"; }
      :host ::ng-deep .mdi-apple-keyboard-caps:before {
        content: "\\F632"; }
      :host ::ng-deep .mdi-apple-keyboard-command:before {
        content: "\\F633"; }
      :host ::ng-deep .mdi-apple-keyboard-control:before {
        content: "\\F634"; }
      :host ::ng-deep .mdi-apple-keyboard-option:before {
        content: "\\F635"; }
      :host ::ng-deep .mdi-apple-keyboard-shift:before {
        content: "\\F636"; }
      :host ::ng-deep .mdi-apple-mobileme:before {
        content: "\\F038"; }
      :host ::ng-deep .mdi-apple-safari:before {
        content: "\\F039"; }
      :host ::ng-deep .mdi-application:before {
        content: "\\F614"; }
      :host ::ng-deep .mdi-apps:before {
        content: "\\F03B"; }
      :host ::ng-deep .mdi-archive:before {
        content: "\\F03C"; }
      :host ::ng-deep .mdi-arrange-bring-forward:before {
        content: "\\F03D"; }
      :host ::ng-deep .mdi-arrange-bring-to-front:before {
        content: "\\F03E"; }
      :host ::ng-deep .mdi-arrange-send-backward:before {
        content: "\\F03F"; }
      :host ::ng-deep .mdi-arrange-send-to-back:before {
        content: "\\F040"; }
      :host ::ng-deep .mdi-arrow-all:before {
        content: "\\F041"; }
      :host ::ng-deep .mdi-arrow-bottom-left:before {
        content: "\\F042"; }
      :host ::ng-deep .mdi-arrow-bottom-right:before {
        content: "\\F043"; }
      :host ::ng-deep .mdi-arrow-compress:before {
        content: "\\F615"; }
      :host ::ng-deep .mdi-arrow-compress-all:before {
        content: "\\F044"; }
      :host ::ng-deep .mdi-arrow-down:before {
        content: "\\F045"; }
      :host ::ng-deep .mdi-arrow-down-bold:before {
        content: "\\F046"; }
      :host ::ng-deep .mdi-arrow-down-bold-circle:before {
        content: "\\F047"; }
      :host ::ng-deep .mdi-arrow-down-bold-circle-outline:before {
        content: "\\F048"; }
      :host ::ng-deep .mdi-arrow-down-bold-hexagon-outline:before {
        content: "\\F049"; }
      :host ::ng-deep .mdi-arrow-down-box:before {
        content: "\\F6BF"; }
      :host ::ng-deep .mdi-arrow-down-drop-circle:before {
        content: "\\F04A"; }
      :host ::ng-deep .mdi-arrow-down-drop-circle-outline:before {
        content: "\\F04B"; }
      :host ::ng-deep .mdi-arrow-expand:before {
        content: "\\F616"; }
      :host ::ng-deep .mdi-arrow-expand-all:before {
        content: "\\F04C"; }
      :host ::ng-deep .mdi-arrow-left:before {
        content: "\\F04D"; }
      :host ::ng-deep .mdi-arrow-left-bold:before {
        content: "\\F04E"; }
      :host ::ng-deep .mdi-arrow-left-bold-circle:before {
        content: "\\F04F"; }
      :host ::ng-deep .mdi-arrow-left-bold-circle-outline:before {
        content: "\\F050"; }
      :host ::ng-deep .mdi-arrow-left-bold-hexagon-outline:before {
        content: "\\F051"; }
      :host ::ng-deep .mdi-arrow-left-box:before {
        content: "\\F6C0"; }
      :host ::ng-deep .mdi-arrow-left-drop-circle:before {
        content: "\\F052"; }
      :host ::ng-deep .mdi-arrow-left-drop-circle-outline:before {
        content: "\\F053"; }
      :host ::ng-deep .mdi-arrow-right:before {
        content: "\\F054"; }
      :host ::ng-deep .mdi-arrow-right-bold:before {
        content: "\\F055"; }
      :host ::ng-deep .mdi-arrow-right-bold-circle:before {
        content: "\\F056"; }
      :host ::ng-deep .mdi-arrow-right-bold-circle-outline:before {
        content: "\\F057"; }
      :host ::ng-deep .mdi-arrow-right-bold-hexagon-outline:before {
        content: "\\F058"; }
      :host ::ng-deep .mdi-arrow-right-box:before {
        content: "\\F6C1"; }
      :host ::ng-deep .mdi-arrow-right-drop-circle:before {
        content: "\\F059"; }
      :host ::ng-deep .mdi-arrow-right-drop-circle-outline:before {
        content: "\\F05A"; }
      :host ::ng-deep .mdi-arrow-top-left:before {
        content: "\\F05B"; }
      :host ::ng-deep .mdi-arrow-top-right:before {
        content: "\\F05C"; }
      :host ::ng-deep .mdi-arrow-up:before {
        content: "\\F05D"; }
      :host ::ng-deep .mdi-arrow-up-bold:before {
        content: "\\F05E"; }
      :host ::ng-deep .mdi-arrow-up-bold-circle:before {
        content: "\\F05F"; }
      :host ::ng-deep .mdi-arrow-up-bold-circle-outline:before {
        content: "\\F060"; }
      :host ::ng-deep .mdi-arrow-up-bold-hexagon-outline:before {
        content: "\\F061"; }
      :host ::ng-deep .mdi-arrow-up-box:before {
        content: "\\F6C2"; }
      :host ::ng-deep .mdi-arrow-up-drop-circle:before {
        content: "\\F062"; }
      :host ::ng-deep .mdi-arrow-up-drop-circle-outline:before {
        content: "\\F063"; }
      :host ::ng-deep .mdi-assistant:before {
        content: "\\F064"; }
      :host ::ng-deep .mdi-asterisk:before {
        content: "\\F6C3"; }
      :host ::ng-deep .mdi-at:before {
        content: "\\F065"; }
      :host ::ng-deep .mdi-attachment:before {
        content: "\\F066"; }
      :host ::ng-deep .mdi-audiobook:before {
        content: "\\F067"; }
      :host ::ng-deep .mdi-auto-fix:before {
        content: "\\F068"; }
      :host ::ng-deep .mdi-auto-upload:before {
        content: "\\F069"; }
      :host ::ng-deep .mdi-autorenew:before {
        content: "\\F06A"; }
      :host ::ng-deep .mdi-av-timer:before {
        content: "\\F06B"; }
      :host ::ng-deep .mdi-baby:before {
        content: "\\F06C"; }
      :host ::ng-deep .mdi-baby-buggy:before {
        content: "\\F68E"; }
      :host ::ng-deep .mdi-backburger:before {
        content: "\\F06D"; }
      :host ::ng-deep .mdi-backspace:before {
        content: "\\F06E"; }
      :host ::ng-deep .mdi-backup-restore:before {
        content: "\\F06F"; }
      :host ::ng-deep .mdi-bandcamp:before {
        content: "\\F674"; }
      :host ::ng-deep .mdi-bank:before {
        content: "\\F070"; }
      :host ::ng-deep .mdi-barcode:before {
        content: "\\F071"; }
      :host ::ng-deep .mdi-barcode-scan:before {
        content: "\\F072"; }
      :host ::ng-deep .mdi-barley:before {
        content: "\\F073"; }
      :host ::ng-deep .mdi-barrel:before {
        content: "\\F074"; }
      :host ::ng-deep .mdi-basecamp:before {
        content: "\\F075"; }
      :host ::ng-deep .mdi-basket:before {
        content: "\\F076"; }
      :host ::ng-deep .mdi-basket-fill:before {
        content: "\\F077"; }
      :host ::ng-deep .mdi-basket-unfill:before {
        content: "\\F078"; }
      :host ::ng-deep .mdi-battery:before {
        content: "\\F079"; }
      :host ::ng-deep .mdi-battery-10:before {
        content: "\\F07A"; }
      :host ::ng-deep .mdi-battery-20:before {
        content: "\\F07B"; }
      :host ::ng-deep .mdi-battery-30:before {
        content: "\\F07C"; }
      :host ::ng-deep .mdi-battery-40:before {
        content: "\\F07D"; }
      :host ::ng-deep .mdi-battery-50:before {
        content: "\\F07E"; }
      :host ::ng-deep .mdi-battery-60:before {
        content: "\\F07F"; }
      :host ::ng-deep .mdi-battery-70:before {
        content: "\\F080"; }
      :host ::ng-deep .mdi-battery-80:before {
        content: "\\F081"; }
      :host ::ng-deep .mdi-battery-90:before {
        content: "\\F082"; }
      :host ::ng-deep .mdi-battery-alert:before {
        content: "\\F083"; }
      :host ::ng-deep .mdi-battery-charging:before {
        content: "\\F084"; }
      :host ::ng-deep .mdi-battery-charging-100:before {
        content: "\\F085"; }
      :host ::ng-deep .mdi-battery-charging-20:before {
        content: "\\F086"; }
      :host ::ng-deep .mdi-battery-charging-30:before {
        content: "\\F087"; }
      :host ::ng-deep .mdi-battery-charging-40:before {
        content: "\\F088"; }
      :host ::ng-deep .mdi-battery-charging-60:before {
        content: "\\F089"; }
      :host ::ng-deep .mdi-battery-charging-80:before {
        content: "\\F08A"; }
      :host ::ng-deep .mdi-battery-charging-90:before {
        content: "\\F08B"; }
      :host ::ng-deep .mdi-battery-minus:before {
        content: "\\F08C"; }
      :host ::ng-deep .mdi-battery-negative:before {
        content: "\\F08D"; }
      :host ::ng-deep .mdi-battery-outline:before {
        content: "\\F08E"; }
      :host ::ng-deep .mdi-battery-plus:before {
        content: "\\F08F"; }
      :host ::ng-deep .mdi-battery-positive:before {
        content: "\\F090"; }
      :host ::ng-deep .mdi-battery-unknown:before {
        content: "\\F091"; }
      :host ::ng-deep .mdi-beach:before {
        content: "\\F092"; }
      :host ::ng-deep .mdi-beaker:before {
        content: "\\F68F"; }
      :host ::ng-deep .mdi-beats:before {
        content: "\\F097"; }
      :host ::ng-deep .mdi-beer:before {
        content: "\\F098"; }
      :host ::ng-deep .mdi-behance:before {
        content: "\\F099"; }
      :host ::ng-deep .mdi-bell:before {
        content: "\\F09A"; }
      :host ::ng-deep .mdi-bell-off:before {
        content: "\\F09B"; }
      :host ::ng-deep .mdi-bell-outline:before {
        content: "\\F09C"; }
      :host ::ng-deep .mdi-bell-plus:before {
        content: "\\F09D"; }
      :host ::ng-deep .mdi-bell-ring:before {
        content: "\\F09E"; }
      :host ::ng-deep .mdi-bell-ring-outline:before {
        content: "\\F09F"; }
      :host ::ng-deep .mdi-bell-sleep:before {
        content: "\\F0A0"; }
      :host ::ng-deep .mdi-beta:before {
        content: "\\F0A1"; }
      :host ::ng-deep .mdi-bible:before {
        content: "\\F0A2"; }
      :host ::ng-deep .mdi-bike:before {
        content: "\\F0A3"; }
      :host ::ng-deep .mdi-bing:before {
        content: "\\F0A4"; }
      :host ::ng-deep .mdi-binoculars:before {
        content: "\\F0A5"; }
      :host ::ng-deep .mdi-bio:before {
        content: "\\F0A6"; }
      :host ::ng-deep .mdi-biohazard:before {
        content: "\\F0A7"; }
      :host ::ng-deep .mdi-bitbucket:before {
        content: "\\F0A8"; }
      :host ::ng-deep .mdi-black-mesa:before {
        content: "\\F0A9"; }
      :host ::ng-deep .mdi-blackberry:before {
        content: "\\F0AA"; }
      :host ::ng-deep .mdi-blender:before {
        content: "\\F0AB"; }
      :host ::ng-deep .mdi-blinds:before {
        content: "\\F0AC"; }
      :host ::ng-deep .mdi-block-helper:before {
        content: "\\F0AD"; }
      :host ::ng-deep .mdi-blogger:before {
        content: "\\F0AE"; }
      :host ::ng-deep .mdi-bluetooth:before {
        content: "\\F0AF"; }
      :host ::ng-deep .mdi-bluetooth-audio:before {
        content: "\\F0B0"; }
      :host ::ng-deep .mdi-bluetooth-connect:before {
        content: "\\F0B1"; }
      :host ::ng-deep .mdi-bluetooth-off:before {
        content: "\\F0B2"; }
      :host ::ng-deep .mdi-bluetooth-settings:before {
        content: "\\F0B3"; }
      :host ::ng-deep .mdi-bluetooth-transfer:before {
        content: "\\F0B4"; }
      :host ::ng-deep .mdi-blur:before {
        content: "\\F0B5"; }
      :host ::ng-deep .mdi-blur-linear:before {
        content: "\\F0B6"; }
      :host ::ng-deep .mdi-blur-off:before {
        content: "\\F0B7"; }
      :host ::ng-deep .mdi-blur-radial:before {
        content: "\\F0B8"; }
      :host ::ng-deep .mdi-bomb:before {
        content: "\\F690"; }
      :host ::ng-deep .mdi-bomb-off:before {
        content: "\\F6C4"; }
      :host ::ng-deep .mdi-bone:before {
        content: "\\F0B9"; }
      :host ::ng-deep .mdi-book:before {
        content: "\\F0BA"; }
      :host ::ng-deep .mdi-book-minus:before {
        content: "\\F5D9"; }
      :host ::ng-deep .mdi-book-multiple:before {
        content: "\\F0BB"; }
      :host ::ng-deep .mdi-book-multiple-variant:before {
        content: "\\F0BC"; }
      :host ::ng-deep .mdi-book-open:before {
        content: "\\F0BD"; }
      :host ::ng-deep .mdi-book-open-page-variant:before {
        content: "\\F5DA"; }
      :host ::ng-deep .mdi-book-open-variant:before {
        content: "\\F0BE"; }
      :host ::ng-deep .mdi-book-plus:before {
        content: "\\F5DB"; }
      :host ::ng-deep .mdi-book-variant:before {
        content: "\\F0BF"; }
      :host ::ng-deep .mdi-bookmark:before {
        content: "\\F0C0"; }
      :host ::ng-deep .mdi-bookmark-check:before {
        content: "\\F0C1"; }
      :host ::ng-deep .mdi-bookmark-music:before {
        content: "\\F0C2"; }
      :host ::ng-deep .mdi-bookmark-outline:before {
        content: "\\F0C3"; }
      :host ::ng-deep .mdi-bookmark-plus:before {
        content: "\\F0C5"; }
      :host ::ng-deep .mdi-bookmark-plus-outline:before {
        content: "\\F0C4"; }
      :host ::ng-deep .mdi-bookmark-remove:before {
        content: "\\F0C6"; }
      :host ::ng-deep .mdi-boombox:before {
        content: "\\F5DC"; }
      :host ::ng-deep .mdi-bootstrap:before {
        content: "\\F6C5"; }
      :host ::ng-deep .mdi-border-all:before {
        content: "\\F0C7"; }
      :host ::ng-deep .mdi-border-bottom:before {
        content: "\\F0C8"; }
      :host ::ng-deep .mdi-border-color:before {
        content: "\\F0C9"; }
      :host ::ng-deep .mdi-border-horizontal:before {
        content: "\\F0CA"; }
      :host ::ng-deep .mdi-border-inside:before {
        content: "\\F0CB"; }
      :host ::ng-deep .mdi-border-left:before {
        content: "\\F0CC"; }
      :host ::ng-deep .mdi-border-none:before {
        content: "\\F0CD"; }
      :host ::ng-deep .mdi-border-outside:before {
        content: "\\F0CE"; }
      :host ::ng-deep .mdi-border-right:before {
        content: "\\F0CF"; }
      :host ::ng-deep .mdi-border-style:before {
        content: "\\F0D0"; }
      :host ::ng-deep .mdi-border-top:before {
        content: "\\F0D1"; }
      :host ::ng-deep .mdi-border-vertical:before {
        content: "\\F0D2"; }
      :host ::ng-deep .mdi-bow-tie:before {
        content: "\\F677"; }
      :host ::ng-deep .mdi-bowl:before {
        content: "\\F617"; }
      :host ::ng-deep .mdi-bowling:before {
        content: "\\F0D3"; }
      :host ::ng-deep .mdi-box:before {
        content: "\\F0D4"; }
      :host ::ng-deep .mdi-box-cutter:before {
        content: "\\F0D5"; }
      :host ::ng-deep .mdi-box-shadow:before {
        content: "\\F637"; }
      :host ::ng-deep .mdi-bridge:before {
        content: "\\F618"; }
      :host ::ng-deep .mdi-briefcase:before {
        content: "\\F0D6"; }
      :host ::ng-deep .mdi-briefcase-check:before {
        content: "\\F0D7"; }
      :host ::ng-deep .mdi-briefcase-download:before {
        content: "\\F0D8"; }
      :host ::ng-deep .mdi-briefcase-upload:before {
        content: "\\F0D9"; }
      :host ::ng-deep .mdi-brightness-1:before {
        content: "\\F0DA"; }
      :host ::ng-deep .mdi-brightness-2:before {
        content: "\\F0DB"; }
      :host ::ng-deep .mdi-brightness-3:before {
        content: "\\F0DC"; }
      :host ::ng-deep .mdi-brightness-4:before {
        content: "\\F0DD"; }
      :host ::ng-deep .mdi-brightness-5:before {
        content: "\\F0DE"; }
      :host ::ng-deep .mdi-brightness-6:before {
        content: "\\F0DF"; }
      :host ::ng-deep .mdi-brightness-7:before {
        content: "\\F0E0"; }
      :host ::ng-deep .mdi-brightness-auto:before {
        content: "\\F0E1"; }
      :host ::ng-deep .mdi-broom:before {
        content: "\\F0E2"; }
      :host ::ng-deep .mdi-brush:before {
        content: "\\F0E3"; }
      :host ::ng-deep .mdi-buffer:before {
        content: "\\F619"; }
      :host ::ng-deep .mdi-bug:before {
        content: "\\F0E4"; }
      :host ::ng-deep .mdi-bulletin-board:before {
        content: "\\F0E5"; }
      :host ::ng-deep .mdi-bullhorn:before {
        content: "\\F0E6"; }
      :host ::ng-deep .mdi-bullseye:before {
        content: "\\F5DD"; }
      :host ::ng-deep .mdi-burst-mode:before {
        content: "\\F5DE"; }
      :host ::ng-deep .mdi-bus:before {
        content: "\\F0E7"; }
      :host ::ng-deep .mdi-cached:before {
        content: "\\F0E8"; }
      :host ::ng-deep .mdi-cake:before {
        content: "\\F0E9"; }
      :host ::ng-deep .mdi-cake-layered:before {
        content: "\\F0EA"; }
      :host ::ng-deep .mdi-cake-variant:before {
        content: "\\F0EB"; }
      :host ::ng-deep .mdi-calculator:before {
        content: "\\F0EC"; }
      :host ::ng-deep .mdi-calendar:before {
        content: "\\F0ED"; }
      :host ::ng-deep .mdi-calendar-blank:before {
        content: "\\F0EE"; }
      :host ::ng-deep .mdi-calendar-check:before {
        content: "\\F0EF"; }
      :host ::ng-deep .mdi-calendar-clock:before {
        content: "\\F0F0"; }
      :host ::ng-deep .mdi-calendar-multiple:before {
        content: "\\F0F1"; }
      :host ::ng-deep .mdi-calendar-multiple-check:before {
        content: "\\F0F2"; }
      :host ::ng-deep .mdi-calendar-plus:before {
        content: "\\F0F3"; }
      :host ::ng-deep .mdi-calendar-question:before {
        content: "\\F691"; }
      :host ::ng-deep .mdi-calendar-range:before {
        content: "\\F678"; }
      :host ::ng-deep .mdi-calendar-remove:before {
        content: "\\F0F4"; }
      :host ::ng-deep .mdi-calendar-text:before {
        content: "\\F0F5"; }
      :host ::ng-deep .mdi-calendar-today:before {
        content: "\\F0F6"; }
      :host ::ng-deep .mdi-call-made:before {
        content: "\\F0F7"; }
      :host ::ng-deep .mdi-call-merge:before {
        content: "\\F0F8"; }
      :host ::ng-deep .mdi-call-missed:before {
        content: "\\F0F9"; }
      :host ::ng-deep .mdi-call-received:before {
        content: "\\F0FA"; }
      :host ::ng-deep .mdi-call-split:before {
        content: "\\F0FB"; }
      :host ::ng-deep .mdi-camcorder:before {
        content: "\\F0FC"; }
      :host ::ng-deep .mdi-camcorder-box:before {
        content: "\\F0FD"; }
      :host ::ng-deep .mdi-camcorder-box-off:before {
        content: "\\F0FE"; }
      :host ::ng-deep .mdi-camcorder-off:before {
        content: "\\F0FF"; }
      :host ::ng-deep .mdi-camera:before {
        content: "\\F100"; }
      :host ::ng-deep .mdi-camera-burst:before {
        content: "\\F692"; }
      :host ::ng-deep .mdi-camera-enhance:before {
        content: "\\F101"; }
      :host ::ng-deep .mdi-camera-front:before {
        content: "\\F102"; }
      :host ::ng-deep .mdi-camera-front-variant:before {
        content: "\\F103"; }
      :host ::ng-deep .mdi-camera-iris:before {
        content: "\\F104"; }
      :host ::ng-deep .mdi-camera-off:before {
        content: "\\F5DF"; }
      :host ::ng-deep .mdi-camera-party-mode:before {
        content: "\\F105"; }
      :host ::ng-deep .mdi-camera-rear:before {
        content: "\\F106"; }
      :host ::ng-deep .mdi-camera-rear-variant:before {
        content: "\\F107"; }
      :host ::ng-deep .mdi-camera-switch:before {
        content: "\\F108"; }
      :host ::ng-deep .mdi-camera-timer:before {
        content: "\\F109"; }
      :host ::ng-deep .mdi-candle:before {
        content: "\\F5E2"; }
      :host ::ng-deep .mdi-candycane:before {
        content: "\\F10A"; }
      :host ::ng-deep .mdi-car:before {
        content: "\\F10B"; }
      :host ::ng-deep .mdi-car-battery:before {
        content: "\\F10C"; }
      :host ::ng-deep .mdi-car-connected:before {
        content: "\\F10D"; }
      :host ::ng-deep .mdi-car-wash:before {
        content: "\\F10E"; }
      :host ::ng-deep .mdi-cards:before {
        content: "\\F638"; }
      :host ::ng-deep .mdi-cards-outline:before {
        content: "\\F639"; }
      :host ::ng-deep .mdi-cards-playing-outline:before {
        content: "\\F63A"; }
      :host ::ng-deep .mdi-cards-variant:before {
        content: "\\F6C6"; }
      :host ::ng-deep .mdi-carrot:before {
        content: "\\F10F"; }
      :host ::ng-deep .mdi-cart:before {
        content: "\\F110"; }
      :host ::ng-deep .mdi-cart-off:before {
        content: "\\F66B"; }
      :host ::ng-deep .mdi-cart-outline:before {
        content: "\\F111"; }
      :host ::ng-deep .mdi-cart-plus:before {
        content: "\\F112"; }
      :host ::ng-deep .mdi-case-sensitive-alt:before {
        content: "\\F113"; }
      :host ::ng-deep .mdi-cash:before {
        content: "\\F114"; }
      :host ::ng-deep .mdi-cash-100:before {
        content: "\\F115"; }
      :host ::ng-deep .mdi-cash-multiple:before {
        content: "\\F116"; }
      :host ::ng-deep .mdi-cash-usd:before {
        content: "\\F117"; }
      :host ::ng-deep .mdi-cast:before {
        content: "\\F118"; }
      :host ::ng-deep .mdi-cast-connected:before {
        content: "\\F119"; }
      :host ::ng-deep .mdi-castle:before {
        content: "\\F11A"; }
      :host ::ng-deep .mdi-cat:before {
        content: "\\F11B"; }
      :host ::ng-deep .mdi-cellphone:before {
        content: "\\F11C"; }
      :host ::ng-deep .mdi-cellphone-android:before {
        content: "\\F11D"; }
      :host ::ng-deep .mdi-cellphone-basic:before {
        content: "\\F11E"; }
      :host ::ng-deep .mdi-cellphone-dock:before {
        content: "\\F11F"; }
      :host ::ng-deep .mdi-cellphone-iphone:before {
        content: "\\F120"; }
      :host ::ng-deep .mdi-cellphone-link:before {
        content: "\\F121"; }
      :host ::ng-deep .mdi-cellphone-link-off:before {
        content: "\\F122"; }
      :host ::ng-deep .mdi-cellphone-settings:before {
        content: "\\F123"; }
      :host ::ng-deep .mdi-certificate:before {
        content: "\\F124"; }
      :host ::ng-deep .mdi-chair-school:before {
        content: "\\F125"; }
      :host ::ng-deep .mdi-chart-arc:before {
        content: "\\F126"; }
      :host ::ng-deep .mdi-chart-areaspline:before {
        content: "\\F127"; }
      :host ::ng-deep .mdi-chart-bar:before {
        content: "\\F128"; }
      :host ::ng-deep .mdi-chart-bubble:before {
        content: "\\F5E3"; }
      :host ::ng-deep .mdi-chart-gantt:before {
        content: "\\F66C"; }
      :host ::ng-deep .mdi-chart-histogram:before {
        content: "\\F129"; }
      :host ::ng-deep .mdi-chart-line:before {
        content: "\\F12A"; }
      :host ::ng-deep .mdi-chart-pie:before {
        content: "\\F12B"; }
      :host ::ng-deep .mdi-chart-scatterplot-hexbin:before {
        content: "\\F66D"; }
      :host ::ng-deep .mdi-chart-timeline:before {
        content: "\\F66E"; }
      :host ::ng-deep .mdi-check:before {
        content: "\\F12C"; }
      :host ::ng-deep .mdi-check-all:before {
        content: "\\F12D"; }
      :host ::ng-deep .mdi-check-circle:before {
        content: "\\F5E0"; }
      :host ::ng-deep .mdi-check-circle-outline:before {
        content: "\\F5E1"; }
      :host ::ng-deep .mdi-checkbox-blank:before {
        content: "\\F12E"; }
      :host ::ng-deep .mdi-checkbox-blank-circle:before {
        content: "\\F12F"; }
      :host ::ng-deep .mdi-checkbox-blank-circle-outline:before {
        content: "\\F130"; }
      :host ::ng-deep .mdi-checkbox-blank-outline:before {
        content: "\\F131"; }
      :host ::ng-deep .mdi-checkbox-marked:before {
        content: "\\F132"; }
      :host ::ng-deep .mdi-checkbox-marked-circle:before {
        content: "\\F133"; }
      :host ::ng-deep .mdi-checkbox-marked-circle-outline:before {
        content: "\\F134"; }
      :host ::ng-deep .mdi-checkbox-marked-outline:before {
        content: "\\F135"; }
      :host ::ng-deep .mdi-checkbox-multiple-blank:before {
        content: "\\F136"; }
      :host ::ng-deep .mdi-checkbox-multiple-blank-circle:before {
        content: "\\F63B"; }
      :host ::ng-deep .mdi-checkbox-multiple-blank-circle-outline:before {
        content: "\\F63C"; }
      :host ::ng-deep .mdi-checkbox-multiple-blank-outline:before {
        content: "\\F137"; }
      :host ::ng-deep .mdi-checkbox-multiple-marked:before {
        content: "\\F138"; }
      :host ::ng-deep .mdi-checkbox-multiple-marked-circle:before {
        content: "\\F63D"; }
      :host ::ng-deep .mdi-checkbox-multiple-marked-circle-outline:before {
        content: "\\F63E"; }
      :host ::ng-deep .mdi-checkbox-multiple-marked-outline:before {
        content: "\\F139"; }
      :host ::ng-deep .mdi-checkerboard:before {
        content: "\\F13A"; }
      :host ::ng-deep .mdi-chemical-weapon:before {
        content: "\\F13B"; }
      :host ::ng-deep .mdi-chevron-double-down:before {
        content: "\\F13C"; }
      :host ::ng-deep .mdi-chevron-double-left:before {
        content: "\\F13D"; }
      :host ::ng-deep .mdi-chevron-double-right:before {
        content: "\\F13E"; }
      :host ::ng-deep .mdi-chevron-double-up:before {
        content: "\\F13F"; }
      :host ::ng-deep .mdi-chevron-down:before {
        content: "\\F140"; }
      :host ::ng-deep .mdi-chevron-left:before {
        content: "\\F141"; }
      :host ::ng-deep .mdi-chevron-right:before {
        content: "\\F142"; }
      :host ::ng-deep .mdi-chevron-up:before {
        content: "\\F143"; }
      :host ::ng-deep .mdi-chip:before {
        content: "\\F61A"; }
      :host ::ng-deep .mdi-church:before {
        content: "\\F144"; }
      :host ::ng-deep .mdi-cisco-webex:before {
        content: "\\F145"; }
      :host ::ng-deep .mdi-city:before {
        content: "\\F146"; }
      :host ::ng-deep .mdi-clipboard:before {
        content: "\\F147"; }
      :host ::ng-deep .mdi-clipboard-account:before {
        content: "\\F148"; }
      :host ::ng-deep .mdi-clipboard-alert:before {
        content: "\\F149"; }
      :host ::ng-deep .mdi-clipboard-arrow-down:before {
        content: "\\F14A"; }
      :host ::ng-deep .mdi-clipboard-arrow-left:before {
        content: "\\F14B"; }
      :host ::ng-deep .mdi-clipboard-check:before {
        content: "\\F14C"; }
      :host ::ng-deep .mdi-clipboard-flow:before {
        content: "\\F6C7"; }
      :host ::ng-deep .mdi-clipboard-outline:before {
        content: "\\F14D"; }
      :host ::ng-deep .mdi-clipboard-text:before {
        content: "\\F14E"; }
      :host ::ng-deep .mdi-clippy:before {
        content: "\\F14F"; }
      :host ::ng-deep .mdi-clock:before {
        content: "\\F150"; }
      :host ::ng-deep .mdi-clock-alert:before {
        content: "\\F5CE"; }
      :host ::ng-deep .mdi-clock-end:before {
        content: "\\F151"; }
      :host ::ng-deep .mdi-clock-fast:before {
        content: "\\F152"; }
      :host ::ng-deep .mdi-clock-in:before {
        content: "\\F153"; }
      :host ::ng-deep .mdi-clock-out:before {
        content: "\\F154"; }
      :host ::ng-deep .mdi-clock-start:before {
        content: "\\F155"; }
      :host ::ng-deep .mdi-close:before {
        content: "\\F156"; }
      :host ::ng-deep .mdi-close-box:before {
        content: "\\F157"; }
      :host ::ng-deep .mdi-close-box-outline:before {
        content: "\\F158"; }
      :host ::ng-deep .mdi-close-circle:before {
        content: "\\F159"; }
      :host ::ng-deep .mdi-close-circle-outline:before {
        content: "\\F15A"; }
      :host ::ng-deep .mdi-close-network:before {
        content: "\\F15B"; }
      :host ::ng-deep .mdi-close-octagon:before {
        content: "\\F15C"; }
      :host ::ng-deep .mdi-close-octagon-outline:before {
        content: "\\F15D"; }
      :host ::ng-deep .mdi-close-outline:before {
        content: "\\F6C8"; }
      :host ::ng-deep .mdi-closed-caption:before {
        content: "\\F15E"; }
      :host ::ng-deep .mdi-cloud:before {
        content: "\\F15F"; }
      :host ::ng-deep .mdi-cloud-check:before {
        content: "\\F160"; }
      :host ::ng-deep .mdi-cloud-circle:before {
        content: "\\F161"; }
      :host ::ng-deep .mdi-cloud-download:before {
        content: "\\F162"; }
      :host ::ng-deep .mdi-cloud-outline:before {
        content: "\\F163"; }
      :host ::ng-deep .mdi-cloud-outline-off:before {
        content: "\\F164"; }
      :host ::ng-deep .mdi-cloud-print:before {
        content: "\\F165"; }
      :host ::ng-deep .mdi-cloud-print-outline:before {
        content: "\\F166"; }
      :host ::ng-deep .mdi-cloud-sync:before {
        content: "\\F63F"; }
      :host ::ng-deep .mdi-cloud-upload:before {
        content: "\\F167"; }
      :host ::ng-deep .mdi-code-array:before {
        content: "\\F168"; }
      :host ::ng-deep .mdi-code-braces:before {
        content: "\\F169"; }
      :host ::ng-deep .mdi-code-brackets:before {
        content: "\\F16A"; }
      :host ::ng-deep .mdi-code-equal:before {
        content: "\\F16B"; }
      :host ::ng-deep .mdi-code-greater-than:before {
        content: "\\F16C"; }
      :host ::ng-deep .mdi-code-greater-than-or-equal:before {
        content: "\\F16D"; }
      :host ::ng-deep .mdi-code-less-than:before {
        content: "\\F16E"; }
      :host ::ng-deep .mdi-code-less-than-or-equal:before {
        content: "\\F16F"; }
      :host ::ng-deep .mdi-code-not-equal:before {
        content: "\\F170"; }
      :host ::ng-deep .mdi-code-not-equal-variant:before {
        content: "\\F171"; }
      :host ::ng-deep .mdi-code-parentheses:before {
        content: "\\F172"; }
      :host ::ng-deep .mdi-code-string:before {
        content: "\\F173"; }
      :host ::ng-deep .mdi-code-tags:before {
        content: "\\F174"; }
      :host ::ng-deep .mdi-code-tags-check:before {
        content: "\\F693"; }
      :host ::ng-deep .mdi-codepen:before {
        content: "\\F175"; }
      :host ::ng-deep .mdi-coffee:before {
        content: "\\F176"; }
      :host ::ng-deep .mdi-coffee-outline:before {
        content: "\\F6C9"; }
      :host ::ng-deep .mdi-coffee-to-go:before {
        content: "\\F177"; }
      :host ::ng-deep .mdi-coin:before {
        content: "\\F178"; }
      :host ::ng-deep .mdi-coins:before {
        content: "\\F694"; }
      :host ::ng-deep .mdi-collage:before {
        content: "\\F640"; }
      :host ::ng-deep .mdi-color-helper:before {
        content: "\\F179"; }
      :host ::ng-deep .mdi-comment:before {
        content: "\\F17A"; }
      :host ::ng-deep .mdi-comment-account:before {
        content: "\\F17B"; }
      :host ::ng-deep .mdi-comment-account-outline:before {
        content: "\\F17C"; }
      :host ::ng-deep .mdi-comment-alert:before {
        content: "\\F17D"; }
      :host ::ng-deep .mdi-comment-alert-outline:before {
        content: "\\F17E"; }
      :host ::ng-deep .mdi-comment-check:before {
        content: "\\F17F"; }
      :host ::ng-deep .mdi-comment-check-outline:before {
        content: "\\F180"; }
      :host ::ng-deep .mdi-comment-multiple-outline:before {
        content: "\\F181"; }
      :host ::ng-deep .mdi-comment-outline:before {
        content: "\\F182"; }
      :host ::ng-deep .mdi-comment-plus-outline:before {
        content: "\\F183"; }
      :host ::ng-deep .mdi-comment-processing:before {
        content: "\\F184"; }
      :host ::ng-deep .mdi-comment-processing-outline:before {
        content: "\\F185"; }
      :host ::ng-deep .mdi-comment-question-outline:before {
        content: "\\F186"; }
      :host ::ng-deep .mdi-comment-remove-outline:before {
        content: "\\F187"; }
      :host ::ng-deep .mdi-comment-text:before {
        content: "\\F188"; }
      :host ::ng-deep .mdi-comment-text-outline:before {
        content: "\\F189"; }
      :host ::ng-deep .mdi-compare:before {
        content: "\\F18A"; }
      :host ::ng-deep .mdi-compass:before {
        content: "\\F18B"; }
      :host ::ng-deep .mdi-compass-outline:before {
        content: "\\F18C"; }
      :host ::ng-deep .mdi-console:before {
        content: "\\F18D"; }
      :host ::ng-deep .mdi-contact-mail:before {
        content: "\\F18E"; }
      :host ::ng-deep .mdi-contacts:before {
        content: "\\F6CA"; }
      :host ::ng-deep .mdi-content-copy:before {
        content: "\\F18F"; }
      :host ::ng-deep .mdi-content-cut:before {
        content: "\\F190"; }
      :host ::ng-deep .mdi-content-duplicate:before {
        content: "\\F191"; }
      :host ::ng-deep .mdi-content-paste:before {
        content: "\\F192"; }
      :host ::ng-deep .mdi-content-save:before {
        content: "\\F193"; }
      :host ::ng-deep .mdi-content-save-all:before {
        content: "\\F194"; }
      :host ::ng-deep .mdi-content-save-settings:before {
        content: "\\F61B"; }
      :host ::ng-deep .mdi-contrast:before {
        content: "\\F195"; }
      :host ::ng-deep .mdi-contrast-box:before {
        content: "\\F196"; }
      :host ::ng-deep .mdi-contrast-circle:before {
        content: "\\F197"; }
      :host ::ng-deep .mdi-cookie:before {
        content: "\\F198"; }
      :host ::ng-deep .mdi-copyright:before {
        content: "\\F5E6"; }
      :host ::ng-deep .mdi-counter:before {
        content: "\\F199"; }
      :host ::ng-deep .mdi-cow:before {
        content: "\\F19A"; }
      :host ::ng-deep .mdi-creation:before {
        content: "\\F1C9"; }
      :host ::ng-deep .mdi-credit-card:before {
        content: "\\F19B"; }
      :host ::ng-deep .mdi-credit-card-multiple:before {
        content: "\\F19C"; }
      :host ::ng-deep .mdi-credit-card-off:before {
        content: "\\F5E4"; }
      :host ::ng-deep .mdi-credit-card-plus:before {
        content: "\\F675"; }
      :host ::ng-deep .mdi-credit-card-scan:before {
        content: "\\F19D"; }
      :host ::ng-deep .mdi-crop:before {
        content: "\\F19E"; }
      :host ::ng-deep .mdi-crop-free:before {
        content: "\\F19F"; }
      :host ::ng-deep .mdi-crop-landscape:before {
        content: "\\F1A0"; }
      :host ::ng-deep .mdi-crop-portrait:before {
        content: "\\F1A1"; }
      :host ::ng-deep .mdi-crop-rotate:before {
        content: "\\F695"; }
      :host ::ng-deep .mdi-crop-square:before {
        content: "\\F1A2"; }
      :host ::ng-deep .mdi-crosshairs:before {
        content: "\\F1A3"; }
      :host ::ng-deep .mdi-crosshairs-gps:before {
        content: "\\F1A4"; }
      :host ::ng-deep .mdi-crown:before {
        content: "\\F1A5"; }
      :host ::ng-deep .mdi-cube:before {
        content: "\\F1A6"; }
      :host ::ng-deep .mdi-cube-outline:before {
        content: "\\F1A7"; }
      :host ::ng-deep .mdi-cube-send:before {
        content: "\\F1A8"; }
      :host ::ng-deep .mdi-cube-unfolded:before {
        content: "\\F1A9"; }
      :host ::ng-deep .mdi-cup:before {
        content: "\\F1AA"; }
      :host ::ng-deep .mdi-cup-off:before {
        content: "\\F5E5"; }
      :host ::ng-deep .mdi-cup-water:before {
        content: "\\F1AB"; }
      :host ::ng-deep .mdi-currency-btc:before {
        content: "\\F1AC"; }
      :host ::ng-deep .mdi-currency-eur:before {
        content: "\\F1AD"; }
      :host ::ng-deep .mdi-currency-gbp:before {
        content: "\\F1AE"; }
      :host ::ng-deep .mdi-currency-inr:before {
        content: "\\F1AF"; }
      :host ::ng-deep .mdi-currency-ngn:before {
        content: "\\F1B0"; }
      :host ::ng-deep .mdi-currency-rub:before {
        content: "\\F1B1"; }
      :host ::ng-deep .mdi-currency-try:before {
        content: "\\F1B2"; }
      :host ::ng-deep .mdi-currency-usd:before {
        content: "\\F1B3"; }
      :host ::ng-deep .mdi-currency-usd-off:before {
        content: "\\F679"; }
      :host ::ng-deep .mdi-cursor-default:before {
        content: "\\F1B4"; }
      :host ::ng-deep .mdi-cursor-default-outline:before {
        content: "\\F1B5"; }
      :host ::ng-deep .mdi-cursor-move:before {
        content: "\\F1B6"; }
      :host ::ng-deep .mdi-cursor-pointer:before {
        content: "\\F1B7"; }
      :host ::ng-deep .mdi-cursor-text:before {
        content: "\\F5E7"; }
      :host ::ng-deep .mdi-database:before {
        content: "\\F1B8"; }
      :host ::ng-deep .mdi-database-minus:before {
        content: "\\F1B9"; }
      :host ::ng-deep .mdi-database-plus:before {
        content: "\\F1BA"; }
      :host ::ng-deep .mdi-debug-step-into:before {
        content: "\\F1BB"; }
      :host ::ng-deep .mdi-debug-step-out:before {
        content: "\\F1BC"; }
      :host ::ng-deep .mdi-debug-step-over:before {
        content: "\\F1BD"; }
      :host ::ng-deep .mdi-decimal-decrease:before {
        content: "\\F1BE"; }
      :host ::ng-deep .mdi-decimal-increase:before {
        content: "\\F1BF"; }
      :host ::ng-deep .mdi-delete:before {
        content: "\\F1C0"; }
      :host ::ng-deep .mdi-delete-circle:before {
        content: "\\F682"; }
      :host ::ng-deep .mdi-delete-empty:before {
        content: "\\F6CB"; }
      :host ::ng-deep .mdi-delete-forever:before {
        content: "\\F5E8"; }
      :host ::ng-deep .mdi-delete-sweep:before {
        content: "\\F5E9"; }
      :host ::ng-deep .mdi-delete-variant:before {
        content: "\\F1C1"; }
      :host ::ng-deep .mdi-delta:before {
        content: "\\F1C2"; }
      :host ::ng-deep .mdi-deskphone:before {
        content: "\\F1C3"; }
      :host ::ng-deep .mdi-desktop-mac:before {
        content: "\\F1C4"; }
      :host ::ng-deep .mdi-desktop-tower:before {
        content: "\\F1C5"; }
      :host ::ng-deep .mdi-details:before {
        content: "\\F1C6"; }
      :host ::ng-deep .mdi-developer-board:before {
        content: "\\F696"; }
      :host ::ng-deep .mdi-deviantart:before {
        content: "\\F1C7"; }
      :host ::ng-deep .mdi-dialpad:before {
        content: "\\F61C"; }
      :host ::ng-deep .mdi-diamond:before {
        content: "\\F1C8"; }
      :host ::ng-deep .mdi-dice-1:before {
        content: "\\F1CA"; }
      :host ::ng-deep .mdi-dice-2:before {
        content: "\\F1CB"; }
      :host ::ng-deep .mdi-dice-3:before {
        content: "\\F1CC"; }
      :host ::ng-deep .mdi-dice-4:before {
        content: "\\F1CD"; }
      :host ::ng-deep .mdi-dice-5:before {
        content: "\\F1CE"; }
      :host ::ng-deep .mdi-dice-6:before {
        content: "\\F1CF"; }
      :host ::ng-deep .mdi-dice-d20:before {
        content: "\\F5EA"; }
      :host ::ng-deep .mdi-dice-d4:before {
        content: "\\F5EB"; }
      :host ::ng-deep .mdi-dice-d6:before {
        content: "\\F5EC"; }
      :host ::ng-deep .mdi-dice-d8:before {
        content: "\\F5ED"; }
      :host ::ng-deep .mdi-dictionary:before {
        content: "\\F61D"; }
      :host ::ng-deep .mdi-directions:before {
        content: "\\F1D0"; }
      :host ::ng-deep .mdi-directions-fork:before {
        content: "\\F641"; }
      :host ::ng-deep .mdi-discord:before {
        content: "\\F66F"; }
      :host ::ng-deep .mdi-disk:before {
        content: "\\F5EE"; }
      :host ::ng-deep .mdi-disk-alert:before {
        content: "\\F1D1"; }
      :host ::ng-deep .mdi-disqus:before {
        content: "\\F1D2"; }
      :host ::ng-deep .mdi-disqus-outline:before {
        content: "\\F1D3"; }
      :host ::ng-deep .mdi-division:before {
        content: "\\F1D4"; }
      :host ::ng-deep .mdi-division-box:before {
        content: "\\F1D5"; }
      :host ::ng-deep .mdi-dna:before {
        content: "\\F683"; }
      :host ::ng-deep .mdi-dns:before {
        content: "\\F1D6"; }
      :host ::ng-deep .mdi-do-not-disturb:before {
        content: "\\F697"; }
      :host ::ng-deep .mdi-do-not-disturb-off:before {
        content: "\\F698"; }
      :host ::ng-deep .mdi-dolby:before {
        content: "\\F6B2"; }
      :host ::ng-deep .mdi-domain:before {
        content: "\\F1D7"; }
      :host ::ng-deep .mdi-dots-horizontal:before {
        content: "\\F1D8"; }
      :host ::ng-deep .mdi-dots-vertical:before {
        content: "\\F1D9"; }
      :host ::ng-deep .mdi-douban:before {
        content: "\\F699"; }
      :host ::ng-deep .mdi-download:before {
        content: "\\F1DA"; }
      :host ::ng-deep .mdi-drag:before {
        content: "\\F1DB"; }
      :host ::ng-deep .mdi-drag-horizontal:before {
        content: "\\F1DC"; }
      :host ::ng-deep .mdi-drag-vertical:before {
        content: "\\F1DD"; }
      :host ::ng-deep .mdi-drawing:before {
        content: "\\F1DE"; }
      :host ::ng-deep .mdi-drawing-box:before {
        content: "\\F1DF"; }
      :host ::ng-deep .mdi-dribbble:before {
        content: "\\F1E0"; }
      :host ::ng-deep .mdi-dribbble-box:before {
        content: "\\F1E1"; }
      :host ::ng-deep .mdi-drone:before {
        content: "\\F1E2"; }
      :host ::ng-deep .mdi-dropbox:before {
        content: "\\F1E3"; }
      :host ::ng-deep .mdi-drupal:before {
        content: "\\F1E4"; }
      :host ::ng-deep .mdi-duck:before {
        content: "\\F1E5"; }
      :host ::ng-deep .mdi-dumbbell:before {
        content: "\\F1E6"; }
      :host ::ng-deep .mdi-earth:before {
        content: "\\F1E7"; }
      :host ::ng-deep .mdi-earth-box:before {
        content: "\\F6CC"; }
      :host ::ng-deep .mdi-earth-box-off:before {
        content: "\\F6CD"; }
      :host ::ng-deep .mdi-earth-off:before {
        content: "\\F1E8"; }
      :host ::ng-deep .mdi-edge:before {
        content: "\\F1E9"; }
      :host ::ng-deep .mdi-eject:before {
        content: "\\F1EA"; }
      :host ::ng-deep .mdi-elevation-decline:before {
        content: "\\F1EB"; }
      :host ::ng-deep .mdi-elevation-rise:before {
        content: "\\F1EC"; }
      :host ::ng-deep .mdi-elevator:before {
        content: "\\F1ED"; }
      :host ::ng-deep .mdi-email:before {
        content: "\\F1EE"; }
      :host ::ng-deep .mdi-email-alert:before {
        content: "\\F6CE"; }
      :host ::ng-deep .mdi-email-open:before {
        content: "\\F1EF"; }
      :host ::ng-deep .mdi-email-open-outline:before {
        content: "\\F5EF"; }
      :host ::ng-deep .mdi-email-outline:before {
        content: "\\F1F0"; }
      :host ::ng-deep .mdi-email-secure:before {
        content: "\\F1F1"; }
      :host ::ng-deep .mdi-email-variant:before {
        content: "\\F5F0"; }
      :host ::ng-deep .mdi-emby:before {
        content: "\\F6B3"; }
      :host ::ng-deep .mdi-emoticon:before {
        content: "\\F1F2"; }
      :host ::ng-deep .mdi-emoticon-cool:before {
        content: "\\F1F3"; }
      :host ::ng-deep .mdi-emoticon-dead:before {
        content: "\\F69A"; }
      :host ::ng-deep .mdi-emoticon-devil:before {
        content: "\\F1F4"; }
      :host ::ng-deep .mdi-emoticon-excited:before {
        content: "\\F69B"; }
      :host ::ng-deep .mdi-emoticon-happy:before {
        content: "\\F1F5"; }
      :host ::ng-deep .mdi-emoticon-neutral:before {
        content: "\\F1F6"; }
      :host ::ng-deep .mdi-emoticon-poop:before {
        content: "\\F1F7"; }
      :host ::ng-deep .mdi-emoticon-sad:before {
        content: "\\F1F8"; }
      :host ::ng-deep .mdi-emoticon-tongue:before {
        content: "\\F1F9"; }
      :host ::ng-deep .mdi-engine:before {
        content: "\\F1FA"; }
      :host ::ng-deep .mdi-engine-outline:before {
        content: "\\F1FB"; }
      :host ::ng-deep .mdi-equal:before {
        content: "\\F1FC"; }
      :host ::ng-deep .mdi-equal-box:before {
        content: "\\F1FD"; }
      :host ::ng-deep .mdi-eraser:before {
        content: "\\F1FE"; }
      :host ::ng-deep .mdi-eraser-variant:before {
        content: "\\F642"; }
      :host ::ng-deep .mdi-escalator:before {
        content: "\\F1FF"; }
      :host ::ng-deep .mdi-ethernet:before {
        content: "\\F200"; }
      :host ::ng-deep .mdi-ethernet-cable:before {
        content: "\\F201"; }
      :host ::ng-deep .mdi-ethernet-cable-off:before {
        content: "\\F202"; }
      :host ::ng-deep .mdi-etsy:before {
        content: "\\F203"; }
      :host ::ng-deep .mdi-ev-station:before {
        content: "\\F5F1"; }
      :host ::ng-deep .mdi-evernote:before {
        content: "\\F204"; }
      :host ::ng-deep .mdi-exclamation:before {
        content: "\\F205"; }
      :host ::ng-deep .mdi-exit-to-app:before {
        content: "\\F206"; }
      :host ::ng-deep .mdi-export:before {
        content: "\\F207"; }
      :host ::ng-deep .mdi-eye:before {
        content: "\\F208"; }
      :host ::ng-deep .mdi-eye-off:before {
        content: "\\F209"; }
      :host ::ng-deep .mdi-eye-outline:before {
        content: "\\F6CF"; }
      :host ::ng-deep .mdi-eye-outline-off:before {
        content: "\\F6D0"; }
      :host ::ng-deep .mdi-eyedropper:before {
        content: "\\F20A"; }
      :host ::ng-deep .mdi-eyedropper-variant:before {
        content: "\\F20B"; }
      :host ::ng-deep .mdi-face:before {
        content: "\\F643"; }
      :host ::ng-deep .mdi-face-profile:before {
        content: "\\F644"; }
      :host ::ng-deep .mdi-facebook:before {
        content: "\\F20C"; }
      :host ::ng-deep .mdi-facebook-box:before {
        content: "\\F20D"; }
      :host ::ng-deep .mdi-facebook-messenger:before {
        content: "\\F20E"; }
      :host ::ng-deep .mdi-factory:before {
        content: "\\F20F"; }
      :host ::ng-deep .mdi-fan:before {
        content: "\\F210"; }
      :host ::ng-deep .mdi-fast-forward:before {
        content: "\\F211"; }
      :host ::ng-deep .mdi-fast-forward-outline:before {
        content: "\\F6D1"; }
      :host ::ng-deep .mdi-fax:before {
        content: "\\F212"; }
      :host ::ng-deep .mdi-feather:before {
        content: "\\F6D2"; }
      :host ::ng-deep .mdi-ferry:before {
        content: "\\F213"; }
      :host ::ng-deep .mdi-file:before {
        content: "\\F214"; }
      :host ::ng-deep .mdi-file-chart:before {
        content: "\\F215"; }
      :host ::ng-deep .mdi-file-check:before {
        content: "\\F216"; }
      :host ::ng-deep .mdi-file-cloud:before {
        content: "\\F217"; }
      :host ::ng-deep .mdi-file-delimited:before {
        content: "\\F218"; }
      :host ::ng-deep .mdi-file-document:before {
        content: "\\F219"; }
      :host ::ng-deep .mdi-file-document-box:before {
        content: "\\F21A"; }
      :host ::ng-deep .mdi-file-excel:before {
        content: "\\F21B"; }
      :host ::ng-deep .mdi-file-excel-box:before {
        content: "\\F21C"; }
      :host ::ng-deep .mdi-file-export:before {
        content: "\\F21D"; }
      :host ::ng-deep .mdi-file-find:before {
        content: "\\F21E"; }
      :host ::ng-deep .mdi-file-hidden:before {
        content: "\\F613"; }
      :host ::ng-deep .mdi-file-image:before {
        content: "\\F21F"; }
      :host ::ng-deep .mdi-file-import:before {
        content: "\\F220"; }
      :host ::ng-deep .mdi-file-lock:before {
        content: "\\F221"; }
      :host ::ng-deep .mdi-file-multiple:before {
        content: "\\F222"; }
      :host ::ng-deep .mdi-file-music:before {
        content: "\\F223"; }
      :host ::ng-deep .mdi-file-outline:before {
        content: "\\F224"; }
      :host ::ng-deep .mdi-file-pdf:before {
        content: "\\F225"; }
      :host ::ng-deep .mdi-file-pdf-box:before {
        content: "\\F226"; }
      :host ::ng-deep .mdi-file-powerpoint:before {
        content: "\\F227"; }
      :host ::ng-deep .mdi-file-powerpoint-box:before {
        content: "\\F228"; }
      :host ::ng-deep .mdi-file-presentation-box:before {
        content: "\\F229"; }
      :host ::ng-deep .mdi-file-restore:before {
        content: "\\F670"; }
      :host ::ng-deep .mdi-file-send:before {
        content: "\\F22A"; }
      :host ::ng-deep .mdi-file-tree:before {
        content: "\\F645"; }
      :host ::ng-deep .mdi-file-video:before {
        content: "\\F22B"; }
      :host ::ng-deep .mdi-file-word:before {
        content: "\\F22C"; }
      :host ::ng-deep .mdi-file-word-box:before {
        content: "\\F22D"; }
      :host ::ng-deep .mdi-file-xml:before {
        content: "\\F22E"; }
      :host ::ng-deep .mdi-film:before {
        content: "\\F22F"; }
      :host ::ng-deep .mdi-filmstrip:before {
        content: "\\F230"; }
      :host ::ng-deep .mdi-filmstrip-off:before {
        content: "\\F231"; }
      :host ::ng-deep .mdi-filter:before {
        content: "\\F232"; }
      :host ::ng-deep .mdi-filter-outline:before {
        content: "\\F233"; }
      :host ::ng-deep .mdi-filter-remove:before {
        content: "\\F234"; }
      :host ::ng-deep .mdi-filter-remove-outline:before {
        content: "\\F235"; }
      :host ::ng-deep .mdi-filter-variant:before {
        content: "\\F236"; }
      :host ::ng-deep .mdi-find-replace:before {
        content: "\\F6D3"; }
      :host ::ng-deep .mdi-fingerprint:before {
        content: "\\F237"; }
      :host ::ng-deep .mdi-fire:before {
        content: "\\F238"; }
      :host ::ng-deep .mdi-firefox:before {
        content: "\\F239"; }
      :host ::ng-deep .mdi-fish:before {
        content: "\\F23A"; }
      :host ::ng-deep .mdi-flag:before {
        content: "\\F23B"; }
      :host ::ng-deep .mdi-flag-checkered:before {
        content: "\\F23C"; }
      :host ::ng-deep .mdi-flag-outline:before {
        content: "\\F23D"; }
      :host ::ng-deep .mdi-flag-outline-variant:before {
        content: "\\F23E"; }
      :host ::ng-deep .mdi-flag-triangle:before {
        content: "\\F23F"; }
      :host ::ng-deep .mdi-flag-variant:before {
        content: "\\F240"; }
      :host ::ng-deep .mdi-flash:before {
        content: "\\F241"; }
      :host ::ng-deep .mdi-flash-auto:before {
        content: "\\F242"; }
      :host ::ng-deep .mdi-flash-off:before {
        content: "\\F243"; }
      :host ::ng-deep .mdi-flash-outline:before {
        content: "\\F6D4"; }
      :host ::ng-deep .mdi-flash-red-eye:before {
        content: "\\F67A"; }
      :host ::ng-deep .mdi-flashlight:before {
        content: "\\F244"; }
      :host ::ng-deep .mdi-flashlight-off:before {
        content: "\\F245"; }
      :host ::ng-deep .mdi-flask:before {
        content: "\\F093"; }
      :host ::ng-deep .mdi-flask-empty:before {
        content: "\\F094"; }
      :host ::ng-deep .mdi-flask-empty-outline:before {
        content: "\\F095"; }
      :host ::ng-deep .mdi-flask-outline:before {
        content: "\\F096"; }
      :host ::ng-deep .mdi-flattr:before {
        content: "\\F246"; }
      :host ::ng-deep .mdi-flip-to-back:before {
        content: "\\F247"; }
      :host ::ng-deep .mdi-flip-to-front:before {
        content: "\\F248"; }
      :host ::ng-deep .mdi-floppy:before {
        content: "\\F249"; }
      :host ::ng-deep .mdi-flower:before {
        content: "\\F24A"; }
      :host ::ng-deep .mdi-folder:before {
        content: "\\F24B"; }
      :host ::ng-deep .mdi-folder-account:before {
        content: "\\F24C"; }
      :host ::ng-deep .mdi-folder-download:before {
        content: "\\F24D"; }
      :host ::ng-deep .mdi-folder-google-drive:before {
        content: "\\F24E"; }
      :host ::ng-deep .mdi-folder-image:before {
        content: "\\F24F"; }
      :host ::ng-deep .mdi-folder-lock:before {
        content: "\\F250"; }
      :host ::ng-deep .mdi-folder-lock-open:before {
        content: "\\F251"; }
      :host ::ng-deep .mdi-folder-move:before {
        content: "\\F252"; }
      :host ::ng-deep .mdi-folder-multiple:before {
        content: "\\F253"; }
      :host ::ng-deep .mdi-folder-multiple-image:before {
        content: "\\F254"; }
      :host ::ng-deep .mdi-folder-multiple-outline:before {
        content: "\\F255"; }
      :host ::ng-deep .mdi-folder-outline:before {
        content: "\\F256"; }
      :host ::ng-deep .mdi-folder-plus:before {
        content: "\\F257"; }
      :host ::ng-deep .mdi-folder-remove:before {
        content: "\\F258"; }
      :host ::ng-deep .mdi-folder-star:before {
        content: "\\F69C"; }
      :host ::ng-deep .mdi-folder-upload:before {
        content: "\\F259"; }
      :host ::ng-deep .mdi-font-awesome:before {
        content: "\\F03A"; }
      :host ::ng-deep .mdi-food:before {
        content: "\\F25A"; }
      :host ::ng-deep .mdi-food-apple:before {
        content: "\\F25B"; }
      :host ::ng-deep .mdi-food-fork-drink:before {
        content: "\\F5F2"; }
      :host ::ng-deep .mdi-food-off:before {
        content: "\\F5F3"; }
      :host ::ng-deep .mdi-food-variant:before {
        content: "\\F25C"; }
      :host ::ng-deep .mdi-football:before {
        content: "\\F25D"; }
      :host ::ng-deep .mdi-football-australian:before {
        content: "\\F25E"; }
      :host ::ng-deep .mdi-football-helmet:before {
        content: "\\F25F"; }
      :host ::ng-deep .mdi-format-align-center:before {
        content: "\\F260"; }
      :host ::ng-deep .mdi-format-align-justify:before {
        content: "\\F261"; }
      :host ::ng-deep .mdi-format-align-left:before {
        content: "\\F262"; }
      :host ::ng-deep .mdi-format-align-right:before {
        content: "\\F263"; }
      :host ::ng-deep .mdi-format-annotation-plus:before {
        content: "\\F646"; }
      :host ::ng-deep .mdi-format-bold:before {
        content: "\\F264"; }
      :host ::ng-deep .mdi-format-clear:before {
        content: "\\F265"; }
      :host ::ng-deep .mdi-format-color-fill:before {
        content: "\\F266"; }
      :host ::ng-deep .mdi-format-color-text:before {
        content: "\\F69D"; }
      :host ::ng-deep .mdi-format-float-center:before {
        content: "\\F267"; }
      :host ::ng-deep .mdi-format-float-left:before {
        content: "\\F268"; }
      :host ::ng-deep .mdi-format-float-none:before {
        content: "\\F269"; }
      :host ::ng-deep .mdi-format-float-right:before {
        content: "\\F26A"; }
      :host ::ng-deep .mdi-format-font:before {
        content: "\\F6D5"; }
      :host ::ng-deep .mdi-format-header-1:before {
        content: "\\F26B"; }
      :host ::ng-deep .mdi-format-header-2:before {
        content: "\\F26C"; }
      :host ::ng-deep .mdi-format-header-3:before {
        content: "\\F26D"; }
      :host ::ng-deep .mdi-format-header-4:before {
        content: "\\F26E"; }
      :host ::ng-deep .mdi-format-header-5:before {
        content: "\\F26F"; }
      :host ::ng-deep .mdi-format-header-6:before {
        content: "\\F270"; }
      :host ::ng-deep .mdi-format-header-decrease:before {
        content: "\\F271"; }
      :host ::ng-deep .mdi-format-header-equal:before {
        content: "\\F272"; }
      :host ::ng-deep .mdi-format-header-increase:before {
        content: "\\F273"; }
      :host ::ng-deep .mdi-format-header-pound:before {
        content: "\\F274"; }
      :host ::ng-deep .mdi-format-horizontal-align-center:before {
        content: "\\F61E"; }
      :host ::ng-deep .mdi-format-horizontal-align-left:before {
        content: "\\F61F"; }
      :host ::ng-deep .mdi-format-horizontal-align-right:before {
        content: "\\F620"; }
      :host ::ng-deep .mdi-format-indent-decrease:before {
        content: "\\F275"; }
      :host ::ng-deep .mdi-format-indent-increase:before {
        content: "\\F276"; }
      :host ::ng-deep .mdi-format-italic:before {
        content: "\\F277"; }
      :host ::ng-deep .mdi-format-line-spacing:before {
        content: "\\F278"; }
      :host ::ng-deep .mdi-format-line-style:before {
        content: "\\F5C8"; }
      :host ::ng-deep .mdi-format-line-weight:before {
        content: "\\F5C9"; }
      :host ::ng-deep .mdi-format-list-bulleted:before {
        content: "\\F279"; }
      :host ::ng-deep .mdi-format-list-bulleted-type:before {
        content: "\\F27A"; }
      :host ::ng-deep .mdi-format-list-numbers:before {
        content: "\\F27B"; }
      :host ::ng-deep .mdi-format-page-break:before {
        content: "\\F6D6"; }
      :host ::ng-deep .mdi-format-paint:before {
        content: "\\F27C"; }
      :host ::ng-deep .mdi-format-paragraph:before {
        content: "\\F27D"; }
      :host ::ng-deep .mdi-format-pilcrow:before {
        content: "\\F6D7"; }
      :host ::ng-deep .mdi-format-quote:before {
        content: "\\F27E"; }
      :host ::ng-deep .mdi-format-rotate-90:before {
        content: "\\F6A9"; }
      :host ::ng-deep .mdi-format-section:before {
        content: "\\F69E"; }
      :host ::ng-deep .mdi-format-size:before {
        content: "\\F27F"; }
      :host ::ng-deep .mdi-format-strikethrough:before {
        content: "\\F280"; }
      :host ::ng-deep .mdi-format-strikethrough-variant:before {
        content: "\\F281"; }
      :host ::ng-deep .mdi-format-subscript:before {
        content: "\\F282"; }
      :host ::ng-deep .mdi-format-superscript:before {
        content: "\\F283"; }
      :host ::ng-deep .mdi-format-text:before {
        content: "\\F284"; }
      :host ::ng-deep .mdi-format-textdirection-l-to-r:before {
        content: "\\F285"; }
      :host ::ng-deep .mdi-format-textdirection-r-to-l:before {
        content: "\\F286"; }
      :host ::ng-deep .mdi-format-title:before {
        content: "\\F5F4"; }
      :host ::ng-deep .mdi-format-underline:before {
        content: "\\F287"; }
      :host ::ng-deep .mdi-format-vertical-align-bottom:before {
        content: "\\F621"; }
      :host ::ng-deep .mdi-format-vertical-align-center:before {
        content: "\\F622"; }
      :host ::ng-deep .mdi-format-vertical-align-top:before {
        content: "\\F623"; }
      :host ::ng-deep .mdi-format-wrap-inline:before {
        content: "\\F288"; }
      :host ::ng-deep .mdi-format-wrap-square:before {
        content: "\\F289"; }
      :host ::ng-deep .mdi-format-wrap-tight:before {
        content: "\\F28A"; }
      :host ::ng-deep .mdi-format-wrap-top-bottom:before {
        content: "\\F28B"; }
      :host ::ng-deep .mdi-forum:before {
        content: "\\F28C"; }
      :host ::ng-deep .mdi-forward:before {
        content: "\\F28D"; }
      :host ::ng-deep .mdi-foursquare:before {
        content: "\\F28E"; }
      :host ::ng-deep .mdi-fridge:before {
        content: "\\F28F"; }
      :host ::ng-deep .mdi-fridge-filled:before {
        content: "\\F290"; }
      :host ::ng-deep .mdi-fridge-filled-bottom:before {
        content: "\\F291"; }
      :host ::ng-deep .mdi-fridge-filled-top:before {
        content: "\\F292"; }
      :host ::ng-deep .mdi-fullscreen:before {
        content: "\\F293"; }
      :host ::ng-deep .mdi-fullscreen-exit:before {
        content: "\\F294"; }
      :host ::ng-deep .mdi-function:before {
        content: "\\F295"; }
      :host ::ng-deep .mdi-gamepad:before {
        content: "\\F296"; }
      :host ::ng-deep .mdi-gamepad-variant:before {
        content: "\\F297"; }
      :host ::ng-deep .mdi-garage:before {
        content: "\\F6D8"; }
      :host ::ng-deep .mdi-garage-open:before {
        content: "\\F6D9"; }
      :host ::ng-deep .mdi-gas-cylinder:before {
        content: "\\F647"; }
      :host ::ng-deep .mdi-gas-station:before {
        content: "\\F298"; }
      :host ::ng-deep .mdi-gate:before {
        content: "\\F299"; }
      :host ::ng-deep .mdi-gauge:before {
        content: "\\F29A"; }
      :host ::ng-deep .mdi-gavel:before {
        content: "\\F29B"; }
      :host ::ng-deep .mdi-gender-female:before {
        content: "\\F29C"; }
      :host ::ng-deep .mdi-gender-male:before {
        content: "\\F29D"; }
      :host ::ng-deep .mdi-gender-male-female:before {
        content: "\\F29E"; }
      :host ::ng-deep .mdi-gender-transgender:before {
        content: "\\F29F"; }
      :host ::ng-deep .mdi-ghost:before {
        content: "\\F2A0"; }
      :host ::ng-deep .mdi-gift:before {
        content: "\\F2A1"; }
      :host ::ng-deep .mdi-git:before {
        content: "\\F2A2"; }
      :host ::ng-deep .mdi-github-box:before {
        content: "\\F2A3"; }
      :host ::ng-deep .mdi-github-circle:before {
        content: "\\F2A4"; }
      :host ::ng-deep .mdi-github-face:before {
        content: "\\F6DA"; }
      :host ::ng-deep .mdi-glass-flute:before {
        content: "\\F2A5"; }
      :host ::ng-deep .mdi-glass-mug:before {
        content: "\\F2A6"; }
      :host ::ng-deep .mdi-glass-stange:before {
        content: "\\F2A7"; }
      :host ::ng-deep .mdi-glass-tulip:before {
        content: "\\F2A8"; }
      :host ::ng-deep .mdi-glassdoor:before {
        content: "\\F2A9"; }
      :host ::ng-deep .mdi-glasses:before {
        content: "\\F2AA"; }
      :host ::ng-deep .mdi-gmail:before {
        content: "\\F2AB"; }
      :host ::ng-deep .mdi-gnome:before {
        content: "\\F2AC"; }
      :host ::ng-deep .mdi-gondola:before {
        content: "\\F685"; }
      :host ::ng-deep .mdi-google:before {
        content: "\\F2AD"; }
      :host ::ng-deep .mdi-google-cardboard:before {
        content: "\\F2AE"; }
      :host ::ng-deep .mdi-google-chrome:before {
        content: "\\F2AF"; }
      :host ::ng-deep .mdi-google-circles:before {
        content: "\\F2B0"; }
      :host ::ng-deep .mdi-google-circles-communities:before {
        content: "\\F2B1"; }
      :host ::ng-deep .mdi-google-circles-extended:before {
        content: "\\F2B2"; }
      :host ::ng-deep .mdi-google-circles-group:before {
        content: "\\F2B3"; }
      :host ::ng-deep .mdi-google-controller:before {
        content: "\\F2B4"; }
      :host ::ng-deep .mdi-google-controller-off:before {
        content: "\\F2B5"; }
      :host ::ng-deep .mdi-google-drive:before {
        content: "\\F2B6"; }
      :host ::ng-deep .mdi-google-earth:before {
        content: "\\F2B7"; }
      :host ::ng-deep .mdi-google-glass:before {
        content: "\\F2B8"; }
      :host ::ng-deep .mdi-google-keep:before {
        content: "\\F6DB"; }
      :host ::ng-deep .mdi-google-maps:before {
        content: "\\F5F5"; }
      :host ::ng-deep .mdi-google-nearby:before {
        content: "\\F2B9"; }
      :host ::ng-deep .mdi-google-pages:before {
        content: "\\F2BA"; }
      :host ::ng-deep .mdi-google-photos:before {
        content: "\\F6DC"; }
      :host ::ng-deep .mdi-google-physical-web:before {
        content: "\\F2BB"; }
      :host ::ng-deep .mdi-google-play:before {
        content: "\\F2BC"; }
      :host ::ng-deep .mdi-google-plus:before {
        content: "\\F2BD"; }
      :host ::ng-deep .mdi-google-plus-box:before {
        content: "\\F2BE"; }
      :host ::ng-deep .mdi-google-translate:before {
        content: "\\F2BF"; }
      :host ::ng-deep .mdi-google-wallet:before {
        content: "\\F2C0"; }
      :host ::ng-deep .mdi-gradient:before {
        content: "\\F69F"; }
      :host ::ng-deep .mdi-grease-pencil:before {
        content: "\\F648"; }
      :host ::ng-deep .mdi-grid:before {
        content: "\\F2C1"; }
      :host ::ng-deep .mdi-grid-off:before {
        content: "\\F2C2"; }
      :host ::ng-deep .mdi-group:before {
        content: "\\F2C3"; }
      :host ::ng-deep .mdi-guitar-electric:before {
        content: "\\F2C4"; }
      :host ::ng-deep .mdi-guitar-pick:before {
        content: "\\F2C5"; }
      :host ::ng-deep .mdi-guitar-pick-outline:before {
        content: "\\F2C6"; }
      :host ::ng-deep .mdi-hackernews:before {
        content: "\\F624"; }
      :host ::ng-deep .mdi-hamburger:before {
        content: "\\F684"; }
      :host ::ng-deep .mdi-hand-pointing-right:before {
        content: "\\F2C7"; }
      :host ::ng-deep .mdi-hanger:before {
        content: "\\F2C8"; }
      :host ::ng-deep .mdi-hangouts:before {
        content: "\\F2C9"; }
      :host ::ng-deep .mdi-harddisk:before {
        content: "\\F2CA"; }
      :host ::ng-deep .mdi-headphones:before {
        content: "\\F2CB"; }
      :host ::ng-deep .mdi-headphones-box:before {
        content: "\\F2CC"; }
      :host ::ng-deep .mdi-headphones-settings:before {
        content: "\\F2CD"; }
      :host ::ng-deep .mdi-headset:before {
        content: "\\F2CE"; }
      :host ::ng-deep .mdi-headset-dock:before {
        content: "\\F2CF"; }
      :host ::ng-deep .mdi-headset-off:before {
        content: "\\F2D0"; }
      :host ::ng-deep .mdi-heart:before {
        content: "\\F2D1"; }
      :host ::ng-deep .mdi-heart-box:before {
        content: "\\F2D2"; }
      :host ::ng-deep .mdi-heart-box-outline:before {
        content: "\\F2D3"; }
      :host ::ng-deep .mdi-heart-broken:before {
        content: "\\F2D4"; }
      :host ::ng-deep .mdi-heart-half-outline:before {
        content: "\\F6DD"; }
      :host ::ng-deep .mdi-heart-half-part:before {
        content: "\\F6DE"; }
      :host ::ng-deep .mdi-heart-half-part-outline:before {
        content: "\\F6DF"; }
      :host ::ng-deep .mdi-heart-outline:before {
        content: "\\F2D5"; }
      :host ::ng-deep .mdi-heart-pulse:before {
        content: "\\F5F6"; }
      :host ::ng-deep .mdi-help:before {
        content: "\\F2D6"; }
      :host ::ng-deep .mdi-help-circle:before {
        content: "\\F2D7"; }
      :host ::ng-deep .mdi-help-circle-outline:before {
        content: "\\F625"; }
      :host ::ng-deep .mdi-hexagon:before {
        content: "\\F2D8"; }
      :host ::ng-deep .mdi-hexagon-multiple:before {
        content: "\\F6E0"; }
      :host ::ng-deep .mdi-hexagon-outline:before {
        content: "\\F2D9"; }
      :host ::ng-deep .mdi-highway:before {
        content: "\\F5F7"; }
      :host ::ng-deep .mdi-history:before {
        content: "\\F2DA"; }
      :host ::ng-deep .mdi-hololens:before {
        content: "\\F2DB"; }
      :host ::ng-deep .mdi-home:before {
        content: "\\F2DC"; }
      :host ::ng-deep .mdi-home-map-marker:before {
        content: "\\F5F8"; }
      :host ::ng-deep .mdi-home-modern:before {
        content: "\\F2DD"; }
      :host ::ng-deep .mdi-home-outline:before {
        content: "\\F6A0"; }
      :host ::ng-deep .mdi-home-variant:before {
        content: "\\F2DE"; }
      :host ::ng-deep .mdi-hook:before {
        content: "\\F6E1"; }
      :host ::ng-deep .mdi-hook-off:before {
        content: "\\F6E2"; }
      :host ::ng-deep .mdi-hops:before {
        content: "\\F2DF"; }
      :host ::ng-deep .mdi-hospital:before {
        content: "\\F2E0"; }
      :host ::ng-deep .mdi-hospital-building:before {
        content: "\\F2E1"; }
      :host ::ng-deep .mdi-hospital-marker:before {
        content: "\\F2E2"; }
      :host ::ng-deep .mdi-hotel:before {
        content: "\\F2E3"; }
      :host ::ng-deep .mdi-houzz:before {
        content: "\\F2E4"; }
      :host ::ng-deep .mdi-houzz-box:before {
        content: "\\F2E5"; }
      :host ::ng-deep .mdi-human:before {
        content: "\\F2E6"; }
      :host ::ng-deep .mdi-human-child:before {
        content: "\\F2E7"; }
      :host ::ng-deep .mdi-human-female:before {
        content: "\\F649"; }
      :host ::ng-deep .mdi-human-greeting:before {
        content: "\\F64A"; }
      :host ::ng-deep .mdi-human-handsdown:before {
        content: "\\F64B"; }
      :host ::ng-deep .mdi-human-handsup:before {
        content: "\\F64C"; }
      :host ::ng-deep .mdi-human-male:before {
        content: "\\F64D"; }
      :host ::ng-deep .mdi-human-male-female:before {
        content: "\\F2E8"; }
      :host ::ng-deep .mdi-human-pregnant:before {
        content: "\\F5CF"; }
      :host ::ng-deep .mdi-image:before {
        content: "\\F2E9"; }
      :host ::ng-deep .mdi-image-album:before {
        content: "\\F2EA"; }
      :host ::ng-deep .mdi-image-area:before {
        content: "\\F2EB"; }
      :host ::ng-deep .mdi-image-area-close:before {
        content: "\\F2EC"; }
      :host ::ng-deep .mdi-image-broken:before {
        content: "\\F2ED"; }
      :host ::ng-deep .mdi-image-broken-variant:before {
        content: "\\F2EE"; }
      :host ::ng-deep .mdi-image-filter:before {
        content: "\\F2EF"; }
      :host ::ng-deep .mdi-image-filter-black-white:before {
        content: "\\F2F0"; }
      :host ::ng-deep .mdi-image-filter-center-focus:before {
        content: "\\F2F1"; }
      :host ::ng-deep .mdi-image-filter-center-focus-weak:before {
        content: "\\F2F2"; }
      :host ::ng-deep .mdi-image-filter-drama:before {
        content: "\\F2F3"; }
      :host ::ng-deep .mdi-image-filter-frames:before {
        content: "\\F2F4"; }
      :host ::ng-deep .mdi-image-filter-hdr:before {
        content: "\\F2F5"; }
      :host ::ng-deep .mdi-image-filter-none:before {
        content: "\\F2F6"; }
      :host ::ng-deep .mdi-image-filter-tilt-shift:before {
        content: "\\F2F7"; }
      :host ::ng-deep .mdi-image-filter-vintage:before {
        content: "\\F2F8"; }
      :host ::ng-deep .mdi-image-multiple:before {
        content: "\\F2F9"; }
      :host ::ng-deep .mdi-import:before {
        content: "\\F2FA"; }
      :host ::ng-deep .mdi-inbox:before {
        content: "\\F686"; }
      :host ::ng-deep .mdi-inbox-arrow-down:before {
        content: "\\F2FB"; }
      :host ::ng-deep .mdi-inbox-arrow-up:before {
        content: "\\F3D1"; }
      :host ::ng-deep .mdi-incognito:before {
        content: "\\F5F9"; }
      :host ::ng-deep .mdi-infinity:before {
        content: "\\F6E3"; }
      :host ::ng-deep .mdi-information:before {
        content: "\\F2FC"; }
      :host ::ng-deep .mdi-information-outline:before {
        content: "\\F2FD"; }
      :host ::ng-deep .mdi-information-variant:before {
        content: "\\F64E"; }
      :host ::ng-deep .mdi-instagram:before {
        content: "\\F2FE"; }
      :host ::ng-deep .mdi-instapaper:before {
        content: "\\F2FF"; }
      :host ::ng-deep .mdi-internet-explorer:before {
        content: "\\F300"; }
      :host ::ng-deep .mdi-invert-colors:before {
        content: "\\F301"; }
      :host ::ng-deep .mdi-itunes:before {
        content: "\\F676"; }
      :host ::ng-deep .mdi-jeepney:before {
        content: "\\F302"; }
      :host ::ng-deep .mdi-jira:before {
        content: "\\F303"; }
      :host ::ng-deep .mdi-jsfiddle:before {
        content: "\\F304"; }
      :host ::ng-deep .mdi-json:before {
        content: "\\F626"; }
      :host ::ng-deep .mdi-keg:before {
        content: "\\F305"; }
      :host ::ng-deep .mdi-kettle:before {
        content: "\\F5FA"; }
      :host ::ng-deep .mdi-key:before {
        content: "\\F306"; }
      :host ::ng-deep .mdi-key-change:before {
        content: "\\F307"; }
      :host ::ng-deep .mdi-key-minus:before {
        content: "\\F308"; }
      :host ::ng-deep .mdi-key-plus:before {
        content: "\\F309"; }
      :host ::ng-deep .mdi-key-remove:before {
        content: "\\F30A"; }
      :host ::ng-deep .mdi-key-variant:before {
        content: "\\F30B"; }
      :host ::ng-deep .mdi-keyboard:before {
        content: "\\F30C"; }
      :host ::ng-deep .mdi-keyboard-backspace:before {
        content: "\\F30D"; }
      :host ::ng-deep .mdi-keyboard-caps:before {
        content: "\\F30E"; }
      :host ::ng-deep .mdi-keyboard-close:before {
        content: "\\F30F"; }
      :host ::ng-deep .mdi-keyboard-off:before {
        content: "\\F310"; }
      :host ::ng-deep .mdi-keyboard-return:before {
        content: "\\F311"; }
      :host ::ng-deep .mdi-keyboard-tab:before {
        content: "\\F312"; }
      :host ::ng-deep .mdi-keyboard-variant:before {
        content: "\\F313"; }
      :host ::ng-deep .mdi-kodi:before {
        content: "\\F314"; }
      :host ::ng-deep .mdi-label:before {
        content: "\\F315"; }
      :host ::ng-deep .mdi-label-outline:before {
        content: "\\F316"; }
      :host ::ng-deep .mdi-lambda:before {
        content: "\\F627"; }
      :host ::ng-deep .mdi-lamp:before {
        content: "\\F6B4"; }
      :host ::ng-deep .mdi-lan:before {
        content: "\\F317"; }
      :host ::ng-deep .mdi-lan-connect:before {
        content: "\\F318"; }
      :host ::ng-deep .mdi-lan-disconnect:before {
        content: "\\F319"; }
      :host ::ng-deep .mdi-lan-pending:before {
        content: "\\F31A"; }
      :host ::ng-deep .mdi-language-c:before {
        content: "\\F671"; }
      :host ::ng-deep .mdi-language-cpp:before {
        content: "\\F672"; }
      :host ::ng-deep .mdi-language-csharp:before {
        content: "\\F31B"; }
      :host ::ng-deep .mdi-language-css3:before {
        content: "\\F31C"; }
      :host ::ng-deep .mdi-language-html5:before {
        content: "\\F31D"; }
      :host ::ng-deep .mdi-language-javascript:before {
        content: "\\F31E"; }
      :host ::ng-deep .mdi-language-php:before {
        content: "\\F31F"; }
      :host ::ng-deep .mdi-language-python:before {
        content: "\\F320"; }
      :host ::ng-deep .mdi-language-python-text:before {
        content: "\\F321"; }
      :host ::ng-deep .mdi-language-swift:before {
        content: "\\F6E4"; }
      :host ::ng-deep .mdi-language-typescript:before {
        content: "\\F6E5"; }
      :host ::ng-deep .mdi-laptop:before {
        content: "\\F322"; }
      :host ::ng-deep .mdi-laptop-chromebook:before {
        content: "\\F323"; }
      :host ::ng-deep .mdi-laptop-mac:before {
        content: "\\F324"; }
      :host ::ng-deep .mdi-laptop-off:before {
        content: "\\F6E6"; }
      :host ::ng-deep .mdi-laptop-windows:before {
        content: "\\F325"; }
      :host ::ng-deep .mdi-lastfm:before {
        content: "\\F326"; }
      :host ::ng-deep .mdi-launch:before {
        content: "\\F327"; }
      :host ::ng-deep .mdi-layers:before {
        content: "\\F328"; }
      :host ::ng-deep .mdi-layers-off:before {
        content: "\\F329"; }
      :host ::ng-deep .mdi-lead-pencil:before {
        content: "\\F64F"; }
      :host ::ng-deep .mdi-leaf:before {
        content: "\\F32A"; }
      :host ::ng-deep .mdi-led-off:before {
        content: "\\F32B"; }
      :host ::ng-deep .mdi-led-on:before {
        content: "\\F32C"; }
      :host ::ng-deep .mdi-led-outline:before {
        content: "\\F32D"; }
      :host ::ng-deep .mdi-led-variant-off:before {
        content: "\\F32E"; }
      :host ::ng-deep .mdi-led-variant-on:before {
        content: "\\F32F"; }
      :host ::ng-deep .mdi-led-variant-outline:before {
        content: "\\F330"; }
      :host ::ng-deep .mdi-library:before {
        content: "\\F331"; }
      :host ::ng-deep .mdi-library-books:before {
        content: "\\F332"; }
      :host ::ng-deep .mdi-library-music:before {
        content: "\\F333"; }
      :host ::ng-deep .mdi-library-plus:before {
        content: "\\F334"; }
      :host ::ng-deep .mdi-lightbulb:before {
        content: "\\F335"; }
      :host ::ng-deep .mdi-lightbulb-on:before {
        content: "\\F6E7"; }
      :host ::ng-deep .mdi-lightbulb-on-outline:before {
        content: "\\F6E8"; }
      :host ::ng-deep .mdi-lightbulb-outline:before {
        content: "\\F336"; }
      :host ::ng-deep .mdi-link:before {
        content: "\\F337"; }
      :host ::ng-deep .mdi-link-off:before {
        content: "\\F338"; }
      :host ::ng-deep .mdi-link-variant:before {
        content: "\\F339"; }
      :host ::ng-deep .mdi-link-variant-off:before {
        content: "\\F33A"; }
      :host ::ng-deep .mdi-linkedin:before {
        content: "\\F33B"; }
      :host ::ng-deep .mdi-linkedin-box:before {
        content: "\\F33C"; }
      :host ::ng-deep .mdi-linux:before {
        content: "\\F33D"; }
      :host ::ng-deep .mdi-lock:before {
        content: "\\F33E"; }
      :host ::ng-deep .mdi-lock-open:before {
        content: "\\F33F"; }
      :host ::ng-deep .mdi-lock-open-outline:before {
        content: "\\F340"; }
      :host ::ng-deep .mdi-lock-outline:before {
        content: "\\F341"; }
      :host ::ng-deep .mdi-lock-pattern:before {
        content: "\\F6E9"; }
      :host ::ng-deep .mdi-lock-plus:before {
        content: "\\F5FB"; }
      :host ::ng-deep .mdi-login:before {
        content: "\\F342"; }
      :host ::ng-deep .mdi-login-variant:before {
        content: "\\F5FC"; }
      :host ::ng-deep .mdi-logout:before {
        content: "\\F343"; }
      :host ::ng-deep .mdi-logout-variant:before {
        content: "\\F5FD"; }
      :host ::ng-deep .mdi-looks:before {
        content: "\\F344"; }
      :host ::ng-deep .mdi-loop:before {
        content: "\\F6EA"; }
      :host ::ng-deep .mdi-loupe:before {
        content: "\\F345"; }
      :host ::ng-deep .mdi-lumx:before {
        content: "\\F346"; }
      :host ::ng-deep .mdi-magnet:before {
        content: "\\F347"; }
      :host ::ng-deep .mdi-magnet-on:before {
        content: "\\F348"; }
      :host ::ng-deep .mdi-magnify:before {
        content: "\\F349"; }
      :host ::ng-deep .mdi-magnify-minus:before {
        content: "\\F34A"; }
      :host ::ng-deep .mdi-magnify-minus-outline:before {
        content: "\\F6EB"; }
      :host ::ng-deep .mdi-magnify-plus:before {
        content: "\\F34B"; }
      :host ::ng-deep .mdi-magnify-plus-outline:before {
        content: "\\F6EC"; }
      :host ::ng-deep .mdi-mail-ru:before {
        content: "\\F34C"; }
      :host ::ng-deep .mdi-mailbox:before {
        content: "\\F6ED"; }
      :host ::ng-deep .mdi-map:before {
        content: "\\F34D"; }
      :host ::ng-deep .mdi-map-marker:before {
        content: "\\F34E"; }
      :host ::ng-deep .mdi-map-marker-circle:before {
        content: "\\F34F"; }
      :host ::ng-deep .mdi-map-marker-minus:before {
        content: "\\F650"; }
      :host ::ng-deep .mdi-map-marker-multiple:before {
        content: "\\F350"; }
      :host ::ng-deep .mdi-map-marker-off:before {
        content: "\\F351"; }
      :host ::ng-deep .mdi-map-marker-plus:before {
        content: "\\F651"; }
      :host ::ng-deep .mdi-map-marker-radius:before {
        content: "\\F352"; }
      :host ::ng-deep .mdi-margin:before {
        content: "\\F353"; }
      :host ::ng-deep .mdi-markdown:before {
        content: "\\F354"; }
      :host ::ng-deep .mdi-marker:before {
        content: "\\F652"; }
      :host ::ng-deep .mdi-marker-check:before {
        content: "\\F355"; }
      :host ::ng-deep .mdi-martini:before {
        content: "\\F356"; }
      :host ::ng-deep .mdi-material-ui:before {
        content: "\\F357"; }
      :host ::ng-deep .mdi-math-compass:before {
        content: "\\F358"; }
      :host ::ng-deep .mdi-matrix:before {
        content: "\\F628"; }
      :host ::ng-deep .mdi-maxcdn:before {
        content: "\\F359"; }
      :host ::ng-deep .mdi-medical-bag:before {
        content: "\\F6EE"; }
      :host ::ng-deep .mdi-medium:before {
        content: "\\F35A"; }
      :host ::ng-deep .mdi-memory:before {
        content: "\\F35B"; }
      :host ::ng-deep .mdi-menu:before {
        content: "\\F35C"; }
      :host ::ng-deep .mdi-menu-down:before {
        content: "\\F35D"; }
      :host ::ng-deep .mdi-menu-down-outline:before {
        content: "\\F6B5"; }
      :host ::ng-deep .mdi-menu-left:before {
        content: "\\F35E"; }
      :host ::ng-deep .mdi-menu-right:before {
        content: "\\F35F"; }
      :host ::ng-deep .mdi-menu-up:before {
        content: "\\F360"; }
      :host ::ng-deep .mdi-menu-up-outline:before {
        content: "\\F6B6"; }
      :host ::ng-deep .mdi-message:before {
        content: "\\F361"; }
      :host ::ng-deep .mdi-message-alert:before {
        content: "\\F362"; }
      :host ::ng-deep .mdi-message-bulleted:before {
        content: "\\F6A1"; }
      :host ::ng-deep .mdi-message-bulleted-off:before {
        content: "\\F6A2"; }
      :host ::ng-deep .mdi-message-draw:before {
        content: "\\F363"; }
      :host ::ng-deep .mdi-message-image:before {
        content: "\\F364"; }
      :host ::ng-deep .mdi-message-outline:before {
        content: "\\F365"; }
      :host ::ng-deep .mdi-message-plus:before {
        content: "\\F653"; }
      :host ::ng-deep .mdi-message-processing:before {
        content: "\\F366"; }
      :host ::ng-deep .mdi-message-reply:before {
        content: "\\F367"; }
      :host ::ng-deep .mdi-message-reply-text:before {
        content: "\\F368"; }
      :host ::ng-deep .mdi-message-settings:before {
        content: "\\F6EF"; }
      :host ::ng-deep .mdi-message-settings-variant:before {
        content: "\\F6F0"; }
      :host ::ng-deep .mdi-message-text:before {
        content: "\\F369"; }
      :host ::ng-deep .mdi-message-text-outline:before {
        content: "\\F36A"; }
      :host ::ng-deep .mdi-message-video:before {
        content: "\\F36B"; }
      :host ::ng-deep .mdi-meteor:before {
        content: "\\F629"; }
      :host ::ng-deep .mdi-microphone:before {
        content: "\\F36C"; }
      :host ::ng-deep .mdi-microphone-off:before {
        content: "\\F36D"; }
      :host ::ng-deep .mdi-microphone-outline:before {
        content: "\\F36E"; }
      :host ::ng-deep .mdi-microphone-settings:before {
        content: "\\F36F"; }
      :host ::ng-deep .mdi-microphone-variant:before {
        content: "\\F370"; }
      :host ::ng-deep .mdi-microphone-variant-off:before {
        content: "\\F371"; }
      :host ::ng-deep .mdi-microscope:before {
        content: "\\F654"; }
      :host ::ng-deep .mdi-microsoft:before {
        content: "\\F372"; }
      :host ::ng-deep .mdi-minecraft:before {
        content: "\\F373"; }
      :host ::ng-deep .mdi-minus:before {
        content: "\\F374"; }
      :host ::ng-deep .mdi-minus-box:before {
        content: "\\F375"; }
      :host ::ng-deep .mdi-minus-box-outline:before {
        content: "\\F6F1"; }
      :host ::ng-deep .mdi-minus-circle:before {
        content: "\\F376"; }
      :host ::ng-deep .mdi-minus-circle-outline:before {
        content: "\\F377"; }
      :host ::ng-deep .mdi-minus-network:before {
        content: "\\F378"; }
      :host ::ng-deep .mdi-mixcloud:before {
        content: "\\F62A"; }
      :host ::ng-deep .mdi-monitor:before {
        content: "\\F379"; }
      :host ::ng-deep .mdi-monitor-multiple:before {
        content: "\\F37A"; }
      :host ::ng-deep .mdi-more:before {
        content: "\\F37B"; }
      :host ::ng-deep .mdi-motorbike:before {
        content: "\\F37C"; }
      :host ::ng-deep .mdi-mouse:before {
        content: "\\F37D"; }
      :host ::ng-deep .mdi-mouse-off:before {
        content: "\\F37E"; }
      :host ::ng-deep .mdi-mouse-variant:before {
        content: "\\F37F"; }
      :host ::ng-deep .mdi-mouse-variant-off:before {
        content: "\\F380"; }
      :host ::ng-deep .mdi-move-resize:before {
        content: "\\F655"; }
      :host ::ng-deep .mdi-move-resize-variant:before {
        content: "\\F656"; }
      :host ::ng-deep .mdi-movie:before {
        content: "\\F381"; }
      :host ::ng-deep .mdi-multiplication:before {
        content: "\\F382"; }
      :host ::ng-deep .mdi-multiplication-box:before {
        content: "\\F383"; }
      :host ::ng-deep .mdi-music-box:before {
        content: "\\F384"; }
      :host ::ng-deep .mdi-music-box-outline:before {
        content: "\\F385"; }
      :host ::ng-deep .mdi-music-circle:before {
        content: "\\F386"; }
      :host ::ng-deep .mdi-music-note:before {
        content: "\\F387"; }
      :host ::ng-deep .mdi-music-note-bluetooth:before {
        content: "\\F5FE"; }
      :host ::ng-deep .mdi-music-note-bluetooth-off:before {
        content: "\\F5FF"; }
      :host ::ng-deep .mdi-music-note-eighth:before {
        content: "\\F388"; }
      :host ::ng-deep .mdi-music-note-half:before {
        content: "\\F389"; }
      :host ::ng-deep .mdi-music-note-off:before {
        content: "\\F38A"; }
      :host ::ng-deep .mdi-music-note-quarter:before {
        content: "\\F38B"; }
      :host ::ng-deep .mdi-music-note-sixteenth:before {
        content: "\\F38C"; }
      :host ::ng-deep .mdi-music-note-whole:before {
        content: "\\F38D"; }
      :host ::ng-deep .mdi-nature:before {
        content: "\\F38E"; }
      :host ::ng-deep .mdi-nature-people:before {
        content: "\\F38F"; }
      :host ::ng-deep .mdi-navigation:before {
        content: "\\F390"; }
      :host ::ng-deep .mdi-near-me:before {
        content: "\\F5CD"; }
      :host ::ng-deep .mdi-needle:before {
        content: "\\F391"; }
      :host ::ng-deep .mdi-nest-protect:before {
        content: "\\F392"; }
      :host ::ng-deep .mdi-nest-thermostat:before {
        content: "\\F393"; }
      :host ::ng-deep .mdi-network:before {
        content: "\\F6F2"; }
      :host ::ng-deep .mdi-network-download:before {
        content: "\\F6F3"; }
      :host ::ng-deep .mdi-network-question:before {
        content: "\\F6F4"; }
      :host ::ng-deep .mdi-network-upload:before {
        content: "\\F6F5"; }
      :host ::ng-deep .mdi-new-box:before {
        content: "\\F394"; }
      :host ::ng-deep .mdi-newspaper:before {
        content: "\\F395"; }
      :host ::ng-deep .mdi-nfc:before {
        content: "\\F396"; }
      :host ::ng-deep .mdi-nfc-tap:before {
        content: "\\F397"; }
      :host ::ng-deep .mdi-nfc-variant:before {
        content: "\\F398"; }
      :host ::ng-deep .mdi-nodejs:before {
        content: "\\F399"; }
      :host ::ng-deep .mdi-note:before {
        content: "\\F39A"; }
      :host ::ng-deep .mdi-note-multiple:before {
        content: "\\F6B7"; }
      :host ::ng-deep .mdi-note-multiple-outline:before {
        content: "\\F6B8"; }
      :host ::ng-deep .mdi-note-outline:before {
        content: "\\F39B"; }
      :host ::ng-deep .mdi-note-plus:before {
        content: "\\F39C"; }
      :host ::ng-deep .mdi-note-plus-outline:before {
        content: "\\F39D"; }
      :host ::ng-deep .mdi-note-text:before {
        content: "\\F39E"; }
      :host ::ng-deep .mdi-notification-clear-all:before {
        content: "\\F39F"; }
      :host ::ng-deep .mdi-npm:before {
        content: "\\F6F6"; }
      :host ::ng-deep .mdi-nuke:before {
        content: "\\F6A3"; }
      :host ::ng-deep .mdi-numeric:before {
        content: "\\F3A0"; }
      :host ::ng-deep .mdi-numeric-0-box:before {
        content: "\\F3A1"; }
      :host ::ng-deep .mdi-numeric-0-box-multiple-outline:before {
        content: "\\F3A2"; }
      :host ::ng-deep .mdi-numeric-0-box-outline:before {
        content: "\\F3A3"; }
      :host ::ng-deep .mdi-numeric-1-box:before {
        content: "\\F3A4"; }
      :host ::ng-deep .mdi-numeric-1-box-multiple-outline:before {
        content: "\\F3A5"; }
      :host ::ng-deep .mdi-numeric-1-box-outline:before {
        content: "\\F3A6"; }
      :host ::ng-deep .mdi-numeric-2-box:before {
        content: "\\F3A7"; }
      :host ::ng-deep .mdi-numeric-2-box-multiple-outline:before {
        content: "\\F3A8"; }
      :host ::ng-deep .mdi-numeric-2-box-outline:before {
        content: "\\F3A9"; }
      :host ::ng-deep .mdi-numeric-3-box:before {
        content: "\\F3AA"; }
      :host ::ng-deep .mdi-numeric-3-box-multiple-outline:before {
        content: "\\F3AB"; }
      :host ::ng-deep .mdi-numeric-3-box-outline:before {
        content: "\\F3AC"; }
      :host ::ng-deep .mdi-numeric-4-box:before {
        content: "\\F3AD"; }
      :host ::ng-deep .mdi-numeric-4-box-multiple-outline:before {
        content: "\\F3AE"; }
      :host ::ng-deep .mdi-numeric-4-box-outline:before {
        content: "\\F3AF"; }
      :host ::ng-deep .mdi-numeric-5-box:before {
        content: "\\F3B0"; }
      :host ::ng-deep .mdi-numeric-5-box-multiple-outline:before {
        content: "\\F3B1"; }
      :host ::ng-deep .mdi-numeric-5-box-outline:before {
        content: "\\F3B2"; }
      :host ::ng-deep .mdi-numeric-6-box:before {
        content: "\\F3B3"; }
      :host ::ng-deep .mdi-numeric-6-box-multiple-outline:before {
        content: "\\F3B4"; }
      :host ::ng-deep .mdi-numeric-6-box-outline:before {
        content: "\\F3B5"; }
      :host ::ng-deep .mdi-numeric-7-box:before {
        content: "\\F3B6"; }
      :host ::ng-deep .mdi-numeric-7-box-multiple-outline:before {
        content: "\\F3B7"; }
      :host ::ng-deep .mdi-numeric-7-box-outline:before {
        content: "\\F3B8"; }
      :host ::ng-deep .mdi-numeric-8-box:before {
        content: "\\F3B9"; }
      :host ::ng-deep .mdi-numeric-8-box-multiple-outline:before {
        content: "\\F3BA"; }
      :host ::ng-deep .mdi-numeric-8-box-outline:before {
        content: "\\F3BB"; }
      :host ::ng-deep .mdi-numeric-9-box:before {
        content: "\\F3BC"; }
      :host ::ng-deep .mdi-numeric-9-box-multiple-outline:before {
        content: "\\F3BD"; }
      :host ::ng-deep .mdi-numeric-9-box-outline:before {
        content: "\\F3BE"; }
      :host ::ng-deep .mdi-numeric-9-plus-box:before {
        content: "\\F3BF"; }
      :host ::ng-deep .mdi-numeric-9-plus-box-multiple-outline:before {
        content: "\\F3C0"; }
      :host ::ng-deep .mdi-numeric-9-plus-box-outline:before {
        content: "\\F3C1"; }
      :host ::ng-deep .mdi-nut:before {
        content: "\\F6F7"; }
      :host ::ng-deep .mdi-nutrition:before {
        content: "\\F3C2"; }
      :host ::ng-deep .mdi-oar:before {
        content: "\\F67B"; }
      :host ::ng-deep .mdi-octagon:before {
        content: "\\F3C3"; }
      :host ::ng-deep .mdi-octagon-outline:before {
        content: "\\F3C4"; }
      :host ::ng-deep .mdi-octagram:before {
        content: "\\F6F8"; }
      :host ::ng-deep .mdi-odnoklassniki:before {
        content: "\\F3C5"; }
      :host ::ng-deep .mdi-office:before {
        content: "\\F3C6"; }
      :host ::ng-deep .mdi-oil:before {
        content: "\\F3C7"; }
      :host ::ng-deep .mdi-oil-temperature:before {
        content: "\\F3C8"; }
      :host ::ng-deep .mdi-omega:before {
        content: "\\F3C9"; }
      :host ::ng-deep .mdi-onedrive:before {
        content: "\\F3CA"; }
      :host ::ng-deep .mdi-opacity:before {
        content: "\\F5CC"; }
      :host ::ng-deep .mdi-open-in-app:before {
        content: "\\F3CB"; }
      :host ::ng-deep .mdi-open-in-new:before {
        content: "\\F3CC"; }
      :host ::ng-deep .mdi-openid:before {
        content: "\\F3CD"; }
      :host ::ng-deep .mdi-opera:before {
        content: "\\F3CE"; }
      :host ::ng-deep .mdi-ornament:before {
        content: "\\F3CF"; }
      :host ::ng-deep .mdi-ornament-variant:before {
        content: "\\F3D0"; }
      :host ::ng-deep .mdi-owl:before {
        content: "\\F3D2"; }
      :host ::ng-deep .mdi-package:before {
        content: "\\F3D3"; }
      :host ::ng-deep .mdi-package-down:before {
        content: "\\F3D4"; }
      :host ::ng-deep .mdi-package-up:before {
        content: "\\F3D5"; }
      :host ::ng-deep .mdi-package-variant:before {
        content: "\\F3D6"; }
      :host ::ng-deep .mdi-package-variant-closed:before {
        content: "\\F3D7"; }
      :host ::ng-deep .mdi-page-first:before {
        content: "\\F600"; }
      :host ::ng-deep .mdi-page-last:before {
        content: "\\F601"; }
      :host ::ng-deep .mdi-page-layout-body:before {
        content: "\\F6F9"; }
      :host ::ng-deep .mdi-page-layout-footer:before {
        content: "\\F6FA"; }
      :host ::ng-deep .mdi-page-layout-header:before {
        content: "\\F6FB"; }
      :host ::ng-deep .mdi-page-layout-sidebar-left:before {
        content: "\\F6FC"; }
      :host ::ng-deep .mdi-page-layout-sidebar-right:before {
        content: "\\F6FD"; }
      :host ::ng-deep .mdi-palette:before {
        content: "\\F3D8"; }
      :host ::ng-deep .mdi-palette-advanced:before {
        content: "\\F3D9"; }
      :host ::ng-deep .mdi-panda:before {
        content: "\\F3DA"; }
      :host ::ng-deep .mdi-pandora:before {
        content: "\\F3DB"; }
      :host ::ng-deep .mdi-panorama:before {
        content: "\\F3DC"; }
      :host ::ng-deep .mdi-panorama-fisheye:before {
        content: "\\F3DD"; }
      :host ::ng-deep .mdi-panorama-horizontal:before {
        content: "\\F3DE"; }
      :host ::ng-deep .mdi-panorama-vertical:before {
        content: "\\F3DF"; }
      :host ::ng-deep .mdi-panorama-wide-angle:before {
        content: "\\F3E0"; }
      :host ::ng-deep .mdi-paper-cut-vertical:before {
        content: "\\F3E1"; }
      :host ::ng-deep .mdi-paperclip:before {
        content: "\\F3E2"; }
      :host ::ng-deep .mdi-parking:before {
        content: "\\F3E3"; }
      :host ::ng-deep .mdi-pause:before {
        content: "\\F3E4"; }
      :host ::ng-deep .mdi-pause-circle:before {
        content: "\\F3E5"; }
      :host ::ng-deep .mdi-pause-circle-outline:before {
        content: "\\F3E6"; }
      :host ::ng-deep .mdi-pause-octagon:before {
        content: "\\F3E7"; }
      :host ::ng-deep .mdi-pause-octagon-outline:before {
        content: "\\F3E8"; }
      :host ::ng-deep .mdi-paw:before {
        content: "\\F3E9"; }
      :host ::ng-deep .mdi-paw-off:before {
        content: "\\F657"; }
      :host ::ng-deep .mdi-pen:before {
        content: "\\F3EA"; }
      :host ::ng-deep .mdi-pencil:before {
        content: "\\F3EB"; }
      :host ::ng-deep .mdi-pencil-box:before {
        content: "\\F3EC"; }
      :host ::ng-deep .mdi-pencil-box-outline:before {
        content: "\\F3ED"; }
      :host ::ng-deep .mdi-pencil-circle:before {
        content: "\\F6FE"; }
      :host ::ng-deep .mdi-pencil-lock:before {
        content: "\\F3EE"; }
      :host ::ng-deep .mdi-pencil-off:before {
        content: "\\F3EF"; }
      :host ::ng-deep .mdi-pentagon:before {
        content: "\\F6FF"; }
      :host ::ng-deep .mdi-pentagon-outline:before {
        content: "\\F700"; }
      :host ::ng-deep .mdi-percent:before {
        content: "\\F3F0"; }
      :host ::ng-deep .mdi-pharmacy:before {
        content: "\\F3F1"; }
      :host ::ng-deep .mdi-phone:before {
        content: "\\F3F2"; }
      :host ::ng-deep .mdi-phone-bluetooth:before {
        content: "\\F3F3"; }
      :host ::ng-deep .mdi-phone-classic:before {
        content: "\\F602"; }
      :host ::ng-deep .mdi-phone-forward:before {
        content: "\\F3F4"; }
      :host ::ng-deep .mdi-phone-hangup:before {
        content: "\\F3F5"; }
      :host ::ng-deep .mdi-phone-in-talk:before {
        content: "\\F3F6"; }
      :host ::ng-deep .mdi-phone-incoming:before {
        content: "\\F3F7"; }
      :host ::ng-deep .mdi-phone-locked:before {
        content: "\\F3F8"; }
      :host ::ng-deep .mdi-phone-log:before {
        content: "\\F3F9"; }
      :host ::ng-deep .mdi-phone-minus:before {
        content: "\\F658"; }
      :host ::ng-deep .mdi-phone-missed:before {
        content: "\\F3FA"; }
      :host ::ng-deep .mdi-phone-outgoing:before {
        content: "\\F3FB"; }
      :host ::ng-deep .mdi-phone-paused:before {
        content: "\\F3FC"; }
      :host ::ng-deep .mdi-phone-plus:before {
        content: "\\F659"; }
      :host ::ng-deep .mdi-phone-settings:before {
        content: "\\F3FD"; }
      :host ::ng-deep .mdi-phone-voip:before {
        content: "\\F3FE"; }
      :host ::ng-deep .mdi-pi:before {
        content: "\\F3FF"; }
      :host ::ng-deep .mdi-pi-box:before {
        content: "\\F400"; }
      :host ::ng-deep .mdi-piano:before {
        content: "\\F67C"; }
      :host ::ng-deep .mdi-pig:before {
        content: "\\F401"; }
      :host ::ng-deep .mdi-pill:before {
        content: "\\F402"; }
      :host ::ng-deep .mdi-pillar:before {
        content: "\\F701"; }
      :host ::ng-deep .mdi-pin:before {
        content: "\\F403"; }
      :host ::ng-deep .mdi-pin-off:before {
        content: "\\F404"; }
      :host ::ng-deep .mdi-pine-tree:before {
        content: "\\F405"; }
      :host ::ng-deep .mdi-pine-tree-box:before {
        content: "\\F406"; }
      :host ::ng-deep .mdi-pinterest:before {
        content: "\\F407"; }
      :host ::ng-deep .mdi-pinterest-box:before {
        content: "\\F408"; }
      :host ::ng-deep .mdi-pistol:before {
        content: "\\F702"; }
      :host ::ng-deep .mdi-pizza:before {
        content: "\\F409"; }
      :host ::ng-deep .mdi-plane-shield:before {
        content: "\\F6BA"; }
      :host ::ng-deep .mdi-play:before {
        content: "\\F40A"; }
      :host ::ng-deep .mdi-play-box-outline:before {
        content: "\\F40B"; }
      :host ::ng-deep .mdi-play-circle:before {
        content: "\\F40C"; }
      :host ::ng-deep .mdi-play-circle-outline:before {
        content: "\\F40D"; }
      :host ::ng-deep .mdi-play-pause:before {
        content: "\\F40E"; }
      :host ::ng-deep .mdi-play-protected-content:before {
        content: "\\F40F"; }
      :host ::ng-deep .mdi-playlist-check:before {
        content: "\\F5C7"; }
      :host ::ng-deep .mdi-playlist-minus:before {
        content: "\\F410"; }
      :host ::ng-deep .mdi-playlist-play:before {
        content: "\\F411"; }
      :host ::ng-deep .mdi-playlist-plus:before {
        content: "\\F412"; }
      :host ::ng-deep .mdi-playlist-remove:before {
        content: "\\F413"; }
      :host ::ng-deep .mdi-playstation:before {
        content: "\\F414"; }
      :host ::ng-deep .mdi-plex:before {
        content: "\\F6B9"; }
      :host ::ng-deep .mdi-plus:before {
        content: "\\F415"; }
      :host ::ng-deep .mdi-plus-box:before {
        content: "\\F416"; }
      :host ::ng-deep .mdi-plus-box-outline:before {
        content: "\\F703"; }
      :host ::ng-deep .mdi-plus-circle:before {
        content: "\\F417"; }
      :host ::ng-deep .mdi-plus-circle-multiple-outline:before {
        content: "\\F418"; }
      :host ::ng-deep .mdi-plus-circle-outline:before {
        content: "\\F419"; }
      :host ::ng-deep .mdi-plus-network:before {
        content: "\\F41A"; }
      :host ::ng-deep .mdi-plus-one:before {
        content: "\\F41B"; }
      :host ::ng-deep .mdi-plus-outline:before {
        content: "\\F704"; }
      :host ::ng-deep .mdi-pocket:before {
        content: "\\F41C"; }
      :host ::ng-deep .mdi-pokeball:before {
        content: "\\F41D"; }
      :host ::ng-deep .mdi-polaroid:before {
        content: "\\F41E"; }
      :host ::ng-deep .mdi-poll:before {
        content: "\\F41F"; }
      :host ::ng-deep .mdi-poll-box:before {
        content: "\\F420"; }
      :host ::ng-deep .mdi-polymer:before {
        content: "\\F421"; }
      :host ::ng-deep .mdi-pool:before {
        content: "\\F606"; }
      :host ::ng-deep .mdi-popcorn:before {
        content: "\\F422"; }
      :host ::ng-deep .mdi-pot:before {
        content: "\\F65A"; }
      :host ::ng-deep .mdi-pot-mix:before {
        content: "\\F65B"; }
      :host ::ng-deep .mdi-pound:before {
        content: "\\F423"; }
      :host ::ng-deep .mdi-pound-box:before {
        content: "\\F424"; }
      :host ::ng-deep .mdi-power:before {
        content: "\\F425"; }
      :host ::ng-deep .mdi-power-plug:before {
        content: "\\F6A4"; }
      :host ::ng-deep .mdi-power-plug-off:before {
        content: "\\F6A5"; }
      :host ::ng-deep .mdi-power-settings:before {
        content: "\\F426"; }
      :host ::ng-deep .mdi-power-socket:before {
        content: "\\F427"; }
      :host ::ng-deep .mdi-prescription:before {
        content: "\\F705"; }
      :host ::ng-deep .mdi-presentation:before {
        content: "\\F428"; }
      :host ::ng-deep .mdi-presentation-play:before {
        content: "\\F429"; }
      :host ::ng-deep .mdi-printer:before {
        content: "\\F42A"; }
      :host ::ng-deep .mdi-printer-3d:before {
        content: "\\F42B"; }
      :host ::ng-deep .mdi-printer-alert:before {
        content: "\\F42C"; }
      :host ::ng-deep .mdi-printer-settings:before {
        content: "\\F706"; }
      :host ::ng-deep .mdi-priority-high:before {
        content: "\\F603"; }
      :host ::ng-deep .mdi-priority-low:before {
        content: "\\F604"; }
      :host ::ng-deep .mdi-professional-hexagon:before {
        content: "\\F42D"; }
      :host ::ng-deep .mdi-projector:before {
        content: "\\F42E"; }
      :host ::ng-deep .mdi-projector-screen:before {
        content: "\\F42F"; }
      :host ::ng-deep .mdi-publish:before {
        content: "\\F6A6"; }
      :host ::ng-deep .mdi-pulse:before {
        content: "\\F430"; }
      :host ::ng-deep .mdi-puzzle:before {
        content: "\\F431"; }
      :host ::ng-deep .mdi-qqchat:before {
        content: "\\F605"; }
      :host ::ng-deep .mdi-qrcode:before {
        content: "\\F432"; }
      :host ::ng-deep .mdi-qrcode-scan:before {
        content: "\\F433"; }
      :host ::ng-deep .mdi-quadcopter:before {
        content: "\\F434"; }
      :host ::ng-deep .mdi-quality-high:before {
        content: "\\F435"; }
      :host ::ng-deep .mdi-quicktime:before {
        content: "\\F436"; }
      :host ::ng-deep .mdi-radar:before {
        content: "\\F437"; }
      :host ::ng-deep .mdi-radiator:before {
        content: "\\F438"; }
      :host ::ng-deep .mdi-radio:before {
        content: "\\F439"; }
      :host ::ng-deep .mdi-radio-handheld:before {
        content: "\\F43A"; }
      :host ::ng-deep .mdi-radio-tower:before {
        content: "\\F43B"; }
      :host ::ng-deep .mdi-radioactive:before {
        content: "\\F43C"; }
      :host ::ng-deep .mdi-radiobox-blank:before {
        content: "\\F43D"; }
      :host ::ng-deep .mdi-radiobox-marked:before {
        content: "\\F43E"; }
      :host ::ng-deep .mdi-raspberrypi:before {
        content: "\\F43F"; }
      :host ::ng-deep .mdi-ray-end:before {
        content: "\\F440"; }
      :host ::ng-deep .mdi-ray-end-arrow:before {
        content: "\\F441"; }
      :host ::ng-deep .mdi-ray-start:before {
        content: "\\F442"; }
      :host ::ng-deep .mdi-ray-start-arrow:before {
        content: "\\F443"; }
      :host ::ng-deep .mdi-ray-start-end:before {
        content: "\\F444"; }
      :host ::ng-deep .mdi-ray-vertex:before {
        content: "\\F445"; }
      :host ::ng-deep .mdi-rdio:before {
        content: "\\F446"; }
      :host ::ng-deep .mdi-react:before {
        content: "\\F707"; }
      :host ::ng-deep .mdi-read:before {
        content: "\\F447"; }
      :host ::ng-deep .mdi-readability:before {
        content: "\\F448"; }
      :host ::ng-deep .mdi-receipt:before {
        content: "\\F449"; }
      :host ::ng-deep .mdi-record:before {
        content: "\\F44A"; }
      :host ::ng-deep .mdi-record-rec:before {
        content: "\\F44B"; }
      :host ::ng-deep .mdi-recycle:before {
        content: "\\F44C"; }
      :host ::ng-deep .mdi-reddit:before {
        content: "\\F44D"; }
      :host ::ng-deep .mdi-redo:before {
        content: "\\F44E"; }
      :host ::ng-deep .mdi-redo-variant:before {
        content: "\\F44F"; }
      :host ::ng-deep .mdi-refresh:before {
        content: "\\F450"; }
      :host ::ng-deep .mdi-regex:before {
        content: "\\F451"; }
      :host ::ng-deep .mdi-relative-scale:before {
        content: "\\F452"; }
      :host ::ng-deep .mdi-reload:before {
        content: "\\F453"; }
      :host ::ng-deep .mdi-remote:before {
        content: "\\F454"; }
      :host ::ng-deep .mdi-rename-box:before {
        content: "\\F455"; }
      :host ::ng-deep .mdi-reorder-horizontal:before {
        content: "\\F687"; }
      :host ::ng-deep .mdi-reorder-vertical:before {
        content: "\\F688"; }
      :host ::ng-deep .mdi-repeat:before {
        content: "\\F456"; }
      :host ::ng-deep .mdi-repeat-off:before {
        content: "\\F457"; }
      :host ::ng-deep .mdi-repeat-once:before {
        content: "\\F458"; }
      :host ::ng-deep .mdi-replay:before {
        content: "\\F459"; }
      :host ::ng-deep .mdi-reply:before {
        content: "\\F45A"; }
      :host ::ng-deep .mdi-reply-all:before {
        content: "\\F45B"; }
      :host ::ng-deep .mdi-reproduction:before {
        content: "\\F45C"; }
      :host ::ng-deep .mdi-resize-bottom-right:before {
        content: "\\F45D"; }
      :host ::ng-deep .mdi-responsive:before {
        content: "\\F45E"; }
      :host ::ng-deep .mdi-restart:before {
        content: "\\F708"; }
      :host ::ng-deep .mdi-restore:before {
        content: "\\F6A7"; }
      :host ::ng-deep .mdi-rewind:before {
        content: "\\F45F"; }
      :host ::ng-deep .mdi-rewind-outline:before {
        content: "\\F709"; }
      :host ::ng-deep .mdi-rhombus:before {
        content: "\\F70A"; }
      :host ::ng-deep .mdi-rhombus-outline:before {
        content: "\\F70B"; }
      :host ::ng-deep .mdi-ribbon:before {
        content: "\\F460"; }
      :host ::ng-deep .mdi-road:before {
        content: "\\F461"; }
      :host ::ng-deep .mdi-road-variant:before {
        content: "\\F462"; }
      :host ::ng-deep .mdi-robot:before {
        content: "\\F6A8"; }
      :host ::ng-deep .mdi-rocket:before {
        content: "\\F463"; }
      :host ::ng-deep .mdi-roomba:before {
        content: "\\F70C"; }
      :host ::ng-deep .mdi-rotate-3d:before {
        content: "\\F464"; }
      :host ::ng-deep .mdi-rotate-left:before {
        content: "\\F465"; }
      :host ::ng-deep .mdi-rotate-left-variant:before {
        content: "\\F466"; }
      :host ::ng-deep .mdi-rotate-right:before {
        content: "\\F467"; }
      :host ::ng-deep .mdi-rotate-right-variant:before {
        content: "\\F468"; }
      :host ::ng-deep .mdi-rounded-corner:before {
        content: "\\F607"; }
      :host ::ng-deep .mdi-router-wireless:before {
        content: "\\F469"; }
      :host ::ng-deep .mdi-routes:before {
        content: "\\F46A"; }
      :host ::ng-deep .mdi-rowing:before {
        content: "\\F608"; }
      :host ::ng-deep .mdi-rss:before {
        content: "\\F46B"; }
      :host ::ng-deep .mdi-rss-box:before {
        content: "\\F46C"; }
      :host ::ng-deep .mdi-ruler:before {
        content: "\\F46D"; }
      :host ::ng-deep .mdi-run:before {
        content: "\\F70D"; }
      :host ::ng-deep .mdi-run-fast:before {
        content: "\\F46E"; }
      :host ::ng-deep .mdi-sale:before {
        content: "\\F46F"; }
      :host ::ng-deep .mdi-satellite:before {
        content: "\\F470"; }
      :host ::ng-deep .mdi-satellite-variant:before {
        content: "\\F471"; }
      :host ::ng-deep .mdi-saxophone:before {
        content: "\\F609"; }
      :host ::ng-deep .mdi-scale:before {
        content: "\\F472"; }
      :host ::ng-deep .mdi-scale-balance:before {
        content: "\\F5D1"; }
      :host ::ng-deep .mdi-scale-bathroom:before {
        content: "\\F473"; }
      :host ::ng-deep .mdi-scanner:before {
        content: "\\F6AA"; }
      :host ::ng-deep .mdi-school:before {
        content: "\\F474"; }
      :host ::ng-deep .mdi-screen-rotation:before {
        content: "\\F475"; }
      :host ::ng-deep .mdi-screen-rotation-lock:before {
        content: "\\F476"; }
      :host ::ng-deep .mdi-screwdriver:before {
        content: "\\F477"; }
      :host ::ng-deep .mdi-script:before {
        content: "\\F478"; }
      :host ::ng-deep .mdi-sd:before {
        content: "\\F479"; }
      :host ::ng-deep .mdi-seal:before {
        content: "\\F47A"; }
      :host ::ng-deep .mdi-search-web:before {
        content: "\\F70E"; }
      :host ::ng-deep .mdi-seat-flat:before {
        content: "\\F47B"; }
      :host ::ng-deep .mdi-seat-flat-angled:before {
        content: "\\F47C"; }
      :host ::ng-deep .mdi-seat-individual-suite:before {
        content: "\\F47D"; }
      :host ::ng-deep .mdi-seat-legroom-extra:before {
        content: "\\F47E"; }
      :host ::ng-deep .mdi-seat-legroom-normal:before {
        content: "\\F47F"; }
      :host ::ng-deep .mdi-seat-legroom-reduced:before {
        content: "\\F480"; }
      :host ::ng-deep .mdi-seat-recline-extra:before {
        content: "\\F481"; }
      :host ::ng-deep .mdi-seat-recline-normal:before {
        content: "\\F482"; }
      :host ::ng-deep .mdi-security:before {
        content: "\\F483"; }
      :host ::ng-deep .mdi-security-home:before {
        content: "\\F689"; }
      :host ::ng-deep .mdi-security-network:before {
        content: "\\F484"; }
      :host ::ng-deep .mdi-select:before {
        content: "\\F485"; }
      :host ::ng-deep .mdi-select-all:before {
        content: "\\F486"; }
      :host ::ng-deep .mdi-select-inverse:before {
        content: "\\F487"; }
      :host ::ng-deep .mdi-select-off:before {
        content: "\\F488"; }
      :host ::ng-deep .mdi-selection:before {
        content: "\\F489"; }
      :host ::ng-deep .mdi-send:before {
        content: "\\F48A"; }
      :host ::ng-deep .mdi-serial-port:before {
        content: "\\F65C"; }
      :host ::ng-deep .mdi-server:before {
        content: "\\F48B"; }
      :host ::ng-deep .mdi-server-minus:before {
        content: "\\F48C"; }
      :host ::ng-deep .mdi-server-network:before {
        content: "\\F48D"; }
      :host ::ng-deep .mdi-server-network-off:before {
        content: "\\F48E"; }
      :host ::ng-deep .mdi-server-off:before {
        content: "\\F48F"; }
      :host ::ng-deep .mdi-server-plus:before {
        content: "\\F490"; }
      :host ::ng-deep .mdi-server-remove:before {
        content: "\\F491"; }
      :host ::ng-deep .mdi-server-security:before {
        content: "\\F492"; }
      :host ::ng-deep .mdi-settings:before {
        content: "\\F493"; }
      :host ::ng-deep .mdi-settings-box:before {
        content: "\\F494"; }
      :host ::ng-deep .mdi-shape-circle-plus:before {
        content: "\\F65D"; }
      :host ::ng-deep .mdi-shape-plus:before {
        content: "\\F495"; }
      :host ::ng-deep .mdi-shape-polygon-plus:before {
        content: "\\F65E"; }
      :host ::ng-deep .mdi-shape-rectangle-plus:before {
        content: "\\F65F"; }
      :host ::ng-deep .mdi-shape-square-plus:before {
        content: "\\F660"; }
      :host ::ng-deep .mdi-share:before {
        content: "\\F496"; }
      :host ::ng-deep .mdi-share-variant:before {
        content: "\\F497"; }
      :host ::ng-deep .mdi-shield:before {
        content: "\\F498"; }
      :host ::ng-deep .mdi-shield-outline:before {
        content: "\\F499"; }
      :host ::ng-deep .mdi-shopping:before {
        content: "\\F49A"; }
      :host ::ng-deep .mdi-shopping-music:before {
        content: "\\F49B"; }
      :host ::ng-deep .mdi-shovel:before {
        content: "\\F70F"; }
      :host ::ng-deep .mdi-shovel-off:before {
        content: "\\F710"; }
      :host ::ng-deep .mdi-shredder:before {
        content: "\\F49C"; }
      :host ::ng-deep .mdi-shuffle:before {
        content: "\\F49D"; }
      :host ::ng-deep .mdi-shuffle-disabled:before {
        content: "\\F49E"; }
      :host ::ng-deep .mdi-shuffle-variant:before {
        content: "\\F49F"; }
      :host ::ng-deep .mdi-sigma:before {
        content: "\\F4A0"; }
      :host ::ng-deep .mdi-sigma-lower:before {
        content: "\\F62B"; }
      :host ::ng-deep .mdi-sign-caution:before {
        content: "\\F4A1"; }
      :host ::ng-deep .mdi-signal:before {
        content: "\\F4A2"; }
      :host ::ng-deep .mdi-signal-2g:before {
        content: "\\F711"; }
      :host ::ng-deep .mdi-signal-3g:before {
        content: "\\F712"; }
      :host ::ng-deep .mdi-signal-4g:before {
        content: "\\F713"; }
      :host ::ng-deep .mdi-signal-hspa:before {
        content: "\\F714"; }
      :host ::ng-deep .mdi-signal-hspa-plus:before {
        content: "\\F715"; }
      :host ::ng-deep .mdi-signal-variant:before {
        content: "\\F60A"; }
      :host ::ng-deep .mdi-silverware:before {
        content: "\\F4A3"; }
      :host ::ng-deep .mdi-silverware-fork:before {
        content: "\\F4A4"; }
      :host ::ng-deep .mdi-silverware-spoon:before {
        content: "\\F4A5"; }
      :host ::ng-deep .mdi-silverware-variant:before {
        content: "\\F4A6"; }
      :host ::ng-deep .mdi-sim:before {
        content: "\\F4A7"; }
      :host ::ng-deep .mdi-sim-alert:before {
        content: "\\F4A8"; }
      :host ::ng-deep .mdi-sim-off:before {
        content: "\\F4A9"; }
      :host ::ng-deep .mdi-sitemap:before {
        content: "\\F4AA"; }
      :host ::ng-deep .mdi-skip-backward:before {
        content: "\\F4AB"; }
      :host ::ng-deep .mdi-skip-forward:before {
        content: "\\F4AC"; }
      :host ::ng-deep .mdi-skip-next:before {
        content: "\\F4AD"; }
      :host ::ng-deep .mdi-skip-next-circle:before {
        content: "\\F661"; }
      :host ::ng-deep .mdi-skip-next-circle-outline:before {
        content: "\\F662"; }
      :host ::ng-deep .mdi-skip-previous:before {
        content: "\\F4AE"; }
      :host ::ng-deep .mdi-skip-previous-circle:before {
        content: "\\F663"; }
      :host ::ng-deep .mdi-skip-previous-circle-outline:before {
        content: "\\F664"; }
      :host ::ng-deep .mdi-skull:before {
        content: "\\F68B"; }
      :host ::ng-deep .mdi-skype:before {
        content: "\\F4AF"; }
      :host ::ng-deep .mdi-skype-business:before {
        content: "\\F4B0"; }
      :host ::ng-deep .mdi-slack:before {
        content: "\\F4B1"; }
      :host ::ng-deep .mdi-sleep:before {
        content: "\\F4B2"; }
      :host ::ng-deep .mdi-sleep-off:before {
        content: "\\F4B3"; }
      :host ::ng-deep .mdi-smoking:before {
        content: "\\F4B4"; }
      :host ::ng-deep .mdi-smoking-off:before {
        content: "\\F4B5"; }
      :host ::ng-deep .mdi-snapchat:before {
        content: "\\F4B6"; }
      :host ::ng-deep .mdi-snowflake:before {
        content: "\\F716"; }
      :host ::ng-deep .mdi-snowman:before {
        content: "\\F4B7"; }
      :host ::ng-deep .mdi-soccer:before {
        content: "\\F4B8"; }
      :host ::ng-deep .mdi-sofa:before {
        content: "\\F4B9"; }
      :host ::ng-deep .mdi-solid:before {
        content: "\\F68C"; }
      :host ::ng-deep .mdi-sort:before {
        content: "\\F4BA"; }
      :host ::ng-deep .mdi-sort-alphabetical:before {
        content: "\\F4BB"; }
      :host ::ng-deep .mdi-sort-ascending:before {
        content: "\\F4BC"; }
      :host ::ng-deep .mdi-sort-descending:before {
        content: "\\F4BD"; }
      :host ::ng-deep .mdi-sort-numeric:before {
        content: "\\F4BE"; }
      :host ::ng-deep .mdi-sort-variant:before {
        content: "\\F4BF"; }
      :host ::ng-deep .mdi-soundcloud:before {
        content: "\\F4C0"; }
      :host ::ng-deep .mdi-source-branch:before {
        content: "\\F62C"; }
      :host ::ng-deep .mdi-source-commit:before {
        content: "\\F717"; }
      :host ::ng-deep .mdi-source-commit-end:before {
        content: "\\F718"; }
      :host ::ng-deep .mdi-source-commit-end-local:before {
        content: "\\F719"; }
      :host ::ng-deep .mdi-source-commit-local:before {
        content: "\\F71A"; }
      :host ::ng-deep .mdi-source-commit-next-local:before {
        content: "\\F71B"; }
      :host ::ng-deep .mdi-source-commit-start:before {
        content: "\\F71C"; }
      :host ::ng-deep .mdi-source-commit-start-next-local:before {
        content: "\\F71D"; }
      :host ::ng-deep .mdi-source-fork:before {
        content: "\\F4C1"; }
      :host ::ng-deep .mdi-source-merge:before {
        content: "\\F62D"; }
      :host ::ng-deep .mdi-source-pull:before {
        content: "\\F4C2"; }
      :host ::ng-deep .mdi-speaker:before {
        content: "\\F4C3"; }
      :host ::ng-deep .mdi-speaker-off:before {
        content: "\\F4C4"; }
      :host ::ng-deep .mdi-speaker-wireless:before {
        content: "\\F71E"; }
      :host ::ng-deep .mdi-speedometer:before {
        content: "\\F4C5"; }
      :host ::ng-deep .mdi-spellcheck:before {
        content: "\\F4C6"; }
      :host ::ng-deep .mdi-spotify:before {
        content: "\\F4C7"; }
      :host ::ng-deep .mdi-spotlight:before {
        content: "\\F4C8"; }
      :host ::ng-deep .mdi-spotlight-beam:before {
        content: "\\F4C9"; }
      :host ::ng-deep .mdi-spray:before {
        content: "\\F665"; }
      :host ::ng-deep .mdi-square-inc:before {
        content: "\\F4CA"; }
      :host ::ng-deep .mdi-square-inc-cash:before {
        content: "\\F4CB"; }
      :host ::ng-deep .mdi-stackexchange:before {
        content: "\\F60B"; }
      :host ::ng-deep .mdi-stackoverflow:before {
        content: "\\F4CC"; }
      :host ::ng-deep .mdi-stadium:before {
        content: "\\F71F"; }
      :host ::ng-deep .mdi-stairs:before {
        content: "\\F4CD"; }
      :host ::ng-deep .mdi-star:before {
        content: "\\F4CE"; }
      :host ::ng-deep .mdi-star-circle:before {
        content: "\\F4CF"; }
      :host ::ng-deep .mdi-star-half:before {
        content: "\\F4D0"; }
      :host ::ng-deep .mdi-star-off:before {
        content: "\\F4D1"; }
      :host ::ng-deep .mdi-star-outline:before {
        content: "\\F4D2"; }
      :host ::ng-deep .mdi-steam:before {
        content: "\\F4D3"; }
      :host ::ng-deep .mdi-steering:before {
        content: "\\F4D4"; }
      :host ::ng-deep .mdi-step-backward:before {
        content: "\\F4D5"; }
      :host ::ng-deep .mdi-step-backward-2:before {
        content: "\\F4D6"; }
      :host ::ng-deep .mdi-step-forward:before {
        content: "\\F4D7"; }
      :host ::ng-deep .mdi-step-forward-2:before {
        content: "\\F4D8"; }
      :host ::ng-deep .mdi-stethoscope:before {
        content: "\\F4D9"; }
      :host ::ng-deep .mdi-sticker:before {
        content: "\\F5D0"; }
      :host ::ng-deep .mdi-stocking:before {
        content: "\\F4DA"; }
      :host ::ng-deep .mdi-stop:before {
        content: "\\F4DB"; }
      :host ::ng-deep .mdi-stop-circle:before {
        content: "\\F666"; }
      :host ::ng-deep .mdi-stop-circle-outline:before {
        content: "\\F667"; }
      :host ::ng-deep .mdi-store:before {
        content: "\\F4DC"; }
      :host ::ng-deep .mdi-store-24-hour:before {
        content: "\\F4DD"; }
      :host ::ng-deep .mdi-stove:before {
        content: "\\F4DE"; }
      :host ::ng-deep .mdi-subdirectory-arrow-left:before {
        content: "\\F60C"; }
      :host ::ng-deep .mdi-subdirectory-arrow-right:before {
        content: "\\F60D"; }
      :host ::ng-deep .mdi-subway:before {
        content: "\\F6AB"; }
      :host ::ng-deep .mdi-subway-variant:before {
        content: "\\F4DF"; }
      :host ::ng-deep .mdi-sunglasses:before {
        content: "\\F4E0"; }
      :host ::ng-deep .mdi-surround-sound:before {
        content: "\\F5C5"; }
      :host ::ng-deep .mdi-svg:before {
        content: "\\F720"; }
      :host ::ng-deep .mdi-swap-horizontal:before {
        content: "\\F4E1"; }
      :host ::ng-deep .mdi-swap-vertical:before {
        content: "\\F4E2"; }
      :host ::ng-deep .mdi-swim:before {
        content: "\\F4E3"; }
      :host ::ng-deep .mdi-switch:before {
        content: "\\F4E4"; }
      :host ::ng-deep .mdi-sword:before {
        content: "\\F4E5"; }
      :host ::ng-deep .mdi-sync:before {
        content: "\\F4E6"; }
      :host ::ng-deep .mdi-sync-alert:before {
        content: "\\F4E7"; }
      :host ::ng-deep .mdi-sync-off:before {
        content: "\\F4E8"; }
      :host ::ng-deep .mdi-tab:before {
        content: "\\F4E9"; }
      :host ::ng-deep .mdi-tab-unselected:before {
        content: "\\F4EA"; }
      :host ::ng-deep .mdi-table:before {
        content: "\\F4EB"; }
      :host ::ng-deep .mdi-table-column-plus-after:before {
        content: "\\F4EC"; }
      :host ::ng-deep .mdi-table-column-plus-before:before {
        content: "\\F4ED"; }
      :host ::ng-deep .mdi-table-column-remove:before {
        content: "\\F4EE"; }
      :host ::ng-deep .mdi-table-column-width:before {
        content: "\\F4EF"; }
      :host ::ng-deep .mdi-table-edit:before {
        content: "\\F4F0"; }
      :host ::ng-deep .mdi-table-large:before {
        content: "\\F4F1"; }
      :host ::ng-deep .mdi-table-row-height:before {
        content: "\\F4F2"; }
      :host ::ng-deep .mdi-table-row-plus-after:before {
        content: "\\F4F3"; }
      :host ::ng-deep .mdi-table-row-plus-before:before {
        content: "\\F4F4"; }
      :host ::ng-deep .mdi-table-row-remove:before {
        content: "\\F4F5"; }
      :host ::ng-deep .mdi-tablet:before {
        content: "\\F4F6"; }
      :host ::ng-deep .mdi-tablet-android:before {
        content: "\\F4F7"; }
      :host ::ng-deep .mdi-tablet-ipad:before {
        content: "\\F4F8"; }
      :host ::ng-deep .mdi-tag:before {
        content: "\\F4F9"; }
      :host ::ng-deep .mdi-tag-faces:before {
        content: "\\F4FA"; }
      :host ::ng-deep .mdi-tag-heart:before {
        content: "\\F68A"; }
      :host ::ng-deep .mdi-tag-multiple:before {
        content: "\\F4FB"; }
      :host ::ng-deep .mdi-tag-outline:before {
        content: "\\F4FC"; }
      :host ::ng-deep .mdi-tag-plus:before {
        content: "\\F721"; }
      :host ::ng-deep .mdi-tag-remove:before {
        content: "\\F722"; }
      :host ::ng-deep .mdi-tag-text-outline:before {
        content: "\\F4FD"; }
      :host ::ng-deep .mdi-target:before {
        content: "\\F4FE"; }
      :host ::ng-deep .mdi-taxi:before {
        content: "\\F4FF"; }
      :host ::ng-deep .mdi-teamviewer:before {
        content: "\\F500"; }
      :host ::ng-deep .mdi-telegram:before {
        content: "\\F501"; }
      :host ::ng-deep .mdi-television:before {
        content: "\\F502"; }
      :host ::ng-deep .mdi-television-guide:before {
        content: "\\F503"; }
      :host ::ng-deep .mdi-temperature-celsius:before {
        content: "\\F504"; }
      :host ::ng-deep .mdi-temperature-fahrenheit:before {
        content: "\\F505"; }
      :host ::ng-deep .mdi-temperature-kelvin:before {
        content: "\\F506"; }
      :host ::ng-deep .mdi-tennis:before {
        content: "\\F507"; }
      :host ::ng-deep .mdi-tent:before {
        content: "\\F508"; }
      :host ::ng-deep .mdi-terrain:before {
        content: "\\F509"; }
      :host ::ng-deep .mdi-test-tube:before {
        content: "\\F668"; }
      :host ::ng-deep .mdi-text-shadow:before {
        content: "\\F669"; }
      :host ::ng-deep .mdi-text-to-speech:before {
        content: "\\F50A"; }
      :host ::ng-deep .mdi-text-to-speech-off:before {
        content: "\\F50B"; }
      :host ::ng-deep .mdi-textbox:before {
        content: "\\F60E"; }
      :host ::ng-deep .mdi-texture:before {
        content: "\\F50C"; }
      :host ::ng-deep .mdi-theater:before {
        content: "\\F50D"; }
      :host ::ng-deep .mdi-theme-light-dark:before {
        content: "\\F50E"; }
      :host ::ng-deep .mdi-thermometer:before {
        content: "\\F50F"; }
      :host ::ng-deep .mdi-thermometer-lines:before {
        content: "\\F510"; }
      :host ::ng-deep .mdi-thumb-down:before {
        content: "\\F511"; }
      :host ::ng-deep .mdi-thumb-down-outline:before {
        content: "\\F512"; }
      :host ::ng-deep .mdi-thumb-up:before {
        content: "\\F513"; }
      :host ::ng-deep .mdi-thumb-up-outline:before {
        content: "\\F514"; }
      :host ::ng-deep .mdi-thumbs-up-down:before {
        content: "\\F515"; }
      :host ::ng-deep .mdi-ticket:before {
        content: "\\F516"; }
      :host ::ng-deep .mdi-ticket-account:before {
        content: "\\F517"; }
      :host ::ng-deep .mdi-ticket-confirmation:before {
        content: "\\F518"; }
      :host ::ng-deep .mdi-ticket-percent:before {
        content: "\\F723"; }
      :host ::ng-deep .mdi-tie:before {
        content: "\\F519"; }
      :host ::ng-deep .mdi-tilde:before {
        content: "\\F724"; }
      :host ::ng-deep .mdi-timelapse:before {
        content: "\\F51A"; }
      :host ::ng-deep .mdi-timer:before {
        content: "\\F51B"; }
      :host ::ng-deep .mdi-timer-10:before {
        content: "\\F51C"; }
      :host ::ng-deep .mdi-timer-3:before {
        content: "\\F51D"; }
      :host ::ng-deep .mdi-timer-off:before {
        content: "\\F51E"; }
      :host ::ng-deep .mdi-timer-sand:before {
        content: "\\F51F"; }
      :host ::ng-deep .mdi-timer-sand-empty:before {
        content: "\\F6AC"; }
      :host ::ng-deep .mdi-timetable:before {
        content: "\\F520"; }
      :host ::ng-deep .mdi-toggle-switch:before {
        content: "\\F521"; }
      :host ::ng-deep .mdi-toggle-switch-off:before {
        content: "\\F522"; }
      :host ::ng-deep .mdi-tooltip:before {
        content: "\\F523"; }
      :host ::ng-deep .mdi-tooltip-edit:before {
        content: "\\F524"; }
      :host ::ng-deep .mdi-tooltip-image:before {
        content: "\\F525"; }
      :host ::ng-deep .mdi-tooltip-outline:before {
        content: "\\F526"; }
      :host ::ng-deep .mdi-tooltip-outline-plus:before {
        content: "\\F527"; }
      :host ::ng-deep .mdi-tooltip-text:before {
        content: "\\F528"; }
      :host ::ng-deep .mdi-tooth:before {
        content: "\\F529"; }
      :host ::ng-deep .mdi-tor:before {
        content: "\\F52A"; }
      :host ::ng-deep .mdi-tower-beach:before {
        content: "\\F680"; }
      :host ::ng-deep .mdi-tower-fire:before {
        content: "\\F681"; }
      :host ::ng-deep .mdi-traffic-light:before {
        content: "\\F52B"; }
      :host ::ng-deep .mdi-train:before {
        content: "\\F52C"; }
      :host ::ng-deep .mdi-tram:before {
        content: "\\F52D"; }
      :host ::ng-deep .mdi-transcribe:before {
        content: "\\F52E"; }
      :host ::ng-deep .mdi-transcribe-close:before {
        content: "\\F52F"; }
      :host ::ng-deep .mdi-transfer:before {
        content: "\\F530"; }
      :host ::ng-deep .mdi-transit-transfer:before {
        content: "\\F6AD"; }
      :host ::ng-deep .mdi-translate:before {
        content: "\\F5CA"; }
      :host ::ng-deep .mdi-treasure-chest:before {
        content: "\\F725"; }
      :host ::ng-deep .mdi-tree:before {
        content: "\\F531"; }
      :host ::ng-deep .mdi-trello:before {
        content: "\\F532"; }
      :host ::ng-deep .mdi-trending-down:before {
        content: "\\F533"; }
      :host ::ng-deep .mdi-trending-neutral:before {
        content: "\\F534"; }
      :host ::ng-deep .mdi-trending-up:before {
        content: "\\F535"; }
      :host ::ng-deep .mdi-triangle:before {
        content: "\\F536"; }
      :host ::ng-deep .mdi-triangle-outline:before {
        content: "\\F537"; }
      :host ::ng-deep .mdi-trophy:before {
        content: "\\F538"; }
      :host ::ng-deep .mdi-trophy-award:before {
        content: "\\F539"; }
      :host ::ng-deep .mdi-trophy-outline:before {
        content: "\\F53A"; }
      :host ::ng-deep .mdi-trophy-variant:before {
        content: "\\F53B"; }
      :host ::ng-deep .mdi-trophy-variant-outline:before {
        content: "\\F53C"; }
      :host ::ng-deep .mdi-truck:before {
        content: "\\F53D"; }
      :host ::ng-deep .mdi-truck-delivery:before {
        content: "\\F53E"; }
      :host ::ng-deep .mdi-truck-trailer:before {
        content: "\\F726"; }
      :host ::ng-deep .mdi-tshirt-crew:before {
        content: "\\F53F"; }
      :host ::ng-deep .mdi-tshirt-v:before {
        content: "\\F540"; }
      :host ::ng-deep .mdi-tumblr:before {
        content: "\\F541"; }
      :host ::ng-deep .mdi-tumblr-reblog:before {
        content: "\\F542"; }
      :host ::ng-deep .mdi-tune:before {
        content: "\\F62E"; }
      :host ::ng-deep .mdi-tune-vertical:before {
        content: "\\F66A"; }
      :host ::ng-deep .mdi-twitch:before {
        content: "\\F543"; }
      :host ::ng-deep .mdi-twitter:before {
        content: "\\F544"; }
      :host ::ng-deep .mdi-twitter-box:before {
        content: "\\F545"; }
      :host ::ng-deep .mdi-twitter-circle:before {
        content: "\\F546"; }
      :host ::ng-deep .mdi-twitter-retweet:before {
        content: "\\F547"; }
      :host ::ng-deep .mdi-ubuntu:before {
        content: "\\F548"; }
      :host ::ng-deep .mdi-umbraco:before {
        content: "\\F549"; }
      :host ::ng-deep .mdi-umbrella:before {
        content: "\\F54A"; }
      :host ::ng-deep .mdi-umbrella-outline:before {
        content: "\\F54B"; }
      :host ::ng-deep .mdi-undo:before {
        content: "\\F54C"; }
      :host ::ng-deep .mdi-undo-variant:before {
        content: "\\F54D"; }
      :host ::ng-deep .mdi-unfold-less:before {
        content: "\\F54E"; }
      :host ::ng-deep .mdi-unfold-more:before {
        content: "\\F54F"; }
      :host ::ng-deep .mdi-ungroup:before {
        content: "\\F550"; }
      :host ::ng-deep .mdi-unity:before {
        content: "\\F6AE"; }
      :host ::ng-deep .mdi-untappd:before {
        content: "\\F551"; }
      :host ::ng-deep .mdi-update:before {
        content: "\\F6AF"; }
      :host ::ng-deep .mdi-upload:before {
        content: "\\F552"; }
      :host ::ng-deep .mdi-usb:before {
        content: "\\F553"; }
      :host ::ng-deep .mdi-vector-arrange-above:before {
        content: "\\F554"; }
      :host ::ng-deep .mdi-vector-arrange-below:before {
        content: "\\F555"; }
      :host ::ng-deep .mdi-vector-circle:before {
        content: "\\F556"; }
      :host ::ng-deep .mdi-vector-circle-variant:before {
        content: "\\F557"; }
      :host ::ng-deep .mdi-vector-combine:before {
        content: "\\F558"; }
      :host ::ng-deep .mdi-vector-curve:before {
        content: "\\F559"; }
      :host ::ng-deep .mdi-vector-difference:before {
        content: "\\F55A"; }
      :host ::ng-deep .mdi-vector-difference-ab:before {
        content: "\\F55B"; }
      :host ::ng-deep .mdi-vector-difference-ba:before {
        content: "\\F55C"; }
      :host ::ng-deep .mdi-vector-intersection:before {
        content: "\\F55D"; }
      :host ::ng-deep .mdi-vector-line:before {
        content: "\\F55E"; }
      :host ::ng-deep .mdi-vector-point:before {
        content: "\\F55F"; }
      :host ::ng-deep .mdi-vector-polygon:before {
        content: "\\F560"; }
      :host ::ng-deep .mdi-vector-polyline:before {
        content: "\\F561"; }
      :host ::ng-deep .mdi-vector-rectangle:before {
        content: "\\F5C6"; }
      :host ::ng-deep .mdi-vector-selection:before {
        content: "\\F562"; }
      :host ::ng-deep .mdi-vector-square:before {
        content: "\\F001"; }
      :host ::ng-deep .mdi-vector-triangle:before {
        content: "\\F563"; }
      :host ::ng-deep .mdi-vector-union:before {
        content: "\\F564"; }
      :host ::ng-deep .mdi-verified:before {
        content: "\\F565"; }
      :host ::ng-deep .mdi-vibrate:before {
        content: "\\F566"; }
      :host ::ng-deep .mdi-video:before {
        content: "\\F567"; }
      :host ::ng-deep .mdi-video-off:before {
        content: "\\F568"; }
      :host ::ng-deep .mdi-video-switch:before {
        content: "\\F569"; }
      :host ::ng-deep .mdi-view-agenda:before {
        content: "\\F56A"; }
      :host ::ng-deep .mdi-view-array:before {
        content: "\\F56B"; }
      :host ::ng-deep .mdi-view-carousel:before {
        content: "\\F56C"; }
      :host ::ng-deep .mdi-view-column:before {
        content: "\\F56D"; }
      :host ::ng-deep .mdi-view-dashboard:before {
        content: "\\F56E"; }
      :host ::ng-deep .mdi-view-day:before {
        content: "\\F56F"; }
      :host ::ng-deep .mdi-view-grid:before {
        content: "\\F570"; }
      :host ::ng-deep .mdi-view-headline:before {
        content: "\\F571"; }
      :host ::ng-deep .mdi-view-list:before {
        content: "\\F572"; }
      :host ::ng-deep .mdi-view-module:before {
        content: "\\F573"; }
      :host ::ng-deep .mdi-view-parallel:before {
        content: "\\F727"; }
      :host ::ng-deep .mdi-view-quilt:before {
        content: "\\F574"; }
      :host ::ng-deep .mdi-view-sequential:before {
        content: "\\F728"; }
      :host ::ng-deep .mdi-view-stream:before {
        content: "\\F575"; }
      :host ::ng-deep .mdi-view-week:before {
        content: "\\F576"; }
      :host ::ng-deep .mdi-vimeo:before {
        content: "\\F577"; }
      :host ::ng-deep .mdi-vine:before {
        content: "\\F578"; }
      :host ::ng-deep .mdi-violin:before {
        content: "\\F60F"; }
      :host ::ng-deep .mdi-visualstudio:before {
        content: "\\F610"; }
      :host ::ng-deep .mdi-vk:before {
        content: "\\F579"; }
      :host ::ng-deep .mdi-vk-box:before {
        content: "\\F57A"; }
      :host ::ng-deep .mdi-vk-circle:before {
        content: "\\F57B"; }
      :host ::ng-deep .mdi-vlc:before {
        content: "\\F57C"; }
      :host ::ng-deep .mdi-voice:before {
        content: "\\F5CB"; }
      :host ::ng-deep .mdi-voicemail:before {
        content: "\\F57D"; }
      :host ::ng-deep .mdi-volume-high:before {
        content: "\\F57E"; }
      :host ::ng-deep .mdi-volume-low:before {
        content: "\\F57F"; }
      :host ::ng-deep .mdi-volume-medium:before {
        content: "\\F580"; }
      :host ::ng-deep .mdi-volume-off:before {
        content: "\\F581"; }
      :host ::ng-deep .mdi-vpn:before {
        content: "\\F582"; }
      :host ::ng-deep .mdi-walk:before {
        content: "\\F583"; }
      :host ::ng-deep .mdi-wallet:before {
        content: "\\F584"; }
      :host ::ng-deep .mdi-wallet-giftcard:before {
        content: "\\F585"; }
      :host ::ng-deep .mdi-wallet-membership:before {
        content: "\\F586"; }
      :host ::ng-deep .mdi-wallet-travel:before {
        content: "\\F587"; }
      :host ::ng-deep .mdi-wan:before {
        content: "\\F588"; }
      :host ::ng-deep .mdi-washing-machine:before {
        content: "\\F729"; }
      :host ::ng-deep .mdi-watch:before {
        content: "\\F589"; }
      :host ::ng-deep .mdi-watch-export:before {
        content: "\\F58A"; }
      :host ::ng-deep .mdi-watch-import:before {
        content: "\\F58B"; }
      :host ::ng-deep .mdi-watch-vibrate:before {
        content: "\\F6B0"; }
      :host ::ng-deep .mdi-water:before {
        content: "\\F58C"; }
      :host ::ng-deep .mdi-water-off:before {
        content: "\\F58D"; }
      :host ::ng-deep .mdi-water-percent:before {
        content: "\\F58E"; }
      :host ::ng-deep .mdi-water-pump:before {
        content: "\\F58F"; }
      :host ::ng-deep .mdi-watermark:before {
        content: "\\F612"; }
      :host ::ng-deep .mdi-weather-cloudy:before {
        content: "\\F590"; }
      :host ::ng-deep .mdi-weather-fog:before {
        content: "\\F591"; }
      :host ::ng-deep .mdi-weather-hail:before {
        content: "\\F592"; }
      :host ::ng-deep .mdi-weather-lightning:before {
        content: "\\F593"; }
      :host ::ng-deep .mdi-weather-lightning-rainy:before {
        content: "\\F67D"; }
      :host ::ng-deep .mdi-weather-night:before {
        content: "\\F594"; }
      :host ::ng-deep .mdi-weather-partlycloudy:before {
        content: "\\F595"; }
      :host ::ng-deep .mdi-weather-pouring:before {
        content: "\\F596"; }
      :host ::ng-deep .mdi-weather-rainy:before {
        content: "\\F597"; }
      :host ::ng-deep .mdi-weather-snowy:before {
        content: "\\F598"; }
      :host ::ng-deep .mdi-weather-snowy-rainy:before {
        content: "\\F67E"; }
      :host ::ng-deep .mdi-weather-sunny:before {
        content: "\\F599"; }
      :host ::ng-deep .mdi-weather-sunset:before {
        content: "\\F59A"; }
      :host ::ng-deep .mdi-weather-sunset-down:before {
        content: "\\F59B"; }
      :host ::ng-deep .mdi-weather-sunset-up:before {
        content: "\\F59C"; }
      :host ::ng-deep .mdi-weather-windy:before {
        content: "\\F59D"; }
      :host ::ng-deep .mdi-weather-windy-variant:before {
        content: "\\F59E"; }
      :host ::ng-deep .mdi-web:before {
        content: "\\F59F"; }
      :host ::ng-deep .mdi-webcam:before {
        content: "\\F5A0"; }
      :host ::ng-deep .mdi-webhook:before {
        content: "\\F62F"; }
      :host ::ng-deep .mdi-webpack:before {
        content: "\\F72A"; }
      :host ::ng-deep .mdi-wechat:before {
        content: "\\F611"; }
      :host ::ng-deep .mdi-weight:before {
        content: "\\F5A1"; }
      :host ::ng-deep .mdi-weight-kilogram:before {
        content: "\\F5A2"; }
      :host ::ng-deep .mdi-whatsapp:before {
        content: "\\F5A3"; }
      :host ::ng-deep .mdi-wheelchair-accessibility:before {
        content: "\\F5A4"; }
      :host ::ng-deep .mdi-white-balance-auto:before {
        content: "\\F5A5"; }
      :host ::ng-deep .mdi-white-balance-incandescent:before {
        content: "\\F5A6"; }
      :host ::ng-deep .mdi-white-balance-iridescent:before {
        content: "\\F5A7"; }
      :host ::ng-deep .mdi-white-balance-sunny:before {
        content: "\\F5A8"; }
      :host ::ng-deep .mdi-widgets:before {
        content: "\\F72B"; }
      :host ::ng-deep .mdi-wifi:before {
        content: "\\F5A9"; }
      :host ::ng-deep .mdi-wifi-off:before {
        content: "\\F5AA"; }
      :host ::ng-deep .mdi-wii:before {
        content: "\\F5AB"; }
      :host ::ng-deep .mdi-wiiu:before {
        content: "\\F72C"; }
      :host ::ng-deep .mdi-wikipedia:before {
        content: "\\F5AC"; }
      :host ::ng-deep .mdi-window-close:before {
        content: "\\F5AD"; }
      :host ::ng-deep .mdi-window-closed:before {
        content: "\\F5AE"; }
      :host ::ng-deep .mdi-window-maximize:before {
        content: "\\F5AF"; }
      :host ::ng-deep .mdi-window-minimize:before {
        content: "\\F5B0"; }
      :host ::ng-deep .mdi-window-open:before {
        content: "\\F5B1"; }
      :host ::ng-deep .mdi-window-restore:before {
        content: "\\F5B2"; }
      :host ::ng-deep .mdi-windows:before {
        content: "\\F5B3"; }
      :host ::ng-deep .mdi-wordpress:before {
        content: "\\F5B4"; }
      :host ::ng-deep .mdi-worker:before {
        content: "\\F5B5"; }
      :host ::ng-deep .mdi-wrap:before {
        content: "\\F5B6"; }
      :host ::ng-deep .mdi-wrench:before {
        content: "\\F5B7"; }
      :host ::ng-deep .mdi-wunderlist:before {
        content: "\\F5B8"; }
      :host ::ng-deep .mdi-xaml:before {
        content: "\\F673"; }
      :host ::ng-deep .mdi-xbox:before {
        content: "\\F5B9"; }
      :host ::ng-deep .mdi-xbox-controller:before {
        content: "\\F5BA"; }
      :host ::ng-deep .mdi-xbox-controller-off:before {
        content: "\\F5BB"; }
      :host ::ng-deep .mdi-xda:before {
        content: "\\F5BC"; }
      :host ::ng-deep .mdi-xing:before {
        content: "\\F5BD"; }
      :host ::ng-deep .mdi-xing-box:before {
        content: "\\F5BE"; }
      :host ::ng-deep .mdi-xing-circle:before {
        content: "\\F5BF"; }
      :host ::ng-deep .mdi-xml:before {
        content: "\\F5C0"; }
      :host ::ng-deep .mdi-yeast:before {
        content: "\\F5C1"; }
      :host ::ng-deep .mdi-yelp:before {
        content: "\\F5C2"; }
      :host ::ng-deep .mdi-yin-yang:before {
        content: "\\F67F"; }
      :host ::ng-deep .mdi-youtube-play:before {
        content: "\\F5C3"; }
      :host ::ng-deep .mdi-zip-box:before {
        content: "\\F5C4"; }
      :host ::ng-deep .mdi-18px.mdi-set, :host ::ng-deep .mdi-18px.mdi:before {
        font-size: 18px; }
      :host ::ng-deep .mdi-24px.mdi-set, :host ::ng-deep .mdi-24px.mdi:before {
        font-size: 24px; }
      :host ::ng-deep .mdi-36px.mdi-set, :host ::ng-deep .mdi-36px.mdi:before {
        font-size: 36px; }
      :host ::ng-deep .mdi-48px.mdi-set, :host ::ng-deep .mdi-48px.mdi:before {
        font-size: 48px; }
      :host ::ng-deep .mdi-dark {
        color: rgba(0, 0, 0, 0.54); }
      :host ::ng-deep .mdi-dark.mdi-inactive {
        color: rgba(0, 0, 0, 0.26); }
      :host ::ng-deep .mdi-light {
        color: white; }
      :host ::ng-deep .mdi-light.mdi-inactive {
        color: rgba(255, 255, 255, 0.3); }
      :host ::ng-deep .inputs-wrapper {
        margin-bottom: 30px; }
      :host ::ng-deep .input-holder {
        width: 100%;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
            -ms-flex-align: center;
                align-items: center;
        min-height: 54px;
        margin-bottom: 11px;
        -webkit-transition: border-color 0.15s ease-out 0s;
        transition: border-color 0.15s ease-out 0s; }
        @media screen and (max-width: 640px) {
          :host ::ng-deep .input-holder {
            min-height: 40px; } }
        :host ::ng-deep .input-holder:last-child {
          margin-bottom: 0; }
        :host ::ng-deep .input-holder input ~ label {
          position: absolute;
          bottom: 15px;
          left: 0;
          padding-left: 20px;
          -webkit-transform: translateY(0);
                  transform: translateY(0);
          -webkit-transform-origin: 0;
                  transform-origin: 0;
          font-size: 16px;
          color: #64686a;
          pointer-events: none;
          -webkit-transition: top .15s, font-size .15s, -webkit-transform .15s;
          transition: top .15s, font-size .15s, -webkit-transform .15s;
          transition: top .15s, font-size .15s, transform .15s;
          transition: top .15s, font-size .15s, transform .15s, -webkit-transform .15s; }
          @media screen and (max-width: 640px) {
            :host ::ng-deep .input-holder input ~ label {
              bottom: 12px;
              font-size: 14px;
              padding-left: 11px; } }
        :host ::ng-deep .input-holder input[readonly] {
          border-color: #30C4F3;
          pointer-events: none; }
          :host ::ng-deep .input-holder input[readonly] ~ label {
            font-size: .55rem;
            -webkit-transform: translateY(-22px);
                    transform: translateY(-22px); }
            @media screen and (max-width: 640px) {
              :host ::ng-deep .input-holder input[readonly] ~ label {
                -webkit-transform: translateY(-17px);
                        transform: translateY(-17px);
                font-size: 10px; } }
        :host ::ng-deep .input-holder .input-state {
          display: block;
          font-size: 24px; }
        :host ::ng-deep .input-holder input {
          display: block;
          width: 100%;
          height: 54px;
          border-radius: 5px;
          padding: 20px 36px 15px 20px;
          background: transparent;
          color: #000;
          font-size: 16px;
          overflow: hidden;
          white-space: nowrap;
          text-overflow: ellipsis;
          -webkit-box-shadow: none;
                  box-shadow: none;
          border: 1px solid #d5d5d5;
          -webkit-transition: border .12s ease-in-out;
          transition: border .12s ease-in-out; }
          @media screen and (max-width: 640px) {
            :host ::ng-deep .input-holder input {
              height: 44px;
              padding: 14px 18px 10px 10px;
              font-size: 14px; } }
          :host ::ng-deep .input-holder input:focus, :host ::ng-deep .input-holder input:valid {
            border-color: #30C4F3; }
          :host ::ng-deep .input-holder input.ng-dirty.ng-invalid {
            border-color: #fc5d2b; }
            :host ::ng-deep .input-holder input.ng-dirty.ng-invalid ~ .input-state:before {
              content: "\\F026";
              color: #fc5d2b; }
          :host ::ng-deep .input-holder input.ng-dirty.ng-valid ~ .input-state:before {
            content: "\\F12C";
            color: #bfc4c8; }
        :host ::ng-deep .input-holder.focus-field input, :host ::ng-deep .input-holder.valid-field input {
          border-color: #30c4f3; }
        :host ::ng-deep .input-holder.error-input input {
          border-color: #fc3e6b; }
        :host ::ng-deep .input-holder .required-marker {
          position: absolute;
          top: -3px;
          right: -13px; }
          :host ::ng-deep .input-holder .required-marker:before {
            content: '*';
            font-size: 1rem;
            color: #fc3e6b; }
        :host ::ng-deep .input-holder .mdi {
          position: absolute;
          top: 50%;
          -webkit-transform: translateY(-50%);
                  transform: translateY(-50%);
          right: 20px;
          font-size: 24px; }
          :host ::ng-deep .input-holder .mdi-alert {
            color: #fc3e6b; }
          :host ::ng-deep .input-holder .mdi-check {
            color: #1daafc; }
        :host ::ng-deep .input-holder input:focus ~ label, :host ::ng-deep .input-holder input:valid ~ label {
          font-size: .55rem;
          -webkit-transform: translateY(-22px);
                  transform: translateY(-22px); }
          @media screen and (max-width: 640px) {
            :host ::ng-deep .input-holder input:focus ~ label, :host ::ng-deep .input-holder input:valid ~ label {
              -webkit-transform: translateY(-17px);
                      transform: translateY(-17px);
              font-size: 10px; } }
        :host ::ng-deep .input-holder.label-top label {
          font-size: .55rem !important;
          -webkit-transform: translateY(-22px);
                  transform: translateY(-22px); }
          @media screen and (max-width: 640px) {
            :host ::ng-deep .input-holder.label-top label {
              -webkit-transform: translateY(-17px);
                      transform: translateY(-17px);
              font-size: 10px; } }
      :host ::ng-deep button {
        overflow: visible; }
      :host ::ng-deep .button, :host ::ng-deep button {
        border: none;
        padding: 8px 24px;
        border-radius: 30px;
        text-transform: uppercase;
        line-height: 36px;
        font-size: .8rem;
        color: #fff;
        background: #1daafc;
        -webkit-box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0.5);
                box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0.5);
        outline: none;
        font-weight: 600;
        cursor: pointer;
        -webkit-transition: background .15s, color .15s, border .15s, -webkit-box-shadow .15s;
        transition: background .15s, color .15s, border .15s, -webkit-box-shadow .15s;
        transition: background .15s, box-shadow .15s, color .15s, border .15s;
        transition: background .15s, box-shadow .15s, color .15s, border .15s, -webkit-box-shadow .15s; }
        @media screen and (max-width: 640px) {
          :host ::ng-deep .button, :host ::ng-deep button {
            line-height: 24px;
            font-size: 12px; } }
        :host ::ng-deep .button:hover, :host ::ng-deep button:hover {
          background: #39b6ff; }
        :host ::ng-deep .button:before, :host ::ng-deep button:before {
          -webkit-transition: color .15s;
          transition: color .15s; }
        :host ::ng-deep .button[disabled], :host ::ng-deep button[disabled] {
          background: #eee;
          color: #a5a8a9;
          pointer-events: none;
          -webkit-box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0);
                  box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0); }
        :host ::ng-deep .button:active, :host ::ng-deep button:active {
          background: #108fd9;
          -webkit-box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0);
                  box-shadow: 0 3px 8px -1px rgba(29, 170, 252, 0); }
      :host ::ng-deep .mdi:before,
      :host ::ng-deep .mdi-set {
        display: inline-block;
        font: normal normal normal 24px/1 "Material Design Icons";
        font-size: inherit;
        text-rendering: auto;
        line-height: inherit;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        -webkit-transform: translate(0, 0);
                transform: translate(0, 0); }
      :host ::ng-deep .card-form-holder {
        position: relative;
        width: 376px;
        margin: 0 auto;
        text-align: center; }
        @media screen and (max-width: 640px) {
          :host ::ng-deep .card-form-holder {
            width: 300px;
            padding-top: 18px; } }
        :host ::ng-deep .card-form-holder .title {
          font-size: 14px;
          margin-bottom: 25px;
          color: #64686a;
          text-align: center; }
        :host ::ng-deep .card-form-holder .mdi-close {
          display: inline-block;
          width: 20px;
          height: 20px;
          position: absolute;
          top: 16px;
          right: 16px;
          font-size: 0;
          cursor: pointer; }
          :host ::ng-deep .card-form-holder .mdi-close:hover:before {
            color: #a4abb0; }
          :host ::ng-deep .card-form-holder .mdi-close:before {
            font-size: 14px;
            color: #bfc4c8;
            -webkit-transition: color .12s ease;
            transition: color .12s ease; }
        :host ::ng-deep .card-form-holder .inputs-wrapper {
          padding: 0 15px; }
          @media screen and (max-width: 640px) {
            :host ::ng-deep .card-form-holder .inputs-wrapper {
              padding: 0 30px; } }
        :host ::ng-deep .card-form-holder .description {
          max-width: 250px;
          margin: 0 auto 35px;
          font-size: 12px;
          color: #b2b4b6; }
          @media screen and (max-width: 640px) {
            :host ::ng-deep .card-form-holder .description {
              margin-bottom: 20px; } }
        :host ::ng-deep .card-form-holder .button-holder {
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-align: center;
              -ms-flex-align: center;
                  align-items: center;
          -webkit-box-pack: end;
              -ms-flex-pack: end;
                  justify-content: flex-end;
          height: 113px;
          padding: 0 32px;
          border-top: 1px solid #dadee1; }
          @media screen and (max-width: 640px) {
            :host ::ng-deep .card-form-holder .button-holder {
              height: 70px; } }
          :host ::ng-deep .card-form-holder .button-holder .button + button {
            margin-left: 10px; }
          :host ::ng-deep .card-form-holder .button-holder .button.cancel-button {
            background-color: transparent;
            color: #1daafc;
            -webkit-box-shadow: 0 3px 6px transparent;
                    box-shadow: 0 3px 6px transparent; }
            :host ::ng-deep .card-form-holder .button-holder .button.cancel-button:hover {
              -webkit-box-shadow: 0 3px 6px rgba(29, 170, 252, 0.6);
                      box-shadow: 0 3px 6px rgba(29, 170, 252, 0.6);
              color: #fff;
              background-color: #1daafc; }
      :host ::ng-deep .single-card .card {
        max-width: 150px;
        margin: 0 auto 25px;
        border-radius: 0 0 8px 8px;
        cursor: default; }
        :host ::ng-deep .single-card .card .card-holder {
          height: 100px; }
          :host ::ng-deep .single-card .card .card-holder .card-actions {
            opacity: 0;
            pointer-events: none; }
      :host ::ng-deep .single-card .card-name {
        display: none; }
      :host ::ng-deep app-card {
        display: block;
        height: 100%; }
      :host ::ng-deep .card {
        position: relative;
        width: 172px;
        height: 100%;
        min-height: 154px;
        text-align: center;
        font-size: 16px;
        color: #64686a;
        background-color: #f5f7f8;
        border-radius: 0 0 8px 8px;
        cursor: pointer;
        float: none; }
        :host ::ng-deep .card .card-holder {
          height: 112px;
          border-radius: 8px;
          border-bottom: 2px solid rgba(0, 0, 0, 0.2);
          -webkit-transition: -webkit-box-shadow .12s ease;
          transition: -webkit-box-shadow .12s ease;
          transition: box-shadow .12s ease;
          transition: box-shadow .12s ease, -webkit-box-shadow .12s ease;
          -webkit-box-shadow: 0 19px 19px -14px rgba(251, 99, 125, 0);
                  box-shadow: 0 19px 19px -14px rgba(251, 99, 125, 0); }
          :host ::ng-deep .card .card-holder:hover .card-info {
            opacity: 0; }
          :host ::ng-deep .card .card-holder:hover .card-actions {
            opacity: 1; }
          :host ::ng-deep .card .card-holder.visa {
            background: linear-gradient(45deg, #4a84fe 0%, #91b3fe 100%); }
            :host ::ng-deep .card .card-holder.visa .card-info .card-logo {
              background-image: url("assets/card_visa.svg"); }
          :host ::ng-deep .card .card-holder.mc, :host ::ng-deep .card .card-holder.mast {
            background: linear-gradient(45deg, #fb637d 0%, #feb04f 100%); }
            :host ::ng-deep .card .card-holder.mc .card-info .card-logo, :host ::ng-deep .card .card-holder.mast .card-info .card-logo {
              background-image: url("assets/card_mc.svg"); }
          :host ::ng-deep .card .card-holder.add-card {
            min-height: 154px;
            height: 100%;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
                -ms-flex-direction: column;
                    flex-direction: column;
            -webkit-box-align: center;
                -ms-flex-align: center;
                    align-items: center;
            -webkit-box-pack: center;
                -ms-flex-pack: center;
                    justify-content: center;
            background-color: #f5f7f8;
            cursor: pointer;
            text-align: center;
            border: none;
            color: #868a8c; }
            :host ::ng-deep .card .card-holder.add-card:hover .add-card-button:before {
              color: #0391e3; }
            :host ::ng-deep .card .card-holder.add-card .add-card-button {
              display: block;
              margin-bottom: 10px; }
              :host ::ng-deep .card .card-holder.add-card .add-card-button:before {
                font-size: 40px;
                line-height: 40px;
                display: block;
                color: #1daafc;
                -webkit-transition: color .12s ease-in-out;
                transition: color .12s ease-in-out; }
          :host ::ng-deep .card .card-holder .add-card-text {
            text-align: center;
            font-size: 1rem; }
          :host ::ng-deep .card .card-holder .card-info {
            padding: 19px 0 0 16px;
            -webkit-transition: opacity .12s ease-in-out;
            transition: opacity .12s ease-in-out; }
            :host ::ng-deep .card .card-holder .card-info .card-logo {
              width: 50px;
              height: 24px;
              margin-bottom: 13px;
              background-size: 25px;
              background-repeat: no-repeat; }
            :host ::ng-deep .card .card-holder .card-info .card-pan {
              display: block;
              width: 100%;
              color: #ffffff;
              text-align: left;
              font-size: 13px; }
          :host ::ng-deep .card .card-holder .card-actions {
            width: 100%;
            height: 112px;
            cursor: pointer;
            display: table;
            color: #ffffff;
            position: absolute;
            left: 50%;
            top: 0;
            -webkit-transform: translate(-50%, 0);
                    transform: translate(-50%, 0);
            opacity: 0;
            -webkit-transition: opacity .12s ease-in-out;
            transition: opacity .12s ease-in-out; }
            :host ::ng-deep .card .card-holder .card-actions > * {
              height: 100%;
              width: 50%;
              margin: 0;
              padding: 0;
              display: table-cell;
              overflow: hidden; }
              :host ::ng-deep .card .card-holder .card-actions > *:nth-child(1) {
                border-right: 1px solid rgba(0, 0, 0, 0.1); }
              :host ::ng-deep .card .card-holder .card-actions > *:nth-child(2) {
                border-left: 1px solid rgba(0, 0, 0, 0.1); }
              :host ::ng-deep .card .card-holder .card-actions > * .card-action-icon-holder {
                font-size: 32px;
                height: 100%;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: center;
                    -ms-flex-pack: center;
                        justify-content: center;
                -webkit-box-align: center;
                    -ms-flex-align: center;
                        align-items: center; }
        :host ::ng-deep .card .card-name {
          margin: 0;
          padding: 12px 20px 12px 16px;
          overflow: visible;
          text-overflow: initial;
          white-space: normal;
          text-align: left;
          font-size: 11px;
          word-wrap: break-word; }
      :host ::ng-deep .card-block {
        margin: 0 6px 15px; }
      :host ::ng-deep .cards::-webkit-scrollbar {
        width: 4px;
        background-color: transparent; }
      :host ::ng-deep .cards::-webkit-scrollbar-thumb {
        background-color: #aaa;
        width: 4px;
        border-radius: 6px; }
      :host ::ng-deep app-select-card {
        display: block;
        padding-right: 4px;
        padding-bottom: 4px; }
      :host ::ng-deep .cards {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
            -ms-flex-flow: row wrap;
                flex-flow: row wrap;
        -ms-flex-line-pack: start;
            align-content: flex-start;
        width: 800px;
        max-height: 502px;
        overflow: auto;
        padding: 0 30px; }
        @media screen and (max-width: 640px) {
          :host ::ng-deep .cards {
            width: 405px;
            max-height: 400px;
            padding: 0 15px 20px; } }
        @media screen and (max-width: 1024px) {
          :host ::ng-deep .cards {
            width: 435px; } }
  `]
            },] },
];
/**
 * @nocollapse
 */
MosstCardComponent.ctorParameters = () => [
    { type: LocaleService, },
    { type: ApiService, },
];
MosstCardComponent.propDecorators = {
    'user': [{ type: Input },],
    'lang': [{ type: Input },],
    'onCardSelect': [{ type: Output },],
    'onAddCard': [{ type: Output },],
    'onCardDelete': [{ type: Output },],
};

class MaskWorker {
    /**
     * @param {?=} mask
     * @param {?=} output
     * @param {?=} allowedInputs
     * @param {?=} advancedSetter
     * @param {?=} replacement
     * @param {?=} showConstants
     */
    constructor(mask = (val) => val, output = (val) => val, allowedInputs = '.*', advancedSetter = null, replacement = 'X', showConstants = false) {
        this.mask = mask;
        this.output = output;
        this.allowedInputs = allowedInputs;
        this.advancedSetter = advancedSetter;
        this.replacement = replacement;
        this.showConstants = showConstants;
        this.allowedSymbols = new RegExp(allowedInputs);
        this.constantPositions = this.getConstantPositions(mask);
    }
    /**
     * @param {?=} inputString
     * @param {?=} ignoreReplacement
     * @return {?}
     */
    removeNotAllowedSymbolsFromString(inputString = '', ignoreReplacement = false) {
        let /** @type {?} */ stringWithoutNotAllowedSymbols = '';
        for (let /** @type {?} */ i = 0; i < inputString.length; i++) {
            if (this.allowedSymbols.test(inputString[i]) || (ignoreReplacement && inputString[i] === this.replacement)) {
                stringWithoutNotAllowedSymbols += inputString[i];
            }
        }
        return stringWithoutNotAllowedSymbols;
    }
    /**
     * @param {?} string
     * @return {?}
     */
    restoreToTextOutput(string) {
        const /** @type {?} */ meaningPart = this.getMeaningPart(string);
        let /** @type {?} */ result = this.output;
        for (let /** @type {?} */ i = 0; i < meaningPart.length; i++) {
            result = result.replace(/X/, meaningPart[i]);
        }
        result = result.replace(/X/g, '');
        return result;
    }
    /**
     * @param {?} string
     * @return {?}
     */
    getMeaningPart(string) {
        let /** @type {?} */ meaningPart = '';
        if (string.length > this.mask.length) {
            string = string.slice(0, this.mask.length);
        }
        for (let /** @type {?} */ i = 0; i < string.length; i++) {
            if (this.mask[i] === 'X') {
                meaningPart += string[i];
            }
        }
        return meaningPart;
    }
    /**
     * @param {?} str
     * @return {?}
     */
    removePredefinedOutput(str) {
        const /** @type {?} */ getFirstMatchTail = (_str, symbol) => {
            for (let /** @type {?} */ i = 0; i < _str.length; i++) {
                if (_str[i] === symbol) {
                    return _str.slice(i);
                }
            }
            return '';
        };
        const /** @type {?} */ equalPartsLength = (string1, string2) => {
            let /** @type {?} */ length = 0;
            for (let /** @type {?} */ i = 0; i < string1.length; i++) {
                if (string1[i] === string2[i]) {
                    length++;
                }
                else {
                    return length;
                }
            }
            return length;
        };
        const /** @type {?} */ outputTemplate = this.removeNotAllowedSymbolsFromString(this.output);
        const /** @type {?} */ outputTail = getFirstMatchTail(outputTemplate, str[0]);
        return str.slice(equalPartsLength(outputTail, str));
    }
    /**
     * @param {?} string
     * @return {?}
     */
    cutTail(string) {
        const /** @type {?} */ getPreviousReplacerIndex = (index) => {
            if (!index) {
                return 0;
            }
            for (let /** @type {?} */ i = index - 1; i > 0; i--) {
                if (this.mask[i] === this.replacement) {
                    return i + 1;
                }
            }
            return index;
        };
        const /** @type {?} */ re = new RegExp('X', 'g');
        const /** @type {?} */ findReplacer = re.exec(string);
        return findReplacer ? string.slice(0, getPreviousReplacerIndex(findReplacer.index)) : string;
    }
    /**
     * @param {?} string
     * @return {?}
     */
    useTextMask(string) {
        const /** @type {?} */ allowedLength = this.mask.length - this.mask.replace(/X/g, '').length;
        const /** @type {?} */ stringWithoutNotAllowedSymbols = this.removeNotAllowedSymbolsFromString(string);
        string = this.removePredefinedOutput(stringWithoutNotAllowedSymbols).slice(0, allowedLength);
        let /** @type {?} */ currentString = this.mask;
        for (let /** @type {?} */ i = 0; i < allowedLength; i++) {
            if (i < string.length) {
                currentString = currentString.replace(/X/, string[i]);
            }
            else if (this.showConstants) {
                currentString = currentString.replace(/X/, '');
            }
        }
        currentString = this.cutTail(currentString);
        return currentString;
    }
    /**
     * @param {?} string
     * @return {?}
     */
    applyAdvancedSetter(string) {
        if (typeof this.advancedSetter === 'function') {
            return this.advancedSetter(string);
        }
        if (this.advancedSetter instanceof RegExp) {
            return string.match(this.advancedSetter) ? string : '';
        }
        return string;
    }
    /**
     * @param {?=} string
     * @return {?}
     */
    useMask(string = '') {
        string = string.toString();
        if (typeof this.mask === 'function') {
            string = this.removeNotAllowedSymbolsFromString(string);
            return this.mask(string);
        }
        return this.useTextMask(this.applyAdvancedSetter(string));
    }
    /**
     * @param {?=} string
     * @return {?}
     */
    unmask(string = '') {
        if (typeof this.output === 'function') {
            return this.output(string);
        }
        return this.restoreToTextOutput(string);
    }
    /**
     * @param {?} str
     * @return {?}
     */
    getConstantPositions(str) {
        if (typeof str === 'function') {
            return [];
        }
        const /** @type {?} */ constantPositions = [];
        for (let /** @type {?} */ i = 0; i < str.length; i++) {
            if (str[i] !== 'X') {
                constantPositions.push(i + 1);
            }
        }
        return constantPositions;
    }
    /**
     * @param {?} pos
     * @param {?=} deletion
     * @return {?}
     */
    getCursorPositionWithOffset(pos, deletion = false) {
        const /** @type {?} */ res = deletion
            ? pos - this.getCursorOffsetRight(pos)
            : pos + this.getCursorOffsetRight(pos);
        const /** @type {?} */ startPos = this.getStartPosition();
        return res < startPos ? startPos : res;
    }
    /**
     * @return {?}
     */
    getStartPosition() {
        if (!this.constantPositions.length || this.constantPositions[0] !== 1) {
            return 0;
        }
        let /** @type {?} */ i;
        for (i = 1; i < this.constantPositions.length; ++i) {
            if (this.constantPositions[i] - this.constantPositions[i - 1] !== 1) {
                break;
            }
        }
        return this.constantPositions[i - 1];
    }
    /**
     * @param {?} pos
     * @return {?}
     */
    getCursorOffsetRight(pos) {
        const /** @type {?} */ constantMatchIndex = this.constantPositions.indexOf(pos);
        if (constantMatchIndex !== -1) {
            let /** @type {?} */ i = constantMatchIndex;
            while (i < this.constantPositions.length) {
                if (this.constantPositions[i + 1] - this.constantPositions[i] !== 1) {
                    break;
                }
                i++;
            }
            return i - constantMatchIndex + 1;
        }
        return 0;
    }
    /**
     * @param {?} pos
     * @return {?}
     */
    getCursorOffsetLeft(pos) {
        const /** @type {?} */ constantMatchIndex = this.constantPositions.indexOf(pos);
        if (constantMatchIndex !== -1) {
            let /** @type {?} */ i = constantMatchIndex;
            while (i >= 0) {
                if (this.constantPositions[i] - this.constantPositions[i - 1] !== 1) {
                    break;
                }
                i--;
            }
            return constantMatchIndex - i + 1;
        }
        return 0;
    }
}

/**
 * @param {?} mask
 * @return {?}
 */
function createMaskWorker(mask) {
    return new MaskWorker(mask.mask, mask.output, mask.allowedInputs, mask.advancedSetter, mask.replacement, mask.showConstants);
}
/**
 * @param {?} mask
 * @return {?}
 */
function format(mask) {
    return (value) => {
        const /** @type {?} */ maskWorker = createMaskWorker(mask);
        return maskWorker.useMask(value);
    };
}
class InputMaskDirective {
    /**
     * @param {?} model
     * @param {?} el
     */
    constructor(model, el) {
        this.model = model;
        this.el = el;
        this.valueChanges = new EventEmitter();
        this.previousValue = '';
        this.valueChanges.subscribe(val => {
            if (this.formControl) {
                this.formControl.setValue(val, {
                    onlySelf: true,
                    emitEvent: false,
                    emitModelToViewChange: false,
                    emitViewToModelChange: false
                });
            }
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.maskWorker) {
            if (this.mask) {
                this.initMaskWorker();
            }
        }
        if (this.formControl) {
            if (this.mask.maskedValue) {
                const /** @type {?} */ maskedValue = this.maskWorker.useMask(this.formControl.value || '');
                this.model.valueAccessor.writeValue(maskedValue);
                this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
            }
            else {
                this.formControl.setValue(this.maskWorker.useMask(this.formControl.value || ''));
            }
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyDown(event) {
        if (event.key === 'Delete' && this.customDelete(event)) {
            event.preventDefault();
            return;
        }
        if (event.key === 'Backspace' && this.customBackspace(event)) {
            event.preventDefault();
            return;
        }
        const /** @type {?} */ inputStart = this.maskWorker.getStartPosition();
        const /** @type {?} */ isHomeKey = event.key === 'Home';
        const /** @type {?} */ isArrowLeftKey = event.key === 'ArrowLeft';
        const { selectionStart, selectionEnd, value } = event.target;
        if (!selectionStart && selectionEnd === value.length) {
            return;
        }
        if (selectionStart <= inputStart || isHomeKey) {
            this.el.nativeElement.selectionStart = inputStart;
            this.el.nativeElement.selectionEnd = inputStart;
            if (!isHomeKey && isArrowLeftKey) {
                event.preventDefault();
            }
        }
        if (isHomeKey) {
            event.preventDefault();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onInputChange(event) {
        if (!this.maskWorker) {
            if (!this.mask) {
                return;
            }
            this.initMaskWorker();
        }
        const /** @type {?} */ start = this.el.nativeElement.selectionStart;
        const /** @type {?} */ maskedValue = this.maskWorker.useMask(event);
        this.model.valueAccessor.writeValue(maskedValue);
        const /** @type {?} */ deletion = this.previousValue.length >= maskedValue.length;
        this.el.nativeElement.selectionStart = this.maskWorker.getCursorPositionWithOffset(start, deletion);
        this.el.nativeElement.selectionEnd = this.maskWorker.getCursorPositionWithOffset(start, deletion);
        this.previousValue = maskedValue;
        this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
    }
    /**
     * @param {?} event
     * @param {?=} left
     * @return {?}
     */
    customDelete(event, left = false) {
        const { selectionStart, selectionEnd } = event.target;
        const /** @type {?} */ inputStart = this.maskWorker.getStartPosition();
        let /** @type {?} */ value = '';
        let /** @type {?} */ movePos = selectionStart;
        if (selectionStart > 0 && selectionEnd <= event.target.value.length) {
            const /** @type {?} */ offset = left
                ? -this.maskWorker.getCursorOffsetLeft(selectionStart) - 1
                : this.maskWorker.getCursorOffsetRight(selectionStart + 1);
            const /** @type {?} */ pos = selectionStart + offset;
            if (pos < 0) {
                return true;
            }
            if (left && pos >= this.maskWorker.getStartPosition()) {
                movePos = pos;
            }
            const /** @type {?} */ arr = event.target.value.split('');
            arr.splice(pos, 1);
            value = arr.join('');
        }
        else {
            movePos = inputStart;
        }
        const /** @type {?} */ maskedValue = this.maskWorker.useMask(value);
        this.model.valueAccessor.writeValue(maskedValue);
        event.target.selectionStart = movePos;
        event.target.selectionEnd = movePos;
        this.previousValue = maskedValue;
        this.valueChanges.emit(this.maskWorker.unmask(maskedValue));
        return true;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    customBackspace(event) {
        return this.customDelete(event, true);
    }
    /**
     * @return {?}
     */
    initMaskWorker() {
        this.maskWorker = createMaskWorker(this.mask);
    }
}
InputMaskDirective.decorators = [
    { type: Directive, args: [{
                selector: '[inputMask]',
                providers: [NgModel],
                host: {
                    '(ngModelChange)': 'onInputChange($event)',
                    '(keydown)': 'onKeyDown($event)'
                }
            },] },
];
/**
 * @nocollapse
 */
InputMaskDirective.ctorParameters = () => [
    { type: NgModel, },
    { type: ElementRef, },
];
InputMaskDirective.propDecorators = {
    'formControl': [{ type: Input },],
    'mask': [{ type: Input },],
    'valueChanges': [{ type: Output },],
};

class InputMaskModule {
}
InputMaskModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    InputMaskDirective
                ],
                exports: [
                    InputMaskDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
InputMaskModule.ctorParameters = () => [];

const inputMasks = {
    pan: {
        mask: 'XXXX XXXX XXXX XXXX',
        output: 'XXXXXXXXXXXXXXXX',
        allowedInputs: '[0-9|\*]',
        advancedSetter: /^[456].*/g
    },
    exp: {
        mask: 'XX/XX',
        output: (output) => output,
        allowedInputs: '[0-9]',
        maskedValue: true,
        showConstants: true
    }
};

class CCTypes {
    /**
     * @param {?} cardName
     * @return {?}
     */
    static isAccepted(cardName) {
        return cardName === this.AMERICAN_EXPRESS
            || cardName === this.DISCOVER
            || cardName === this.DINERS_CLUB_INTERNATIONAL
            || cardName === this.VISA
            || cardName === this.MASTERCARD;
    }
}
CCTypes.NONE = 'none';
CCTypes.AMERICAN_EXPRESS = 'amex';
CCTypes.DISCOVER = 'disc';
CCTypes.DINERS_CLUB_INTERNATIONAL = 'dine';
CCTypes.VISA = 'visa';
CCTypes.MASTERCARD = 'mast';

class CCValidator {
    /**
     * @param {?} cardNumber
     * @return {?}
     */
    static isValid(cardNumber) {
        const /** @type {?} */ testNumber = this.__preprocess(cardNumber);
        const /** @type {?} */ cardType = this.getCardType(testNumber);
        const /** @type {?} */ isMasked = /\*/.test(cardNumber);
        if (cardType !== CCTypes.NONE) {
            return this.__lengthValid(testNumber, cardType) && isMasked || this.__luhnValid(testNumber);
        }
        return false;
    }
    /**
     * Return the credit card type based on the card number (provided the card is in the accepted list of cards)
     *
     * @param {?} cardNumber : string - Credit card number, which may include spaces or dashes, i.e. XXXX-YYYY-ZZZZ-ABCD
     *
     * @return {?} string - Credit card (CCTypes member) type, which may be 'none' if the card number is not recognized
     */
    static getCardType(cardNumber) {
        let /** @type {?} */ cardProps;
        const /** @type {?} */ theCard = this.__preprocess(cardNumber);
        for (const /** @type {?} */ key in this._types) {
            if (this._types.hasOwnProperty(key)) {
                cardProps = this._types[key];
                if (theCard.match(cardProps['pattern'])) {
                    return key;
                }
            }
        }
        return 'none';
    }
    /**
     * @param {?} cardNumber
     * @return {?}
     */
    static __preprocess(cardNumber) {
        return cardNumber.replace(/[ -]/g, '');
    }
    /**
     * @param {?} cardNumber
     * @param {?} cardType
     * @return {?}
     */
    static __lengthValid(cardNumber, cardType) {
        const /** @type {?} */ cardProps = this._types[cardType];
        return cardProps ? cardNumber.length === cardProps['length'] : false;
    }
    /**
     * @param {?} cardNumber
     * @return {?}
     */
    static __luhnValid(cardNumber) {
        let /** @type {?} */ digit;
        let /** @type {?} */ n;
        let /** @type {?} */ sum;
        let /** @type {?} */ j;
        sum = 0;
        const /** @type {?} */ numbers = cardNumber.split('').reverse().map((val) => parseFloat(val));
        const /** @type {?} */ len = numbers.length;
        n = 0;
        j = 0;
        // there's really nothing new under the sun ...
        while (j < len) {
            digit = numbers[n];
            digit = +digit;
            if (n % 2) {
                digit *= 2;
                if (digit < 10) {
                    sum += digit;
                }
                else {
                    sum += digit - 9;
                }
            }
            else {
                sum += digit;
            }
            n = ++j;
        }
        return sum % 10 === 0;
    }
}
CCValidator.MIN_LENGTH = 14;
CCValidator._types = {
    // "amex" : { pattern: /^3[47]/, length: 15 }
    disc: { pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/, length: 16 },
    // , "dine" : { pattern: /^36/, length: 14 },
    visa: { pattern: /^4/, length: 16 },
    mast: { pattern: /^5[1-5]/, length: 16 }
};

class CardComponent {
    constructor() {
        this.options = {};
        this.onSelect = new EventEmitter();
        this.onDelete = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        setTimeout(() => {
            this.cardType = CCValidator.getCardType(this.data.card);
            this.pan = format(inputMasks.pan)(this.data.card);
        });
    }
    /**
     * @return {?}
     */
    select() {
        this.onSelect.emit(this.data);
    }
    /**
     * @return {?}
     */
    delete() {
        this.onDelete.emit(this.data);
    }
}
CardComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-card',
                template: `
    <div class="card">
      <div
        class="card-holder"
        [ngClass]="{
          visa: cardType === 'visa',
          mc: cardType === 'mast'
        }"
      >
        <div class="card-info">
          <div class="card-logo"></div>
          <span class="card-pan">{{pan}}</span>
        </div>

        <div class="card-actions">
          <div class="card-action-select" (click)="select()">
            <div class="card-action-icon-holder">
              <span class="mdi mdi-cart-plus"></span>
            </div>
          </div>
          <div class="card-action-delete" (click)="delete()">
            <div class="card-action-icon-holder">
              <span class="mdi mdi-delete"></span>
            </div>
          </div>
        </div>
      </div>
      <p *ngIf="options.name" class="card-name">{{data.alias}}</p>
    </div>
  `
            },] },
];
/**
 * @nocollapse
 */
CardComponent.ctorParameters = () => [];
CardComponent.propDecorators = {
    'data': [{ type: Input },],
    'options': [{ type: Input },],
    'onSelect': [{ type: Output },],
    'onDelete': [{ type: Output },],
};

class SelectCardComponent {
    /**
     * @param {?} apiService
     */
    constructor(apiService) {
        this.apiService = apiService;
        this.cards = [];
        this.cardOptions = {
            actions: true,
            name: true
        };
        this.onAdd = new EventEmitter();
        this.onSelect = new EventEmitter();
        this.onDelete = new EventEmitter();
        this.fetch();
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        setTimeout(() => this.fetch());
    }
    /**
     * @return {?}
     */
    add() {
        this.onAdd.emit();
    }
    /**
     * @param {?} card
     * @return {?}
     */
    select(card) {
        this.onSelect.emit(card);
    }
    /**
     * @param {?} card
     * @return {?}
     */
    delete(card) {
        this.onDelete.emit(card);
    }
    /**
     * @return {?}
     */
    fetch() {
        this.apiService.getCardList()
            .subscribe(cards => this.cards = cards);
    }
}
SelectCardComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-select-card',
                template: `
    <div class="cards">
      <div class="card-block" *ngFor="let card of cards">
        <app-card
          [data]="card"
          [options]="cardOptions"
          (onSelect)="select($event)"
          (onDelete)="delete($event)"
        ></app-card>
      </div>

      <div class="card-block" (click)="add()">
        <div class="card">
          <div class="card-holder add-card">
            <span class="mdi mdi-plus-circle add-card-button"></span>
            <div class="add-card-text">Добавить карту</div>
          </div>
        </div>
      </div>
    </div>
  `
            },] },
];
/**
 * @nocollapse
 */
SelectCardComponent.ctorParameters = () => [
    { type: ApiService, },
];
SelectCardComponent.propDecorators = {
    'onAdd': [{ type: Output },],
    'onSelect': [{ type: Output },],
    'onDelete': [{ type: Output },],
};

class DeleteCardComponent {
    /**
     * @param {?} apiService
     */
    constructor(apiService) {
        this.apiService = apiService;
        this.formGroup = new FormGroup({
            alias: new FormControl(),
            pan: new FormControl(),
            exp: new FormControl()
        });
        this.formGroup = new FormGroup({
            alias: new FormControl(),
            pan: new FormControl(),
            exp: new FormControl()
        });
        this.onDelete = new EventEmitter();
        this.onCancel = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        const { alias, card, expiration } = this.data;
        this.formGroup.setValue({
            alias,
            pan: card,
            exp: expiration
        });
    }
    /**
     * @return {?}
     */
    onSubmit() {
        this.apiService.deleteCard({ guid: this.data.guid })
            .subscribe((res) => {
            this.onDelete.emit(this.data);
        });
    }
    /**
     * @return {?}
     */
    cancel() {
        this.onCancel.emit(this.data);
    }
}
DeleteCardComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-delete-card',
                template: `
    <div class="card-form-holder" *ngIf="data">
        <div class="card-holder single-card">
            <app-card [data]="data"></app-card>
        </div>

        <form [formGroup]="formGroup" (ngSubmit)="onSubmit()">
          <div class="inputs-wrapper">
            <div class="input-holder">
                <input type="text" id="alias" readonly formControlName="alias" required>
                <span class="mdi input-state"></span>
                <label for="alias">{{'CARD.ALIAS' | translate}}</label>
            </div>
        
            <div class="input-holder">
                <input
                    type="text"
                    id="pan"
                    formControlName="pan"
                    readonly
                    required
                >
                <span class="mdi input-state"></span>
                <label for="pan">{{'CARD.PAN' | translate}}</label>
            </div>
        
            <div class="input-holder">
                <input
                    type="text"
                    id="exp"
                    formControlName="exp"
                    readonly
                    required
                >
                <span class="mdi input-state"></span>
                <label for="exp">{{'CARD.EXP' | translate}}</label>
            </div>
          </div>

            <div class="description">{{'DELETE_CARD.DESCRIPTION' | translate}}</div>
        
          <div class="button-holder">
            <button class="button cancel-button secondary" type="button" (click)="cancel()">{{'DELETE_CARD.CANCEL_BUTTON' | translate}}</button>
            <button class="button" type="submit">{{'DELETE_CARD.SUBMIT_BUTTON' | translate}}</button>
          </div>
        </form>
    </div>
  `
            },] },
];
/**
 * @nocollapse
 */
DeleteCardComponent.ctorParameters = () => [
    { type: ApiService, },
];
DeleteCardComponent.propDecorators = {
    'data': [{ type: Input },],
    'onDelete': [{ type: Output },],
    'onCancel': [{ type: Output },],
};

/**
 * @param {?} control
 * @return {?}
 */
function creditCardValidator(control) {
    const /** @type {?} */ card = control.value.toString();
    if (card.length > 0) {
        // length test
        if (card.length < CCValidator.MIN_LENGTH) {
            return { 'minlength': true };
        }
        // general validation test
        if (!CCValidator.isValid(card)) {
            return { 'invalid': true };
        }
    }
}

const expDateRegExp = /^((0[1-9])|(1[0-2]))\/[0-9][0-9]$/;
/**
 * @param {?} control
 * @return {?}
 */
function expDateValidator(control) {
    const /** @type {?} */ exp = control.value.toString();
    if (exp.length > 0) {
        if (!expDateRegExp.test(exp)) {
            return { 'invalid': true };
        }
    }
}

class AddCardComponent {
    /**
     * @param {?} apiService
     */
    constructor(apiService) {
        this.apiService = apiService;
        this.formGroup = new FormGroup({
            alias: new FormControl(),
            pan: new FormControl(),
            exp: new FormControl()
        });
        this.panMask = inputMasks.pan;
        this.expMask = inputMasks.exp;
        this.formGroup = new FormGroup({
            alias: new FormControl(),
            pan: new FormControl('', [
                creditCardValidator
            ]),
            exp: new FormControl('', [
                expDateValidator
            ])
        });
        this.onAdd = new EventEmitter();
        this.onCancel = new EventEmitter();
    }
    /**
     * @return {?}
     */
    onSubmit() {
        const { alias, exp } = this.formGroup.value;
        const { pan } = this;
        this.apiService.addCard({ alias, exp, pan })
            .subscribe((res) => {
            this.onAdd.emit(this.data);
        });
    }
    /**
     * @return {?}
     */
    cancel() {
        this.onCancel.emit();
    }
}
AddCardComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-add-card',
                template: `
    <div class="card-form-holder">
      <form [formGroup]="formGroup" (ngSubmit)="onSubmit()">
        <div class="inputs-wrapper">
          <div class="input-holder">
            <input type="text" id="alias" formControlName="alias" required>
            <span class="mdi input-state"></span>
            <label for="alias">{{'CARD.ALIAS' | translate}}</label>
          </div>
    
          <div class="input-holder">
            <input maxlength="19" type="text" id="pan" formControlName="pan" inputMask [mask]="panMask" (valueChanges)="pan = $event" required>
            <span class="mdi input-state"></span>
            <label for="pan">{{'CARD.PAN' | translate}}</label>
          </div>
    
          <div class="input-holder">
            <input maxlength="5" type="text" id="exp" formControlName="exp" inputMask [mask]="expMask" required>
            <span class="mdi input-state"></span>
            <label for="exp">{{'CARD.EXP' | translate}}</label>
          </div>
        </div>

        <div class="description">{{'ADD_CARD.DESCRIPTION' | translate}}</div>
    
        <div class="button-holder">
          <button class="button cancel-button secondary" type="button" (click)="cancel()">{{'ADD_CARD.CANCEL_BUTTON' | translate}}</button>
          <button class="button" type="submit" [disabled]="!formGroup.valid">{{'ADD_CARD.SUBMIT_BUTTON' | translate}}</button>
        </div>
      </form>
    </div>
  `
            },] },
];
/**
 * @nocollapse
 */
AddCardComponent.ctorParameters = () => [
    { type: ApiService, },
];
AddCardComponent.propDecorators = {
    'data': [{ type: Input },],
    'onAdd': [{ type: Output },],
    'onCancel': [{ type: Output },],
};

class TranslatePipe {
    /**
     * @param {?} localeService
     */
    constructor(localeService) {
        this.localeService = localeService;
    }
    /**
     * @param {?} value
     * @param {...?} args
     * @return {?}
     */
    transform(value, ...args) {
        return this.localeService.translate(value, args);
    }
}
TranslatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'translate',
                pure: false
            },] },
];
/**
 * @nocollapse
 */
TranslatePipe.ctorParameters = () => [
    { type: LocaleService, },
];

// core
// module
// components
// pipe
// services
// modules
// config
const MOSST_CARD_MODULE_CONFIG = new InjectionToken('MOSST_CARD_MODULE_CONFIG');
class MosstCardModule {
    /**
     * @param {?=} config
     * @return {?}
     */
    static forRoot(config) {
        return {
            ngModule: MosstCardModule,
            providers: [
                {
                    provide: MOSST_CARD_MODULE_CONFIG,
                    useValue: config
                },
                {
                    provide: MosstCardModuleConfig,
                    useFactory: provideConfig,
                    deps: [MOSST_CARD_MODULE_CONFIG]
                },
                ConfigService
            ]
        };
    }
    /**
     * @return {?}
     */
    static forChild() {
        return {
            ngModule: MosstCardModule
        };
    }
}
MosstCardModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    HttpModule,
                    InputMaskModule
                ],
                declarations: [
                    MosstCardComponent,
                    CardComponent,
                    SelectCardComponent,
                    DeleteCardComponent,
                    AddCardComponent,
                    TranslatePipe
                ],
                providers: [
                    ApiService,
                    LocaleService,
                    ConfigService
                ],
                exports: [
                    MosstCardComponent
                ]
            },] },
];
/**
 * @nocollapse
 */
MosstCardModule.ctorParameters = () => [];
/**
 * @param {?} config
 * @return {?}
 */
function provideConfig(config) {
    return new MosstCardModuleConfig(config);
}

/**
 * Generated bundle index. Do not edit.
 */

export { MOSST_CARD_MODULE_CONFIG, MosstCardModule, provideConfig, MosstCardComponent, AddCardComponent as ɵj, CardComponent as ɵg, DeleteCardComponent as ɵi, SelectCardComponent as ɵh, MosstCardModuleConfig as ɵf, InputMaskDirective as ɵb, InputMaskModule as ɵa, TranslatePipe as ɵk, ApiService as ɵd, ConfigService as ɵe, LocaleService as ɵc };
//# sourceMappingURL=mosst-card.js.map
