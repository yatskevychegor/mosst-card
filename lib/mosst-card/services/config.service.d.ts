import { MosstCardModuleConfig } from '../config';
export declare class ConfigService {
    private config;
    constructor(config: MosstCardModuleConfig);
    getApiHost(): string;
    getSign(): any;
}
