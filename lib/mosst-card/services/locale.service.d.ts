export declare class LocaleService {
    private currentLang;
    private locales;
    constructor();
    translate(value: any, args: any): any;
    updateLocales(customLocales?: {}): void;
    setLocale(lang: string): void;
}
