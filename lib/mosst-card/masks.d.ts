import { IMask } from './modules/input-mask';
export declare const inputMasks: {
    [key: string]: IMask;
};
