import { ModuleWithProviders, InjectionToken } from '@angular/core';
import { IMosstCardModuleConfig } from './interfaces';
import { MosstCardModuleConfig } from './config';
export declare const MOSST_CARD_MODULE_CONFIG: InjectionToken<{}>;
export declare class MosstCardModule {
    static forRoot(config?: IMosstCardModuleConfig): ModuleWithProviders;
    static forChild(): ModuleWithProviders;
}
export declare function provideConfig(config: IMosstCardModuleConfig): MosstCardModuleConfig;
