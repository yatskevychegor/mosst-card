import { EventEmitter, OnInit, ElementRef } from '@angular/core';
import { NgModel, AbstractControl } from '@angular/forms';
import { MaskWorker } from '../workers/mask.worker';
import { IMask } from '../interfaces';
export declare function createMaskWorker(mask: IMask): MaskWorker;
export declare function format(mask: IMask): (value: string) => any;
export declare class InputMaskDirective implements OnInit {
    protected model: NgModel;
    protected el: ElementRef;
    formControl: AbstractControl;
    mask: IMask;
    valueChanges: EventEmitter<string>;
    protected previousValue: string;
    protected maskWorker: MaskWorker;
    constructor(model: NgModel, el: ElementRef);
    ngOnInit(): void;
    onKeyDown(event: any): void;
    onInputChange(event: any): void;
    protected customDelete(event: any, left?: boolean): boolean;
    protected customBackspace(event: any): boolean;
    protected initMaskWorker(): void;
}
