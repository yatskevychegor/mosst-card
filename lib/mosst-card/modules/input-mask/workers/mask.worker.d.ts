export declare class MaskWorker {
    private mask;
    private output;
    private allowedInputs;
    private advancedSetter;
    private replacement;
    private showConstants;
    private allowedSymbols;
    private constantPositions;
    constructor(mask?: any, output?: any, allowedInputs?: string, advancedSetter?: any, replacement?: string, showConstants?: boolean);
    private removeNotAllowedSymbolsFromString(inputString?, ignoreReplacement?);
    private restoreToTextOutput(string);
    private getMeaningPart(string);
    private removePredefinedOutput(str);
    private cutTail(string);
    private useTextMask(string);
    private applyAdvancedSetter(string);
    useMask(string?: string): any;
    unmask(string?: string): any;
    private getConstantPositions(str);
    getCursorPositionWithOffset(pos: any, deletion?: boolean): number;
    getStartPosition(): number;
    getCursorOffsetRight(pos: any): number;
    getCursorOffsetLeft(pos: any): number;
}
