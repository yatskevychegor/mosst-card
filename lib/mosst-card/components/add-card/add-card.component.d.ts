import { EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { ICard } from '../../interfaces';
import { IMask } from '../../modules/input-mask/index';
export declare class AddCardComponent {
    private apiService;
    data: ICard;
    onAdd: EventEmitter<ICard>;
    onCancel: EventEmitter<void>;
    formGroup: FormGroup;
    pan: string;
    panMask: IMask;
    expMask: IMask;
    constructor(apiService: ApiService);
    onSubmit(): void;
    cancel(): void;
}
