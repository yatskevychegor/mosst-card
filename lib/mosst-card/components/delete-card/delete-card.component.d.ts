import { OnChanges, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { ICard } from '../../interfaces';
export declare class DeleteCardComponent implements OnChanges {
    private apiService;
    data: ICard;
    onDelete: EventEmitter<ICard>;
    onCancel: EventEmitter<ICard>;
    formGroup: FormGroup;
    pan: string;
    constructor(apiService: ApiService);
    ngOnChanges(): void;
    onSubmit(): void;
    cancel(): void;
}
