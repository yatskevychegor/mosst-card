export declare class CCTypes {
    static NONE: string;
    static AMERICAN_EXPRESS: string;
    static DISCOVER: string;
    static DINERS_CLUB_INTERNATIONAL: string;
    static VISA: string;
    static MASTERCARD: string;
    static isAccepted(cardName: string): boolean;
}
