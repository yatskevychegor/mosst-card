import { AbstractControl } from '@angular/forms';
export declare function creditCardValidator(control: AbstractControl): {
    [s: string]: boolean;
};
