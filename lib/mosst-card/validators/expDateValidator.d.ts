import { AbstractControl } from '@angular/forms';
export declare function expDateValidator(control: AbstractControl): {
    [s: string]: boolean;
};
