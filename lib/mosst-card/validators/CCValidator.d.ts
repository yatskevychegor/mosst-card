export declare class CCValidator {
    static MIN_LENGTH: number;
    private static _types;
    static isValid(cardNumber: string): boolean;
    /**
     * Return the credit card type based on the card number (provided the card is in the accepted list of cards)
     *
     * @param cardNumber : string - Credit card number, which may include spaces or dashes, i.e. XXXX-YYYY-ZZZZ-ABCD
     *
     * @return string - Credit card (CCTypes member) type, which may be 'none' if the card number is not recognized
     */
    static getCardType(cardNumber: string): string;
    /**
     * Is the supplied credit-card number valid
     *
     * @param cardNumber : string - Credit card number, which may include spaces or dashes, i.e. XXXX-YYYY-ZZZZ-ABCD
     *
     * @return boolean - true if the card number is recognized as a supported card type and the card-number properties are correct
     * for that card type.  Note that this does not mean the card is valid to be charged against, only that the number is theoretically
     * correct for the card type.
     */
    private static __preprocess(cardNumber);
    private static __lengthValid(cardNumber, cardType);
    private static __luhnValid(cardNumber);
}
