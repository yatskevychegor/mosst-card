import { OnChanges, EventEmitter } from '@angular/core';
import { ICard, IUser } from './interfaces';
import { ApiService } from './services/api.service';
import { LocaleService } from './services/locale.service';
export declare type TMosstCardScreen = 'select' | 'add' | 'delete';
export declare class MosstCardComponent implements OnChanges {
    private localeService;
    private apiService;
    user: IUser;
    lang: string;
    onCardSelect: EventEmitter<ICard>;
    onAddCard: EventEmitter<ICard>;
    onCardDelete: EventEmitter<ICard>;
    screen: TMosstCardScreen;
    card: ICard;
    constructor(localeService: LocaleService, apiService: ApiService);
    ngOnChanges(changes: any): void;
    addCard(card: ICard): void;
    selectCard(card: ICard): void;
    deleteCard(card: ICard): void;
    setScreen(screen: TMosstCardScreen, card?: ICard): void;
}
